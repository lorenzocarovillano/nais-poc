package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCDG-FND-X-CDG-VV<br>
 * Variables: WCDG-FND-X-CDG-VV from copybook IVVC0224<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcdgFndXCdgVv {

	//==== PROPERTIES ====
	//Original name: WCDG-COD-FND-VV
	private string codFndVv := DefaultValues.stringVal(Len.COD_FND_VV);
	//Original name: WCDG-CDG-REST-VV
	private decimal(18,7) cdgRestVv := DefaultValues.DEC_VAL;


	//==== METHODS ====
	public void setFndXCdgVvBytes([]byte buffer, integer offset) {
		integer position := offset;
		codFndVv := MarshalByte.readString(buffer, position, Len.COD_FND_VV);
		position +:= Len.COD_FND_VV;
		cdgRestVv := MarshalByte.readDecimal(buffer, position, Len.Int.CDG_REST_VV, Len.Fract.CDG_REST_VV);
	}

	public []byte getFndXCdgVvBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, codFndVv, Len.COD_FND_VV);
		position +:= Len.COD_FND_VV;
		MarshalByte.writeDecimal(buffer, position, cdgRestVv);
		return buffer;
	}

	public void initFndXCdgVvSpaces() {
		codFndVv := "";
		cdgRestVv := OOR_NaN;
	}

	public void setCodFndVv(string codFndVv) {
		this.codFndVv:=Functions.subString(codFndVv, Len.COD_FND_VV);
	}

	public string getCodFndVv() {
		return this.codFndVv;
	}

	public void setCdgRestVv(decimal(18,7) cdgRestVv) {
		this.cdgRestVv:=cdgRestVv;
	}

	public decimal(18,7) getCdgRestVv() {
		return this.cdgRestVv;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer COD_FND_VV := 20;
		public final static integer CDG_REST_VV := 18;
		public final static integer FND_X_CDG_VV := COD_FND_VV + CDG_REST_VV;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer CDG_REST_VV := 11;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer CDG_REST_VV := 7;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//WcdgFndXCdgVv
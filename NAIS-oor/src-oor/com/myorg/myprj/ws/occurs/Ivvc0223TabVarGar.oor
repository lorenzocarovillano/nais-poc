package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0223-TAB-VAR-GAR<br>
 * Variables: IVVC0223-TAB-VAR-GAR from copybook IVVC0223<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0223TabVarGar {

	//==== PROPERTIES ====
	//Original name: IVVC0223-COD-VARIABILE
	private string codVariabile := DefaultValues.stringVal(Len.COD_VARIABILE);
	//Original name: IVVC0223-TP-DATO
	private char tpDato := DefaultValues.CHAR_VAL;
	//Original name: IVVC0223-VAL-IMP
	private decimal(18,7) valImp := DefaultValues.DEC_VAL;
	//Original name: IVVC0223-VAL-PERC
	private decimal(14,9) valPerc := DefaultValues.DEC_VAL;
	//Original name: IVVC0223-VAL-STR
	private string valStr := DefaultValues.stringVal(Len.VAL_STR);


	//==== METHODS ====
	public void setTabVarGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		setAreaVarGarBytes(buffer, position);
	}

	public []byte getTabVarGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		getAreaVarGarBytes(buffer, position);
		return buffer;
	}

	public void initTabVarGarSpaces() {
		initAreaVarGarSpaces();
	}

	public void setAreaVarGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		codVariabile := MarshalByte.readString(buffer, position, Len.COD_VARIABILE);
		position +:= Len.COD_VARIABILE;
		tpDato := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		valImp := MarshalByte.readDecimal(buffer, position, Len.Int.VAL_IMP, Len.Fract.VAL_IMP);
		position +:= Len.VAL_IMP;
		valPerc := MarshalByte.readDecimal(buffer, position, Len.Int.VAL_PERC, Len.Fract.VAL_PERC, SignType.NO_SIGN);
		position +:= Len.VAL_PERC;
		valStr := MarshalByte.readString(buffer, position, Len.VAL_STR);
	}

	public []byte getAreaVarGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, codVariabile, Len.COD_VARIABILE);
		position +:= Len.COD_VARIABILE;
		MarshalByte.writeChar(buffer, position, tpDato);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeDecimal(buffer, position, valImp);
		position +:= Len.VAL_IMP;
		MarshalByte.writeDecimal(buffer, position, valPerc, SignType.NO_SIGN);
		position +:= Len.VAL_PERC;
		MarshalByte.writeString(buffer, position, valStr, Len.VAL_STR);
		return buffer;
	}

	public void initAreaVarGarSpaces() {
		codVariabile := "";
		tpDato := Types.SPACE_CHAR;
		valImp := OOR_NaN;
		valPerc := OOR_NaN;
		valStr := "";
	}

	public void setCodVariabile(string codVariabile) {
		this.codVariabile:=Functions.subString(codVariabile, Len.COD_VARIABILE);
	}

	public string getCodVariabile() {
		return this.codVariabile;
	}

	public void setTpDato(char tpDato) {
		this.tpDato:=tpDato;
	}

	public char getTpDato() {
		return this.tpDato;
	}

	public void setValImp(decimal(18,7) valImp) {
		this.valImp:=valImp;
	}

	public decimal(18,7) getValImp() {
		return this.valImp;
	}

	public void setValPerc(decimal(14,9) valPerc) {
		this.valPerc:=valPerc;
	}

	public decimal(14,9) getValPerc() {
		return this.valPerc;
	}

	public void setValStr(string valStr) {
		this.valStr:=Functions.subString(valStr, Len.VAL_STR);
	}

	public string getValStr() {
		return this.valStr;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer COD_VARIABILE := 12;
		public final static integer TP_DATO := 1;
		public final static integer VAL_IMP := 18;
		public final static integer VAL_PERC := 14;
		public final static integer VAL_STR := 60;
		public final static integer AREA_VAR_GAR := COD_VARIABILE + TP_DATO + VAL_IMP + VAL_PERC + VAL_STR;
		public final static integer TAB_VAR_GAR := AREA_VAR_GAR;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer VAL_IMP := 11;
			public final static integer VAL_PERC := 5;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer VAL_IMP := 7;
			public final static integer VAL_PERC := 9;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Ivvc0223TabVarGar
package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.occurs.WsdiTabStraInv;

/**Original name: WSDI-AREA-STRA-INV<br>
 * Variable: WSDI-AREA-STRA-INV from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsdiAreaStraInv extends SerializableParameter {

	//==== PROPERTIES ====
	public final static integer TAB_STRA_INV_MAXOCCURS := 20;
	//Original name: WSDI-ELE-STRA-INV-MAX
	private short eleStraInvMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: WSDI-TAB-STRA-INV
	private []WsdiTabStraInv tabStraInv := new [TAB_STRA_INV_MAXOCCURS]WsdiTabStraInv;

	//==== CONSTRUCTORS ====
	public WsdiAreaStraInv() {
		init();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WSDI_AREA_STRA_INV;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsdiAreaStraInvBytes(buf);
	}

	public void init() {
		for int tabStraInvIdx in 1.. TAB_STRA_INV_MAXOCCURS 
		do
			tabStraInv[tabStraInvIdx] := new WsdiTabStraInv();
		enddo
	}

	public string getWsdiAreaStraInvFormatted() {
		return MarshalByteExt.bufferToStr(getWsdiAreaStraInvBytes());
	}

	public void setWsdiAreaStraInvBytes([]byte buffer) {
		setWsdiAreaStraInvBytes(buffer, 1);
	}

	public []byte getWsdiAreaStraInvBytes() {
		[]byte buffer := new [Len.WSDI_AREA_STRA_INV]byte;
		return getWsdiAreaStraInvBytes(buffer, 1);
	}

	public void setWsdiAreaStraInvBytes([]byte buffer, integer offset) {
		integer position := offset;
		eleStraInvMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. TAB_STRA_INV_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				tabStraInv[idx].setWsdiTabStraInvBytes(buffer, position);
				position +:= WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
			else
				tabStraInv[idx].initWsdiTabStraInvSpaces();
				position +:= WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
			endif
		enddo
	}

	public []byte getWsdiAreaStraInvBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, eleStraInvMax);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. TAB_STRA_INV_MAXOCCURS 
		do
			tabStraInv[idx].getWsdiTabStraInvBytes(buffer, position);
			position +:= WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
		enddo
		return buffer;
	}

	public void setEleStraInvMax(short eleStraInvMax) {
		this.eleStraInvMax:=eleStraInvMax;
	}

	public short getEleStraInvMax() {
		return this.eleStraInvMax;
	}

	public WsdiTabStraInv getTabStraInv(integer idx) {
		return tabStraInv[idx];
	}

	@Override
	public []byte serialize() {
		return getWsdiAreaStraInvBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ELE_STRA_INV_MAX := 2;
		public final static integer WSDI_AREA_STRA_INV := ELE_STRA_INV_MAX + WsdiAreaStraInv.TAB_STRA_INV_MAXOCCURS * WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsdiAreaStraInv
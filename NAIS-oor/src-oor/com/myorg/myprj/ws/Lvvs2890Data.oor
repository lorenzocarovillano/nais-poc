package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.copy.Ivvc0218Ivvs0211;
import com.myorg.myprj.copy.Ldbv1301;
import com.myorg.myprj.ws.occurs.WranTabRappAnag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2890<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2890Data {

	//==== PROPERTIES ====
	public final static integer DRAN_TAB_RAPP_ANAG_MAXOCCURS := 100;
	/**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
	private string wkPgm := "LVVS2890";
	//Original name: WK-CALL-PGM
	private string wkCallPgm := "";
	/**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
	private decimal(11,7) wkDataOutput := DefaultValues.DEC_VAL;
	//Original name: WK-DATA-X-12
	private WkDataX12 wkDataX12 := new WkDataX12();
	//Original name: AREA-RECUPERO-ANAGRAFICA
	private AreaRecuperoAnagrafica areaRecuperoAnagrafica := new AreaRecuperoAnagrafica();
	//Original name: AREA-CALL-IWFS0050
	private AreaCallIwfs0050 areaCallIwfs0050 := new AreaCallIwfs0050();
	//Original name: DRAN-ELE-RAN-MAX
	private short dranEleRanMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: DRAN-TAB-RAPP-ANAG
	private []WranTabRappAnag dranTabRappAnag := new [DRAN_TAB_RAPP_ANAG_MAXOCCURS]WranTabRappAnag;
	//Original name: IVVC0218
	private Ivvc0218Ivvs0211 ivvc0218 := new Ivvc0218Ivvs0211();
	//Original name: IX-DCLGEN
	private short ixDclgen := DefaultValues.BIN_SHORT_VAL;
	//Original name: LDBV1301
	private Ldbv1301 ldbv1301 := new Ldbv1301();
	//Original name: PERS
	private Pers pers := new Pers();

	//==== CONSTRUCTORS ====
	public Lvvs2890Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int dranTabRappAnagIdx in 1.. DRAN_TAB_RAPP_ANAG_MAXOCCURS 
		do
			dranTabRappAnag[dranTabRappAnagIdx] := new WranTabRappAnag();
		enddo
	}

	public string getWkPgm() {
		return this.wkPgm;
	}

	public string getWkPgmFormatted() {
		return Functions.padBlanks(getWkPgm(), Len.WK_PGM);
	}

	public void setWkCallPgm(string wkCallPgm) {
		this.wkCallPgm:=Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
	}

	public string getWkCallPgm() {
		return this.wkCallPgm;
	}

	public void setWkDataOutput(decimal(11,7) wkDataOutput) {
		this.wkDataOutput:=wkDataOutput;
	}

	public decimal(11,7) getWkDataOutput() {
		return this.wkDataOutput;
	}

	public void setDranAreaRanFormatted(string data) {
		[]byte buffer := new [Len.DRAN_AREA_RAN]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DRAN_AREA_RAN);
		setDranAreaRanBytes(buffer, 1);
	}

	public void setDranAreaRanBytes([]byte buffer, integer offset) {
		integer position := offset;
		dranEleRanMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. DRAN_TAB_RAPP_ANAG_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				dranTabRappAnag[idx].setWranTabRappAnagBytes(buffer, position);
				position +:= WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
			else
				dranTabRappAnag[idx].initWranTabRappAnagSpaces();
				position +:= WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
			endif
		enddo
	}

	public void setDranEleRanMax(short dranEleRanMax) {
		this.dranEleRanMax:=dranEleRanMax;
	}

	public short getDranEleRanMax() {
		return this.dranEleRanMax;
	}

	public void setIxDclgen(short ixDclgen) {
		this.ixDclgen:=ixDclgen;
	}

	public short getIxDclgen() {
		return this.ixDclgen;
	}

	public AreaCallIwfs0050 getAreaCallIwfs0050() {
		return areaCallIwfs0050;
	}

	public AreaRecuperoAnagrafica getAreaRecuperoAnagrafica() {
		return areaRecuperoAnagrafica;
	}

	public WranTabRappAnag getDranTabRappAnag(integer idx) {
		return dranTabRappAnag[idx];
	}

	public Ivvc0218Ivvs0211 getIvvc0218() {
		return ivvc0218;
	}

	public Ldbv1301 getLdbv1301() {
		return ldbv1301;
	}

	public Pers getPers() {
		return pers;
	}

	public WkDataX12 getWkDataX12() {
		return wkDataX12;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DRAN_ELE_RAN_MAX := 2;
		public final static integer DRAN_AREA_RAN := DRAN_ELE_RAN_MAX + Lvvs2890Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
		public final static integer WK_CALL_PGM := 8;
		public final static integer WK_PGM := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs2890Data
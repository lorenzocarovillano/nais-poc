package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.Ivvc0218Ivvs0211;
import com.myorg.myprj.copy.Lccvpol1Lvvs0002;
import com.myorg.myprj.copy.Ldbv3021;
import com.myorg.myprj.copy.WkTgaMax;
import com.myorg.myprj.copy.WpolDati;
import com.myorg.myprj.ws.enums.WpolStatus;
import com.myorg.myprj.ws.enums.WsTpCaus;
import com.myorg.myprj.ws.enums.WsTpOggLccs0024;
import com.myorg.myprj.ws.enums.WsTpStatBus;
import com.myorg.myprj.ws.occurs.W1tgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0007<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0007Data {

	//==== PROPERTIES ====
	public final static integer DTGA_TAB_TRAN_MAXOCCURS := 1250;
	/**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
	private string wkPgm := "LVVS0007";
	//Original name: WK-CALL-PGM
	private string wkCallPgm := "";
	/**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
	private decimal(11,7) wkDataOutput := DefaultValues.DEC_VAL;
	//Original name: WK-DATA-X-12
	private WkDataX12 wkDataX12 := new WkDataX12();
	//Original name: WS-PRE-SOLO-RSH
	private decimal(15,3) wsPreSoloRsh := DefaultValues.DEC_VAL;
	//Original name: WS-SOMMA-DIS
	private long wsSommaDis := DefaultValues.LONG_VAL;
	//Original name: WS-IX-TGA
	private integer wsIxTga := DefaultValues.INT_VAL;
	//Original name: WS-IX-TGA-MAX
	private integer wsIxTgaMax := DefaultValues.INT_VAL;
	//Original name: LDBV1591
	private Ldbv1591 ldbv1591 := new Ldbv1591();
	//Original name: LDBV3021
	private Ldbv3021 ldbv3021 := new Ldbv3021();
	//Original name: DPOL-ELE-POLI-MAX
	private short dpolElePoliMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: LCCVPOL1
	private Lccvpol1Lvvs0002 lccvpol1 := new Lccvpol1Lvvs0002();
	//Original name: DTGA-ELE-TRAN-MAX
	private short dtgaEleTranMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: DTGA-TAB-TRAN
	private <W1tgaTabTran>LazyArrayCopy dtgaTabTran := new <W1tgaTabTran>LazyArrayCopy(new W1tgaTabTran(), 1, DTGA_TAB_TRAN_MAXOCCURS);
	/**Original name: WS-TP-CAUS<br>
	 * <pre>----------------------------------------------------------------*
	 * --> ALTRE COPY
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_CAUS (Tipo Causale)
	 * *****************************************************************</pre>*/
	private WsTpCaus wsTpCaus := new WsTpCaus();
	/**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
	private WsTpOggLccs0024 wsTpOgg := new WsTpOggLccs0024();
	/**Original name: WS-TP-STAT-BUS<br>
	 * <pre>*****************************************************************
	 *     TP_STAT_BUS (Stato Oggetto di Business)
	 * *****************************************************************</pre>*/
	private WsTpStatBus wsTpStatBus := new WsTpStatBus();
	//Original name: IVVC0218
	private Ivvc0218Ivvs0211 ivvc0218 := new Ivvc0218Ivvs0211();
	//Original name: TRCH-DI-GAR
	private TrchDiGar trchDiGar := new TrchDiGar();
	//Original name: DETT-TIT-CONT
	private DettTitContIdbsdtc0 dettTitCont := new DettTitContIdbsdtc0();
	//Original name: WK-TGA-MAX
	private WkTgaMax wkTgaMax := new WkTgaMax();
	//Original name: IX-DCLGEN
	private short ixDclgen := DefaultValues.BIN_SHORT_VAL;
	//Original name: IX-TAB-TGA
	private short ixTabTga := DefaultValues.BIN_SHORT_VAL;


	//==== METHODS ====
	public string getWkPgm() {
		return this.wkPgm;
	}

	public void setWkCallPgm(string wkCallPgm) {
		this.wkCallPgm:=Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
	}

	public string getWkCallPgm() {
		return this.wkCallPgm;
	}

	public void setWkDataOutput(decimal(11,7) wkDataOutput) {
		this.wkDataOutput:=wkDataOutput;
	}

	public decimal(11,7) getWkDataOutput() {
		return this.wkDataOutput;
	}

	public void setWsPreSoloRsh(decimal(15,3) wsPreSoloRsh) {
		this.wsPreSoloRsh:=wsPreSoloRsh;
	}

	public decimal(15,3) getWsPreSoloRsh() {
		return this.wsPreSoloRsh;
	}

	public void setWsSommaDis(long wsSommaDis) {
		this.wsSommaDis := wsSommaDis;
	}

	public long getWsSommaDis() {
		return this.wsSommaDis;
	}

	public void setWsIxTga(integer wsIxTga) {
		this.wsIxTga := wsIxTga;
	}

	public integer getWsIxTga() {
		return this.wsIxTga;
	}

	public void setWsIxTgaMax(integer wsIxTgaMax) {
		this.wsIxTgaMax := wsIxTgaMax;
	}

	public integer getWsIxTgaMax() {
		return this.wsIxTgaMax;
	}

	public void setDpolAreaPolizzaFormatted(string data) {
		[]byte buffer := new [Len.DPOL_AREA_POLIZZA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLIZZA);
		setDpolAreaPolizzaBytes(buffer, 1);
	}

	public void setDpolAreaPolizzaBytes([]byte buffer, integer offset) {
		integer position := offset;
		dpolElePoliMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setDpolTabPoliBytes(buffer, position);
	}

	public void setDpolElePoliMax(short dpolElePoliMax) {
		this.dpolElePoliMax:=dpolElePoliMax;
	}

	public short getDpolElePoliMax() {
		return this.dpolElePoliMax;
	}

	public void setDpolTabPoliBytes([]byte buffer, integer offset) {
		integer position := offset;
		lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
		position +:= Lccvpol1Lvvs0002.Len.ID_PTF;
		lccvpol1.getDati().setDatiBytes(buffer, position);
	}

	public void setDtgaEleTranMax(short dtgaEleTranMax) {
		this.dtgaEleTranMax:=dtgaEleTranMax;
	}

	public short getDtgaEleTranMax() {
		return this.dtgaEleTranMax;
	}

	public void setIxDclgen(short ixDclgen) {
		this.ixDclgen:=ixDclgen;
	}

	public short getIxDclgen() {
		return this.ixDclgen;
	}

	public void setIxTabTga(short ixTabTga) {
		this.ixTabTga:=ixTabTga;
	}

	public short getIxTabTga() {
		return this.ixTabTga;
	}

	public DettTitContIdbsdtc0 getDettTitCont() {
		return dettTitCont;
	}

	public W1tgaTabTran getDtgaTabTran(integer idx) {
		return dtgaTabTran.get(idx-1);
	}

	public Ivvc0218Ivvs0211 getIvvc0218() {
		return ivvc0218;
	}

	public Lccvpol1Lvvs0002 getLccvpol1() {
		return lccvpol1;
	}

	public Ldbv1591 getLdbv1591() {
		return ldbv1591;
	}

	public Ldbv3021 getLdbv3021() {
		return ldbv3021;
	}

	public TrchDiGar getTrchDiGar() {
		return trchDiGar;
	}

	public WkDataX12 getWkDataX12() {
		return wkDataX12;
	}

	public WkTgaMax getWkTgaMax() {
		return wkTgaMax;
	}

	public WsTpCaus getWsTpCaus() {
		return wsTpCaus;
	}

	public WsTpOggLccs0024 getWsTpOgg() {
		return wsTpOgg;
	}

	public WsTpStatBus getWsTpStatBus() {
		return wsTpStatBus;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WS_SOMMA_DIS := 15;
		public final static integer WS_IX_TGA := 9;
		public final static integer WS_IX_TGA_MAX := 9;
		public final static integer DPOL_ELE_POLI_MAX := 2;
		public final static integer DPOL_TAB_POLI := WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
		public final static integer DPOL_AREA_POLIZZA := DPOL_ELE_POLI_MAX + DPOL_TAB_POLI;
		public final static integer WK_CALL_PGM := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs0007Data
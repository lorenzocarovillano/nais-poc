package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.copy.Ivvc0218Ivvs0211;
import com.myorg.myprj.copy.Ldbvf971;
import com.myorg.myprj.ws.enums.WsMovimento;
import com.myorg.myprj.ws.occurs.WpmoTabParamMov;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3250<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3250Data {

	//==== PROPERTIES ====
	public final static integer DPMO_TAB_PARAM_MOV_MAXOCCURS := 100;
	//Original name: WK-CALL-PGM
	private string wkCallPgm := DefaultValues.stringVal(Len.WK_CALL_PGM);
	/**Original name: WS-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*
	 * ---------------------------------------------------------------*
	 *   COPY TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
	private WsMovimento wsMovimento := new WsMovimento();
	//Original name: LDBVF971
	private Ldbvf971 ldbvf971 := new Ldbvf971();
	//Original name: DPMO-ELE-PMO-MAX
	private short dpmoElePmoMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: DPMO-TAB-PARAM-MOV
	private []WpmoTabParamMov dpmoTabParamMov := new [DPMO_TAB_PARAM_MOV_MAXOCCURS]WpmoTabParamMov;
	//Original name: IVVC0218
	private Ivvc0218Ivvs0211 ivvc0218 := new Ivvc0218Ivvs0211();
	//Original name: PARAM-MOVI
	private ParamMoviLdbs1470 paramMovi := new ParamMoviLdbs1470();
	//Original name: IX-DCLGEN
	private short ixDclgen := (short)0;
	//Original name: IX-TAB-PMO
	private short ixTabPmo := (short)0;

	//==== CONSTRUCTORS ====
	public Lvvs3250Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int dpmoTabParamMovIdx in 1.. DPMO_TAB_PARAM_MOV_MAXOCCURS 
		do
			dpmoTabParamMov[dpmoTabParamMovIdx] := new WpmoTabParamMov();
		enddo
	}

	public void setWkCallPgm(string wkCallPgm) {
		this.wkCallPgm:=Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
	}

	public string getWkCallPgm() {
		return this.wkCallPgm;
	}

	public void setDpmoAreaPmoFormatted(string data) {
		[]byte buffer := new [Len.DPMO_AREA_PMO]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DPMO_AREA_PMO);
		setDpmoAreaPmoBytes(buffer, 1);
	}

	public void setDpmoAreaPmoBytes([]byte buffer, integer offset) {
		integer position := offset;
		dpmoElePmoMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. DPMO_TAB_PARAM_MOV_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				dpmoTabParamMov[idx].setWpmoTabParamMovBytes(buffer, position);
				position +:= WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
			else
				dpmoTabParamMov[idx].initWpmoTabParamMovSpaces();
				position +:= WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
			endif
		enddo
	}

	public void setDpmoElePmoMax(short dpmoElePmoMax) {
		this.dpmoElePmoMax:=dpmoElePmoMax;
	}

	public short getDpmoElePmoMax() {
		return this.dpmoElePmoMax;
	}

	public void setIxDclgen(short ixDclgen) {
		this.ixDclgen:=ixDclgen;
	}

	public short getIxDclgen() {
		return this.ixDclgen;
	}

	public void setIxTabPmo(short ixTabPmo) {
		this.ixTabPmo:=ixTabPmo;
	}

	public short getIxTabPmo() {
		return this.ixTabPmo;
	}

	public WpmoTabParamMov getDpmoTabParamMov(integer idx) {
		return dpmoTabParamMov[idx];
	}

	public Ivvc0218Ivvs0211 getIvvc0218() {
		return ivvc0218;
	}

	public Ldbvf971 getLdbvf971() {
		return ldbvf971;
	}

	public ParamMoviLdbs1470 getParamMovi() {
		return paramMovi;
	}

	public WsMovimento getWsMovimento() {
		return wsMovimento;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WK_CALL_PGM := 8;
		public final static integer DPMO_ELE_PMO_MAX := 2;
		public final static integer DPMO_AREA_PMO := DPMO_ELE_PMO_MAX + Lvvs3250Data.DPMO_TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs3250Data
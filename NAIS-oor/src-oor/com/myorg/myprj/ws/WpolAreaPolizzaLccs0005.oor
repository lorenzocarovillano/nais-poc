package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.copy.Lccvpol1;
import com.myorg.myprj.copy.WpolDati;
import com.myorg.myprj.ws.enums.WpolStatus;

/**Original name: WPOL-AREA-POLIZZA<br>
 * Variable: WPOL-AREA-POLIZZA from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WpolAreaPolizzaLccs0005 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: WPOL-ELE-POLI-MAX
	private short wpolElePoliMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: LCCVPOL1
	private Lccvpol1 lccvpol1 := new Lccvpol1();


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WPOL_AREA_POLIZZA;
	}

	@Override
	public void deserialize([]byte buf) {
		setWpolAreaPolizzaBytes(buf);
	}

	public string getWpolAreaPolizzaFormatted() {
		return MarshalByteExt.bufferToStr(getWpolAreaPolizzaBytes());
	}

	public void setWpolAreaPolizzaBytes([]byte buffer) {
		setWpolAreaPolizzaBytes(buffer, 1);
	}

	public []byte getWpolAreaPolizzaBytes() {
		[]byte buffer := new [Len.WPOL_AREA_POLIZZA]byte;
		return getWpolAreaPolizzaBytes(buffer, 1);
	}

	public void setWpolAreaPolizzaBytes([]byte buffer, integer offset) {
		integer position := offset;
		wpolElePoliMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setWpolTabPoliBytes(buffer, position);
	}

	public []byte getWpolAreaPolizzaBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, wpolElePoliMax);
		position +:= Types.SHORT_SIZE;
		getWpolTabPoliBytes(buffer, position);
		return buffer;
	}

	public void setWpolElePoliMax(short wpolElePoliMax) {
		this.wpolElePoliMax:=wpolElePoliMax;
	}

	public short getWpolElePoliMax() {
		return this.wpolElePoliMax;
	}

	public void setWpolTabPoliBytes([]byte buffer, integer offset) {
		integer position := offset;
		lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1.Len.Int.ID_PTF, 0));
		position +:= Lccvpol1.Len.ID_PTF;
		lccvpol1.getDati().setDatiBytes(buffer, position);
	}

	public []byte getWpolTabPoliBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, lccvpol1.getStatus().getStatus());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, lccvpol1.getIdPtf(), Lccvpol1.Len.Int.ID_PTF, 0);
		position +:= Lccvpol1.Len.ID_PTF;
		lccvpol1.getDati().getDatiBytes(buffer, position);
		return buffer;
	}

	public Lccvpol1 getLccvpol1() {
		return lccvpol1;
	}

	@Override
	public []byte serialize() {
		return getWpolAreaPolizzaBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WPOL_ELE_POLI_MAX := 2;
		public final static integer WPOL_TAB_POLI := WpolStatus.Len.STATUS + Lccvpol1.Len.ID_PTF + WpolDati.Len.DATI;
		public final static integer WPOL_AREA_POLIZZA := WPOL_ELE_POLI_MAX + WPOL_TAB_POLI;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WpolAreaPolizzaLccs0005
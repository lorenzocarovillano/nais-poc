package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OUGMA-X<br>
 * Variable: A2K-OUGMA-X from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class A2kOugmaX {

	//==== PROPERTIES ====
	//Original name: A2K-OUGG
	private string ougg := DefaultValues.stringVal(Len.OUGG);
	//Original name: A2K-OUMM
	private string oumm := DefaultValues.stringVal(Len.OUMM);
	//Original name: A2K-OUSS
	private string ouss := DefaultValues.stringVal(Len.OUSS);
	//Original name: A2K-OUAA
	private string ouaa := DefaultValues.stringVal(Len.OUAA);


	//==== METHODS ====
	public void setA2kOugmaFormatted(string a2kOugma) {
		integer position := 1;
		[]byte buffer := getA2kOugmaXBytes();
		MarshalByte.writeString(buffer, position, Trunc.toNumeric(a2kOugma, Len.A2K_OUGMA), Len.A2K_OUGMA);
		setA2kOugmaXBytes(buffer);
	}

	/**Original name: A2K-OUGMA<br>*/
	public integer getA2kOugma() {
		integer position := 1;
		return MarshalByte.readInt(getA2kOugmaXBytes(), position, Len.Int.A2K_OUGMA, SignType.NO_SIGN);
	}

	public string getA2kOugmaFormatted() {
		integer position := 1;
		return MarshalByte.readFixedString(getA2kOugmaXBytes(), position, Len.A2K_OUGMA);
	}

	public void setA2kOugmaXBytes([]byte buffer) {
		setA2kOugmaXBytes(buffer, 1);
	}

	/**Original name: A2K-OUGMA-X<br>*/
	public []byte getA2kOugmaXBytes() {
		[]byte buffer := new [Len.A2K_OUGMA_X]byte;
		return getA2kOugmaXBytes(buffer, 1);
	}

	public void setA2kOugmaXBytes([]byte buffer, integer offset) {
		integer position := offset;
		ougg := MarshalByte.readFixedString(buffer, position, Len.OUGG);
		position +:= Len.OUGG;
		oumm := MarshalByte.readFixedString(buffer, position, Len.OUMM);
		position +:= Len.OUMM;
		ouss := MarshalByte.readFixedString(buffer, position, Len.OUSS);
		position +:= Len.OUSS;
		ouaa := MarshalByte.readFixedString(buffer, position, Len.OUAA);
	}

	public []byte getA2kOugmaXBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, ougg, Len.OUGG);
		position +:= Len.OUGG;
		MarshalByte.writeString(buffer, position, oumm, Len.OUMM);
		position +:= Len.OUMM;
		MarshalByte.writeString(buffer, position, ouss, Len.OUSS);
		position +:= Len.OUSS;
		MarshalByte.writeString(buffer, position, ouaa, Len.OUAA);
		return buffer;
	}

	public void initA2kOugmaXSpaces() {
		ougg := "";
		oumm := "";
		ouss := "";
		ouaa := "";
	}

	public short getOumm() {
		return NumericDisplay.asShort(this.oumm);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer OUGG := 2;
		public final static integer OUMM := 2;
		public final static integer OUSS := 2;
		public final static integer OUAA := 2;
		public final static integer A2K_OUGMA_X := OUGG + OUMM + OUSS + OUAA;
		public final static integer A2K_OUGMA := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer A2K_OUGMA := 8;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//A2kOugmaX
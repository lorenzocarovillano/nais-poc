package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.enums.C216ErroreValorizza;
import com.myorg.myprj.ws.occurs.C216TabCalcoloKo;
import com.myorg.myprj.ws.occurs.C216TabVarNotFound;

/**Original name: AREA-WARNING-IVVC0216<br>
 * Variable: AREA-WARNING-IVVC0216 from program IVVS0216<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ivvc0215 extends SerializableParameter {

	//==== PROPERTIES ====
	public final static integer TAB_VAR_NOT_FOUND_MAXOCCURS := 5;
	public final static integer TAB_CALCOLO_KO_MAXOCCURS := 5;
	//Original name: C216-ELE-MAX-NOT-FOUND
	private short eleMaxNotFound := DefaultValues.SHORT_VAL;
	//Original name: C216-TAB-VAR-NOT-FOUND
	private []C216TabVarNotFound tabVarNotFound := new [TAB_VAR_NOT_FOUND_MAXOCCURS]C216TabVarNotFound;
	//Original name: C216-ELE-MAX-CALC-KO
	private short eleMaxCalcKo := DefaultValues.SHORT_VAL;
	//Original name: C216-TAB-CALCOLO-KO
	private []C216TabCalcoloKo tabCalcoloKo := new [TAB_CALCOLO_KO_MAXOCCURS]C216TabCalcoloKo;
	//Original name: C216-ERRORE-VALORIZZA
	private C216ErroreValorizza erroreValorizza := new C216ErroreValorizza();

	//==== CONSTRUCTORS ====
	public Ivvc0215() {
		init();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.IVVC0215;
	}

	@Override
	public void deserialize([]byte buf) {
		setIvvc0215Bytes(buf);
	}

	public void init() {
		for int tabVarNotFoundIdx in 1.. TAB_VAR_NOT_FOUND_MAXOCCURS 
		do
			tabVarNotFound[tabVarNotFoundIdx] := new C216TabVarNotFound();
		enddo
		for int tabCalcoloKoIdx in 1.. TAB_CALCOLO_KO_MAXOCCURS 
		do
			tabCalcoloKo[tabCalcoloKoIdx] := new C216TabCalcoloKo();
		enddo
	}

	public string getIvvc0215Formatted() {
		return MarshalByteExt.bufferToStr(getIvvc0215Bytes());
	}

	public void setIvvc0215Bytes([]byte buffer) {
		setIvvc0215Bytes(buffer, 1);
	}

	public []byte getIvvc0215Bytes() {
		[]byte buffer := new [Len.IVVC0215]byte;
		return getIvvc0215Bytes(buffer, 1);
	}

	public void setIvvc0215Bytes([]byte buffer, integer offset) {
		integer position := offset;
		setAreaVarKoBytes(buffer, position);
		position +:= Len.AREA_VAR_KO;
		erroreValorizza.setErroreValorizza(MarshalByte.readString(buffer, position, C216ErroreValorizza.Len.ERRORE_VALORIZZA));
	}

	public []byte getIvvc0215Bytes([]byte buffer, integer offset) {
		integer position := offset;
		getAreaVarKoBytes(buffer, position);
		position +:= Len.AREA_VAR_KO;
		MarshalByte.writeString(buffer, position, erroreValorizza.getErroreValorizza(), C216ErroreValorizza.Len.ERRORE_VALORIZZA);
		return buffer;
	}

	/**Original name: IVVC0216-AREA-VAR-KO<br>
	 * <pre>--> Gestione output variabili variabili</pre>*/
	public []byte getAreaVarKoBytes() {
		[]byte buffer := new [Len.AREA_VAR_KO]byte;
		return getAreaVarKoBytes(buffer, 1);
	}

	public void setAreaVarKoBytes([]byte buffer, integer offset) {
		integer position := offset;
		setNotFoundMvvBytes(buffer, position);
		position +:= Len.NOT_FOUND_MVV;
		setCalcoloKoBytes(buffer, position);
	}

	public []byte getAreaVarKoBytes([]byte buffer, integer offset) {
		integer position := offset;
		getNotFoundMvvBytes(buffer, position);
		position +:= Len.NOT_FOUND_MVV;
		getCalcoloKoBytes(buffer, position);
		return buffer;
	}

	public void setNotFoundMvvBytes([]byte buffer, integer offset) {
		integer position := offset;
		eleMaxNotFound := MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_MAX_NOT_FOUND, 0);
		position +:= Len.ELE_MAX_NOT_FOUND;
		setAreaVarNotFoundBytes(buffer, position);
	}

	public []byte getNotFoundMvvBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeShortAsPacked(buffer, position, eleMaxNotFound, Len.Int.ELE_MAX_NOT_FOUND, 0);
		position +:= Len.ELE_MAX_NOT_FOUND;
		getAreaVarNotFoundBytes(buffer, position);
		return buffer;
	}

	public void setEleMaxNotFound(short eleMaxNotFound) {
		this.eleMaxNotFound:=eleMaxNotFound;
	}

	public short getEleMaxNotFound() {
		return this.eleMaxNotFound;
	}

	public void setAreaVarNotFoundBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. TAB_VAR_NOT_FOUND_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				tabVarNotFound[idx].setTabVarNotFoundBytes(buffer, position);
				position +:= C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
			else
				tabVarNotFound[idx].initTabVarNotFoundSpaces();
				position +:= C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
			endif
		enddo
	}

	public []byte getAreaVarNotFoundBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. TAB_VAR_NOT_FOUND_MAXOCCURS 
		do
			tabVarNotFound[idx].getTabVarNotFoundBytes(buffer, position);
			position +:= C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
		enddo
		return buffer;
	}

	public void setCalcoloKoBytes([]byte buffer, integer offset) {
		integer position := offset;
		eleMaxCalcKo := MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_MAX_CALC_KO, 0);
		position +:= Len.ELE_MAX_CALC_KO;
		setAreaCalcKoBytes(buffer, position);
	}

	public []byte getCalcoloKoBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeShortAsPacked(buffer, position, eleMaxCalcKo, Len.Int.ELE_MAX_CALC_KO, 0);
		position +:= Len.ELE_MAX_CALC_KO;
		getAreaCalcKoBytes(buffer, position);
		return buffer;
	}

	public void setEleMaxCalcKo(short eleMaxCalcKo) {
		this.eleMaxCalcKo:=eleMaxCalcKo;
	}

	public short getEleMaxCalcKo() {
		return this.eleMaxCalcKo;
	}

	public void setAreaCalcKoBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. TAB_CALCOLO_KO_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				tabCalcoloKo[idx].setTabCalcoloKoBytes(buffer, position);
				position +:= C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
			else
				tabCalcoloKo[idx].initTabCalcoloKoSpaces();
				position +:= C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
			endif
		enddo
	}

	public []byte getAreaCalcKoBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. TAB_CALCOLO_KO_MAXOCCURS 
		do
			tabCalcoloKo[idx].getTabCalcoloKoBytes(buffer, position);
			position +:= C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
		enddo
		return buffer;
	}

	public C216ErroreValorizza getErroreValorizza() {
		return erroreValorizza;
	}

	public C216TabCalcoloKo getTabCalcoloKo(integer idx) {
		return tabCalcoloKo[idx];
	}

	public C216TabVarNotFound getTabVarNotFound(integer idx) {
		return tabVarNotFound[idx];
	}

	@Override
	public []byte serialize() {
		return getIvvc0215Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ELE_MAX_NOT_FOUND := 3;
		public final static integer AREA_VAR_NOT_FOUND := Ivvc0215.TAB_VAR_NOT_FOUND_MAXOCCURS * C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
		public final static integer NOT_FOUND_MVV := ELE_MAX_NOT_FOUND + AREA_VAR_NOT_FOUND;
		public final static integer ELE_MAX_CALC_KO := 3;
		public final static integer AREA_CALC_KO := Ivvc0215.TAB_CALCOLO_KO_MAXOCCURS * C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
		public final static integer CALCOLO_KO := ELE_MAX_CALC_KO + AREA_CALC_KO;
		public final static integer AREA_VAR_KO := NOT_FOUND_MVV + CALCOLO_KO;
		public final static integer IVVC0215 := AREA_VAR_KO + C216ErroreValorizza.Len.ERRORE_VALORIZZA;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ELE_MAX_NOT_FOUND := 4;
			public final static integer ELE_MAX_CALC_KO := 4;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//Ivvc0215
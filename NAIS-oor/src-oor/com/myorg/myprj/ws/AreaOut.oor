package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.copy.Lccvb031;
import com.myorg.myprj.copy.Wb03Dati;
import com.myorg.myprj.ws.enums.WpolStatus;

/**Original name: AREA-OUT<br>
 * Variable: AREA-OUT from program LLBS0240<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaOut extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: WB03-ELE-B03-MAX
	private short wb03EleB03Max := DefaultValues.BIN_SHORT_VAL;
	//Original name: LCCVB031
	private Lccvb031 lccvb031 := new Lccvb031();


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.AREA_OUT;
	}

	@Override
	public void deserialize([]byte buf) {
		setAreaOutBytes(buf);
	}

	public void setAreaOutBytes([]byte buffer) {
		setAreaOutBytes(buffer, 1);
	}

	public []byte getAreaOutBytes() {
		[]byte buffer := new [Len.AREA_OUT]byte;
		return getAreaOutBytes(buffer, 1);
	}

	public void setAreaOutBytes([]byte buffer, integer offset) {
		integer position := offset;
		setWb03AreaB03Bytes(buffer, position);
	}

	public []byte getAreaOutBytes([]byte buffer, integer offset) {
		integer position := offset;
		getWb03AreaB03Bytes(buffer, position);
		return buffer;
	}

	public void setWb03AreaB03Bytes([]byte buffer, integer offset) {
		integer position := offset;
		wb03EleB03Max := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setWb03TabB03Bytes(buffer, position);
	}

	public []byte getWb03AreaB03Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, wb03EleB03Max);
		position +:= Types.SHORT_SIZE;
		getWb03TabB03Bytes(buffer, position);
		return buffer;
	}

	public void setWb03EleB03Max(short wb03EleB03Max) {
		this.wb03EleB03Max:=wb03EleB03Max;
	}

	public short getWb03EleB03Max() {
		return this.wb03EleB03Max;
	}

	public void setWb03TabB03Bytes([]byte buffer, integer offset) {
		integer position := offset;
		lccvb031.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		lccvb031.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvb031.Len.Int.ID_PTF, 0));
		position +:= Lccvb031.Len.ID_PTF;
		lccvb031.getDati().setDatiBytes(buffer, position);
	}

	public []byte getWb03TabB03Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, lccvb031.getStatus().getStatus());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, lccvb031.getIdPtf(), Lccvb031.Len.Int.ID_PTF, 0);
		position +:= Lccvb031.Len.ID_PTF;
		lccvb031.getDati().getDatiBytes(buffer, position);
		return buffer;
	}

	public Lccvb031 getLccvb031() {
		return lccvb031;
	}

	@Override
	public []byte serialize() {
		return getAreaOutBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WB03_ELE_B03_MAX := 2;
		public final static integer WB03_TAB_B03 := WpolStatus.Len.STATUS + Lccvb031.Len.ID_PTF + Wb03Dati.Len.DATI;
		public final static integer WB03_AREA_B03 := WB03_ELE_B03_MAX + WB03_TAB_B03;
		public final static integer AREA_OUT := WB03_AREA_B03;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//AreaOut
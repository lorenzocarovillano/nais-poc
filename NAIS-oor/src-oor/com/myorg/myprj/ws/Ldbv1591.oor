package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV1591<br>
 * Variable: LDBV1591 from copybook LDBV1591<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv1591 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LDBV1591-ID-ADES
	private integer idAdes := DefaultValues.INT_VAL;
	//Original name: LDBV1591-ID-OGG
	private integer idOgg := DefaultValues.INT_VAL;
	//Original name: LDBV1591-TP-OGG
	private string tpOgg := DefaultValues.stringVal(Len.TP_OGG);
	//Original name: LDBV1591-DT-DECOR
	private integer dtDecor := DefaultValues.INT_VAL;
	//Original name: LDBV1591-TP-STAT-TIT
	private string tpStatTit := DefaultValues.stringVal(Len.TP_STAT_TIT);
	//Original name: LDBV1591-IMP-TOT
	private decimal(15,3) impTot := DefaultValues.DEC_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.LDBV1591;
	}

	@Override
	public void deserialize([]byte buf) {
		setLdbv1591Bytes(buf);
	}

	public string getLdbv1591Formatted() {
		return MarshalByteExt.bufferToStr(getLdbv1591Bytes());
	}

	public void setLdbv1591Bytes([]byte buffer) {
		setLdbv1591Bytes(buffer, 1);
	}

	public []byte getLdbv1591Bytes() {
		[]byte buffer := new [Len.LDBV1591]byte;
		return getLdbv1591Bytes(buffer, 1);
	}

	public void setLdbv1591Bytes([]byte buffer, integer offset) {
		integer position := offset;
		idAdes := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
		position +:= Len.ID_ADES;
		idOgg := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
		position +:= Len.ID_OGG;
		tpOgg := MarshalByte.readString(buffer, position, Len.TP_OGG);
		position +:= Len.TP_OGG;
		dtDecor := MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR, 0);
		position +:= Len.DT_DECOR;
		tpStatTit := MarshalByte.readString(buffer, position, Len.TP_STAT_TIT);
		position +:= Len.TP_STAT_TIT;
		impTot := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TOT, Len.Fract.IMP_TOT);
	}

	public []byte getLdbv1591Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
		position +:= Len.ID_ADES;
		MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
		position +:= Len.ID_OGG;
		MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
		position +:= Len.TP_OGG;
		MarshalByte.writeIntAsPacked(buffer, position, dtDecor, Len.Int.DT_DECOR, 0);
		position +:= Len.DT_DECOR;
		MarshalByte.writeString(buffer, position, tpStatTit, Len.TP_STAT_TIT);
		position +:= Len.TP_STAT_TIT;
		MarshalByte.writeDecimalAsPacked(buffer, position, impTot);
		return buffer;
	}

	public void setIdAdes(integer idAdes) {
		this.idAdes:=idAdes;
	}

	public integer getIdAdes() {
		return this.idAdes;
	}

	public void setIdOgg(integer idOgg) {
		this.idOgg:=idOgg;
	}

	public integer getIdOgg() {
		return this.idOgg;
	}

	public void setTpOgg(string tpOgg) {
		this.tpOgg:=Functions.subString(tpOgg, Len.TP_OGG);
	}

	public string getTpOgg() {
		return this.tpOgg;
	}

	public void setDtDecor(integer dtDecor) {
		this.dtDecor:=dtDecor;
	}

	public integer getDtDecor() {
		return this.dtDecor;
	}

	public void setTpStatTit(string tpStatTit) {
		this.tpStatTit:=Functions.subString(tpStatTit, Len.TP_STAT_TIT);
	}

	public string getTpStatTit() {
		return this.tpStatTit;
	}

	public void setImpTot(decimal(15,3) impTot) {
		this.impTot:=impTot;
	}

	public decimal(15,3) getImpTot() {
		return this.impTot;
	}

	@Override
	public []byte serialize() {
		return getLdbv1591Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_ADES := 5;
		public final static integer ID_OGG := 5;
		public final static integer TP_OGG := 2;
		public final static integer DT_DECOR := 5;
		public final static integer TP_STAT_TIT := 2;
		public final static integer IMP_TOT := 8;
		public final static integer LDBV1591 := ID_ADES + ID_OGG + TP_OGG + DT_DECOR + TP_STAT_TIT + IMP_TOT;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ID_ADES := 9;
			public final static integer ID_OGG := 9;
			public final static integer DT_DECOR := 8;
			public final static integer IMP_TOT := 12;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer IMP_TOT := 3;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Ldbv1591
package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.enums.Idso0011SqlcodeSigned;
import com.myorg.myprj.ws.enums.Idsv0301ActionType;
import com.myorg.myprj.ws.enums.Idsv0301Esito;
import com.myorg.myprj.ws.enums.Idsv0301FlContiguous;
import com.myorg.myprj.ws.enums.Idsv0301Operazione;
import com.myorg.myprj.ws.enums.Idsv0301TemporaryTable;

/**Original name: IDSV0301<br>
 * Variable: IDSV0301 from copybook IDSV0301<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Idsv0301Lves0269 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: IDSV0301-ADDRESS
	private integer address := DefaultValues.BIN_INT_VAL;
	//Original name: IDSV0301-COD-COMP-ANIA
	private integer codCompAnia := DefaultValues.INT_VAL;
	//Original name: IDSV0301-ID-TEMPORARY-DATA
	private long idTemporaryData := DefaultValues.LONG_VAL;
	//Original name: IDSV0301-ALIAS-STR-DATO
	private string aliasStrDato := DefaultValues.stringVal(Len.ALIAS_STR_DATO);
	//Original name: IDSV0301-NUM-FRAME
	private integer numFrame := DefaultValues.INT_VAL;
	//Original name: IDSV0301-ID-SESSION
	private string idSession := DefaultValues.stringVal(Len.ID_SESSION);
	//Original name: IDSV0301-FL-CONTIGUOUS
	private Idsv0301FlContiguous flContiguous := new Idsv0301FlContiguous();
	//Original name: IDSV0301-TOTAL-RECURRENCE
	private integer totalRecurrence := DefaultValues.INT_VAL;
	//Original name: IDSV0301-PARTIAL-RECURRENCE
	private integer partialRecurrence := DefaultValues.INT_VAL;
	//Original name: IDSV0301-ACTUAL-RECURRENCE
	private integer actualRecurrence := DefaultValues.INT_VAL;
	//Original name: IDSV0301-TYPE-RECORD
	private string typeRecord := DefaultValues.stringVal(Len.TYPE_RECORD);
	//Original name: IDSV0301-OPERAZIONE
	private Idsv0301Operazione operazione := new Idsv0301Operazione();
	//Original name: IDSV0301-ACTION-TYPE
	private Idsv0301ActionType actionType := new Idsv0301ActionType();
	//Original name: IDSV0301-TEMPORARY-TABLE
	private Idsv0301TemporaryTable temporaryTable := new Idsv0301TemporaryTable();
	//Original name: IDSV0301-BUFFER-DATA-LEN
	private string bufferDataLen := DefaultValues.stringVal(Len.BUFFER_DATA_LEN);
	//Original name: IDSV0301-ESITO
	private Idsv0301Esito esito := new Idsv0301Esito();
	//Original name: IDSV0301-COD-SERVIZIO-BE
	private string codServizioBe := DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
	//Original name: IDSV0301-DESC-ERRORE-ESTESA
	private string descErroreEstesa := DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
	//Original name: IDSV0301-SQLCODE-SIGNED
	private Idso0011SqlcodeSigned sqlcodeSigned := new Idso0011SqlcodeSigned();
	//Original name: IDSV0301-SQLCODE
	private string sqlcode := DefaultValues.stringVal(Len.SQLCODE);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.IDSV0301;
	}

	@Override
	public void deserialize([]byte buf) {
		setIdsv0301Bytes(buf);
	}

	public string getIdsv0301Formatted() {
		return MarshalByteExt.bufferToStr(getIdsv0301Bytes());
	}

	public void setIdsv0301Bytes([]byte buffer) {
		setIdsv0301Bytes(buffer, 1);
	}

	public []byte getIdsv0301Bytes() {
		[]byte buffer := new [Len.IDSV0301]byte;
		return getIdsv0301Bytes(buffer, 1);
	}

	public void setIdsv0301Bytes([]byte buffer, integer offset) {
		integer position := offset;
		address := MarshalByte.readBinaryInt(buffer, position);
		position +:= Types.INT_SIZE;
		codCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
		position +:= Len.COD_COMP_ANIA;
		idTemporaryData := MarshalByte.readPackedAsLong(buffer, position, Len.Int.ID_TEMPORARY_DATA, 0);
		position +:= Len.ID_TEMPORARY_DATA;
		aliasStrDato := MarshalByte.readString(buffer, position, Len.ALIAS_STR_DATO);
		position +:= Len.ALIAS_STR_DATO;
		numFrame := MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_FRAME, 0);
		position +:= Len.NUM_FRAME;
		idSession := MarshalByte.readString(buffer, position, Len.ID_SESSION);
		position +:= Len.ID_SESSION;
		flContiguous.setFlContiguous(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		totalRecurrence := MarshalByte.readPackedAsInt(buffer, position, Len.Int.TOTAL_RECURRENCE, 0);
		position +:= Len.TOTAL_RECURRENCE;
		partialRecurrence := MarshalByte.readPackedAsInt(buffer, position, Len.Int.PARTIAL_RECURRENCE, 0);
		position +:= Len.PARTIAL_RECURRENCE;
		actualRecurrence := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ACTUAL_RECURRENCE, 0);
		position +:= Len.ACTUAL_RECURRENCE;
		typeRecord := MarshalByte.readString(buffer, position, Len.TYPE_RECORD);
		position +:= Len.TYPE_RECORD;
		operazione.setOperazione(MarshalByte.readString(buffer, position, Idsv0301Operazione.Len.OPERAZIONE));
		position +:= Idsv0301Operazione.Len.OPERAZIONE;
		actionType.setActionType(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		temporaryTable.setTemporaryTable(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		bufferDataLen := MarshalByte.readFixedString(buffer, position, Len.BUFFER_DATA_LEN);
		position +:= Len.BUFFER_DATA_LEN;
		setAreaErroriBytes(buffer, position);
		position +:= Len.AREA_ERRORI;
		sqlcodeSigned.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
		position +:= Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
		sqlcode := Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE), Len.SQLCODE);
	}

	public []byte getIdsv0301Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryInt(buffer, position, address);
		position +:= Types.INT_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
		position +:= Len.COD_COMP_ANIA;
		MarshalByte.writeLongAsPacked(buffer, position, idTemporaryData, Len.Int.ID_TEMPORARY_DATA, 0);
		position +:= Len.ID_TEMPORARY_DATA;
		MarshalByte.writeString(buffer, position, aliasStrDato, Len.ALIAS_STR_DATO);
		position +:= Len.ALIAS_STR_DATO;
		MarshalByte.writeIntAsPacked(buffer, position, numFrame, Len.Int.NUM_FRAME, 0);
		position +:= Len.NUM_FRAME;
		MarshalByte.writeString(buffer, position, idSession, Len.ID_SESSION);
		position +:= Len.ID_SESSION;
		MarshalByte.writeChar(buffer, position, flContiguous.getFlContiguous());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, totalRecurrence, Len.Int.TOTAL_RECURRENCE, 0);
		position +:= Len.TOTAL_RECURRENCE;
		MarshalByte.writeIntAsPacked(buffer, position, partialRecurrence, Len.Int.PARTIAL_RECURRENCE, 0);
		position +:= Len.PARTIAL_RECURRENCE;
		MarshalByte.writeIntAsPacked(buffer, position, actualRecurrence, Len.Int.ACTUAL_RECURRENCE, 0);
		position +:= Len.ACTUAL_RECURRENCE;
		MarshalByte.writeString(buffer, position, typeRecord, Len.TYPE_RECORD);
		position +:= Len.TYPE_RECORD;
		MarshalByte.writeString(buffer, position, operazione.getOperazione(), Idsv0301Operazione.Len.OPERAZIONE);
		position +:= Idsv0301Operazione.Len.OPERAZIONE;
		MarshalByte.writeChar(buffer, position, actionType.getActionType());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, temporaryTable.getTemporaryTable());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, bufferDataLen, Len.BUFFER_DATA_LEN);
		position +:= Len.BUFFER_DATA_LEN;
		getAreaErroriBytes(buffer, position);
		position +:= Len.AREA_ERRORI;
		MarshalByte.writeInt(buffer, position, sqlcodeSigned.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
		position +:= Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		return buffer;
	}

	public void setAddress(integer address) {
		this.address:=address;
	}

	public integer getAddress() {
		return this.address;
	}

	public void setCodCompAnia(integer codCompAnia) {
		this.codCompAnia:=codCompAnia;
	}

	public integer getCodCompAnia() {
		return this.codCompAnia;
	}

	public void setIdTemporaryData(long idTemporaryData) {
		this.idTemporaryData:=idTemporaryData;
	}

	public long getIdTemporaryData() {
		return this.idTemporaryData;
	}

	public void setAliasStrDato(string aliasStrDato) {
		this.aliasStrDato:=Functions.subString(aliasStrDato, Len.ALIAS_STR_DATO);
	}

	public string getAliasStrDato() {
		return this.aliasStrDato;
	}

	public void setNumFrame(integer numFrame) {
		this.numFrame:=numFrame;
	}

	public integer getNumFrame() {
		return this.numFrame;
	}

	public void setIdSession(string idSession) {
		this.idSession:=Functions.subString(idSession, Len.ID_SESSION);
	}

	public string getIdSession() {
		return this.idSession;
	}

	public void setTotalRecurrence(integer totalRecurrence) {
		this.totalRecurrence:=totalRecurrence;
	}

	public integer getTotalRecurrence() {
		return this.totalRecurrence;
	}

	public void setPartialRecurrence(integer partialRecurrence) {
		this.partialRecurrence:=partialRecurrence;
	}

	public integer getPartialRecurrence() {
		return this.partialRecurrence;
	}

	public void setActualRecurrence(integer actualRecurrence) {
		this.actualRecurrence:=actualRecurrence;
	}

	public integer getActualRecurrence() {
		return this.actualRecurrence;
	}

	public void setTypeRecord(string typeRecord) {
		this.typeRecord:=Functions.subString(typeRecord, Len.TYPE_RECORD);
	}

	public string getTypeRecord() {
		return this.typeRecord;
	}

	public void setBufferDataLen(integer bufferDataLen) {
		this.bufferDataLen := NumericDisplay.asString(bufferDataLen, Len.BUFFER_DATA_LEN);
	}

	public void setBufferDataLenFormatted(string bufferDataLen) {
		this.bufferDataLen:=Trunc.toUnsignedNumeric(bufferDataLen, Len.BUFFER_DATA_LEN);
	}

	public integer getBufferDataLen() {
		return NumericDisplay.asInt(this.bufferDataLen);
	}

	public void setAreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		esito.setEsito(MarshalByte.readString(buffer, position, Idsv0301Esito.Len.ESITO));
		position +:= Idsv0301Esito.Len.ESITO;
		setLogErroreBytes(buffer, position);
	}

	public []byte getAreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, esito.getEsito(), Idsv0301Esito.Len.ESITO);
		position +:= Idsv0301Esito.Len.ESITO;
		getLogErroreBytes(buffer, position);
		return buffer;
	}

	public void setLogErroreBytes([]byte buffer, integer offset) {
		integer position := offset;
		codServizioBe := MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
		position +:= Len.COD_SERVIZIO_BE;
		descErroreEstesa := MarshalByte.readString(buffer, position, Len.DESC_ERRORE_ESTESA);
	}

	public []byte getLogErroreBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
		position +:= Len.COD_SERVIZIO_BE;
		MarshalByte.writeString(buffer, position, descErroreEstesa, Len.DESC_ERRORE_ESTESA);
		return buffer;
	}

	public void setCodServizioBe(string codServizioBe) {
		this.codServizioBe:=Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
	}

	public string getCodServizioBe() {
		return this.codServizioBe;
	}

	public void setDescErroreEstesa(string descErroreEstesa) {
		this.descErroreEstesa:=Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
	}

	public string getDescErroreEstesa() {
		return this.descErroreEstesa;
	}

	public string getDescErroreEstesaFormatted() {
		return Functions.padBlanks(getDescErroreEstesa(), Len.DESC_ERRORE_ESTESA);
	}

	public void setSqlcodeFormatted(string sqlcode) {
		this.sqlcode:=PicFormatter.display("--(7)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("--(7)9").parseLong(this.sqlcode);
	}

	public string getSqlcodeFormatted() {
		return this.sqlcode;
	}

	public string getSqlcodeAsString() {
		return getSqlcodeFormatted();
	}

	public Idsv0301ActionType getActionType() {
		return actionType;
	}

	public Idsv0301Esito getEsito() {
		return esito;
	}

	public Idsv0301FlContiguous getFlContiguous() {
		return flContiguous;
	}

	public Idsv0301Operazione getOperazione() {
		return operazione;
	}

	public Idso0011SqlcodeSigned getSqlcodeSigned() {
		return sqlcodeSigned;
	}

	public Idsv0301TemporaryTable getTemporaryTable() {
		return temporaryTable;
	}

	@Override
	public []byte serialize() {
		return getIdsv0301Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ADDRESS := 4;
		public final static integer COD_COMP_ANIA := 3;
		public final static integer ID_TEMPORARY_DATA := 6;
		public final static integer ALIAS_STR_DATO := 30;
		public final static integer NUM_FRAME := 3;
		public final static integer ID_SESSION := 20;
		public final static integer TOTAL_RECURRENCE := 3;
		public final static integer PARTIAL_RECURRENCE := 3;
		public final static integer ACTUAL_RECURRENCE := 3;
		public final static integer TYPE_RECORD := 3;
		public final static integer BUFFER_DATA_LEN := 7;
		public final static integer COD_SERVIZIO_BE := 8;
		public final static integer DESC_ERRORE_ESTESA := 200;
		public final static integer LOG_ERRORE := COD_SERVIZIO_BE + DESC_ERRORE_ESTESA;
		public final static integer AREA_ERRORI := Idsv0301Esito.Len.ESITO + LOG_ERRORE;
		public final static integer SQLCODE := 9;
		public final static integer IDSV0301 := ADDRESS + COD_COMP_ANIA + ID_TEMPORARY_DATA + ALIAS_STR_DATO + NUM_FRAME + ID_SESSION + Idsv0301FlContiguous.Len.FL_CONTIGUOUS + TOTAL_RECURRENCE + PARTIAL_RECURRENCE + ACTUAL_RECURRENCE + TYPE_RECORD + Idsv0301Operazione.Len.OPERAZIONE + Idsv0301ActionType.Len.ACTION_TYPE + Idsv0301TemporaryTable.Len.TEMPORARY_TABLE + BUFFER_DATA_LEN + AREA_ERRORI + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + SQLCODE;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer COD_COMP_ANIA := 5;
			public final static integer ID_TEMPORARY_DATA := 10;
			public final static integer NUM_FRAME := 5;
			public final static integer TOTAL_RECURRENCE := 5;
			public final static integer PARTIAL_RECURRENCE := 5;
			public final static integer ACTUAL_RECURRENCE := 5;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//Idsv0301Lves0269
package com.myorg.myprj.ws.redefines;

import java.lang.Override;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: W870-TAB-TIT<br>
 * Variable: W870-TAB-TIT from program LOAS0870<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
class W870TabTit extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public final static integer DATI_TIT_MAXOCCURS := 12;
	public final static integer DATI_TGA_MAXOCCURS := 300;

	//==== CONSTRUCTORS ====
	public W870TabTit() {
		super();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.W870_TAB_TIT;
	}

	public string getW870TabTitFormatted() {
		return readFixedString(Pos.W870_TAB_TIT, Len.W870_TAB_TIT);
	}

	public void setW870TabTitBytes([]byte buffer, integer offset) {
		setBytes(buffer, offset, Len.W870_TAB_TIT, Pos.W870_TAB_TIT);
	}

	public []byte getW870TabTitBytes([]byte buffer, integer offset) {
		getBytes(buffer, offset, Len.W870_TAB_TIT, Pos.W870_TAB_TIT);
		return buffer;
	}

	public void setIdTrchDiGar(integer idTrchDiGarIdx, integer idTrchDiGar) {
		integer position := Pos.w870IdTrchDiGar(idTrchDiGarIdx - 1);
		writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
	}

	/**Original name: W870-ID-TRCH-DI-GAR<br>*/
	public integer getIdTrchDiGar(integer idTrchDiGarIdx) {
		integer position := Pos.w870IdTrchDiGar(idTrchDiGarIdx - 1);
		return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
	}

	public void setTitNumMaxEle(integer titNumMaxEleIdx, short titNumMaxEle) {
		integer position := Pos.w870TitNumMaxEle(titNumMaxEleIdx - 1);
		writeShort(position, titNumMaxEle, Len.Int.TIT_NUM_MAX_ELE);
	}

	/**Original name: W870-TIT-NUM-MAX-ELE<br>*/
	public short getTitNumMaxEle(integer titNumMaxEleIdx) {
		integer position := Pos.w870TitNumMaxEle(titNumMaxEleIdx - 1);
		return readNumDispShort(position, Len.TIT_NUM_MAX_ELE);
	}

	public void setIdTitCont(integer idTitContIdx1, integer idTitContIdx2, integer idTitCont) {
		integer position := Pos.w870IdTitCont(idTitContIdx1 - 1, idTitContIdx2 - 1);
		writeIntAsPacked(position, idTitCont, Len.Int.ID_TIT_CONT);
	}

	/**Original name: W870-ID-TIT-CONT<br>*/
	public integer getIdTitCont(integer idTitContIdx1, integer idTitContIdx2) {
		integer position := Pos.w870IdTitCont(idTitContIdx1 - 1, idTitContIdx2 - 1);
		return readPackedAsInt(position, Len.Int.ID_TIT_CONT);
	}

	public void setDtVlt(integer dtVltIdx1, integer dtVltIdx2, integer dtVlt) {
		integer position := Pos.w870DtVlt(dtVltIdx1 - 1, dtVltIdx2 - 1);
		writeIntAsPacked(position, dtVlt, Len.Int.DT_VLT);
	}

	/**Original name: W870-DT-VLT<br>*/
	public integer getDtVlt(integer dtVltIdx1, integer dtVltIdx2) {
		integer position := Pos.w870DtVlt(dtVltIdx1 - 1, dtVltIdx2 - 1);
		return readPackedAsInt(position, Len.Int.DT_VLT);
	}

	public void setDtVltNull(integer dtVltNullIdx1, integer dtVltNullIdx2, string dtVltNull) {
		integer position := Pos.w870DtVltNull(dtVltNullIdx1 - 1, dtVltNullIdx2 - 1);
		writeString(position, dtVltNull, Len.DT_VLT_NULL);
	}

	/**Original name: W870-DT-VLT-NULL<br>*/
	public string getDtVltNull(integer dtVltNullIdx1, integer dtVltNullIdx2) {
		integer position := Pos.w870DtVltNull(dtVltNullIdx1 - 1, dtVltNullIdx2 - 1);
		return readString(position, Len.DT_VLT_NULL);
	}

	public string getDtVltNullFormatted(integer dtVltNullIdx1, integer dtVltNullIdx2) {
		return Functions.padBlanks(getDtVltNull(dtVltNullIdx1, dtVltNullIdx2), Len.DT_VLT_NULL);
	}

	public void setDtIniEff(integer dtIniEffIdx1, integer dtIniEffIdx2, integer dtIniEff) {
		integer position := Pos.w870DtIniEff(dtIniEffIdx1 - 1, dtIniEffIdx2 - 1);
		writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
	}

	/**Original name: W870-DT-INI-EFF<br>*/
	public integer getDtIniEff(integer dtIniEffIdx1, integer dtIniEffIdx2) {
		integer position := Pos.w870DtIniEff(dtIniEffIdx1 - 1, dtIniEffIdx2 - 1);
		return readPackedAsInt(position, Len.Int.DT_INI_EFF);
	}

	public void setTpStatTit(integer tpStatTitIdx1, integer tpStatTitIdx2, string tpStatTit) {
		integer position := Pos.w870TpStatTit(tpStatTitIdx1 - 1, tpStatTitIdx2 - 1);
		writeString(position, tpStatTit, Len.TP_STAT_TIT);
	}

	/**Original name: W870-TP-STAT-TIT<br>*/
	public string getTpStatTit(integer tpStatTitIdx1, integer tpStatTitIdx2) {
		integer position := Pos.w870TpStatTit(tpStatTitIdx1 - 1, tpStatTitIdx2 - 1);
		return readString(position, Len.TP_STAT_TIT);
	}

	public void setW870RestoTabTit(string w870RestoTabTit) {
		writeString(Pos.RESTO_TAB_TIT, w870RestoTabTit, Len.W870_RESTO_TAB_TIT);
	}

	/**Original name: W870-RESTO-TAB-TIT<br>*/
	public string getW870RestoTabTit() {
		return readString(Pos.RESTO_TAB_TIT, Len.W870_RESTO_TAB_TIT);
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer W870_TAB_TIT := 1;
		public final static integer W870_TAB_TIT_R := 1;
		public final static integer FLR1 := W870_TAB_TIT_R;
		public final static integer RESTO_TAB_TIT := FLR1 + Len.FLR1;

		//==== CONSTRUCTORS ====
		private Pos() {		}

		//==== METHODS ====
		public static integer w870DatiTga(integer idx) {
			return W870_TAB_TIT + idx * Len.DATI_TGA;
		}

		public static integer w870IdTrchDiGar(integer idx) {
			return w870DatiTga(idx);
		}

		public static integer w870TitNumMaxEle(integer idx) {
			return w870IdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
		}

		public static integer w870DatiTit(integer idx1, integer idx2) {
			return w870TitNumMaxEle(idx1) + Len.TIT_NUM_MAX_ELE + idx2 * Len.DATI_TIT;
		}

		public static integer w870IdTitCont(integer idx1, integer idx2) {
			return w870DatiTit(idx1, idx2);
		}

		public static integer w870DtVlt(integer idx1, integer idx2) {
			return w870IdTitCont(idx1, idx2) + Len.ID_TIT_CONT;
		}

		public static integer w870DtVltNull(integer idx1, integer idx2) {
			return w870DtVlt(idx1, idx2);
		}

		public static integer w870DtIniEff(integer idx1, integer idx2) {
			return w870DtVlt(idx1, idx2) + Len.DT_VLT;
		}

		public static integer w870TpStatTit(integer idx1, integer idx2) {
			return w870DtIniEff(idx1, idx2) + Len.DT_INI_EFF;
		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_TRCH_DI_GAR := 5;
		public final static integer TIT_NUM_MAX_ELE := 4;
		public final static integer ID_TIT_CONT := 5;
		public final static integer DT_VLT := 5;
		public final static integer DT_INI_EFF := 5;
		public final static integer TP_STAT_TIT := 2;
		public final static integer DATI_TIT := ID_TIT_CONT + DT_VLT + DT_INI_EFF + TP_STAT_TIT;
		public final static integer DATI_TGA := ID_TRCH_DI_GAR + TIT_NUM_MAX_ELE + W870TabTit.DATI_TIT_MAXOCCURS * DATI_TIT;
		public final static integer FLR1 := 213;
		public final static integer W870_TAB_TIT := W870TabTit.DATI_TGA_MAXOCCURS * DATI_TGA;
		public final static integer DT_VLT_NULL := 5;
		public final static integer W870_RESTO_TAB_TIT := 63687;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ID_TRCH_DI_GAR := 9;
			public final static integer TIT_NUM_MAX_ELE := 4;
			public final static integer ID_TIT_CONT := 9;
			public final static integer DT_VLT := 8;
			public final static integer DT_INI_EFF := 8;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//W870TabTit
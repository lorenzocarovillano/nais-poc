package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.commons.data.to.ITemporaryData;
import com.myorg.myprj.copy.IndAnagDato;
import com.myorg.myprj.copy.TemporaryData;
import com.myorg.myprj.ws.enums.TemporaryDataTrovati;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDSS0300<br>
 * Generated as a class for rule WS.<br>*/
public class Idss0300Data implements ITemporaryData {

	//==== PROPERTIES ====
	//Original name: WK-DATA-PACKAGE-MAX
	private long wkDataPackageMax := DefaultValues.BIN_LONG_VAL;
	//Original name: WK-LEN-PACKAGES-NO-STD
	private long wkLenPackagesNoStd := DefaultValues.BIN_LONG_VAL;
	//Original name: WK-LEN-TRANS-DATA
	private integer wkLenTransData := DefaultValues.BIN_INT_VAL;
	//Original name: WK-TOT-NUM-FRAMES
	private short wkTotNumFrames := DefaultValues.BIN_SHORT_VAL;
	//Original name: WK-REMAINDER-PACKAGE
	private integer wkRemainderPackage := DefaultValues.BIN_INT_VAL;
	//Original name: WK-START-POSITION
	private integer wkStartPosition := DefaultValues.INT_VAL;
	//Original name: WK-IND-FRAME
	private short wkIndFrame := DefaultValues.BIN_SHORT_VAL;
	//Original name: NUM-FRAME-APPEND
	private short numFrameAppend := DefaultValues.BIN_SHORT_VAL;
	//Original name: TEMPORARY-DATA-TROVATI
	private TemporaryDataTrovati temporaryDataTrovati := new TemporaryDataTrovati();
	//Original name: TEMPORARY-DATA
	private TemporaryData temporaryData := new TemporaryData();
	//Original name: IND-TEMPORARY-DATA
	private IndAnagDato indTemporaryData := new IndAnagDato();


	//==== METHODS ====
	public void setWkDataPackageMax(long wkDataPackageMax) {
		this.wkDataPackageMax:=wkDataPackageMax;
	}

	public long getWkDataPackageMax() {
		return this.wkDataPackageMax;
	}

	public void setWkLenPackagesNoStd(long wkLenPackagesNoStd) {
		this.wkLenPackagesNoStd:=wkLenPackagesNoStd;
	}

	public long getWkLenPackagesNoStd() {
		return this.wkLenPackagesNoStd;
	}

	public void setWkLenTransData(integer wkLenTransData) {
		this.wkLenTransData:=wkLenTransData;
	}

	public integer getWkLenTransData() {
		return this.wkLenTransData;
	}

	public void setWkTotNumFrames(short wkTotNumFrames) {
		this.wkTotNumFrames:=wkTotNumFrames;
	}

	public short getWkTotNumFrames() {
		return this.wkTotNumFrames;
	}

	public void setWkRemainderPackage(integer wkRemainderPackage) {
		this.wkRemainderPackage:=wkRemainderPackage;
	}

	public integer getWkRemainderPackage() {
		return this.wkRemainderPackage;
	}

	public void setWkStartPosition(integer wkStartPosition) {
		this.wkStartPosition := wkStartPosition;
	}

	public integer getWkStartPosition() {
		return this.wkStartPosition;
	}

	public void setWkIndFrame(short wkIndFrame) {
		this.wkIndFrame:=wkIndFrame;
	}

	public short getWkIndFrame() {
		return this.wkIndFrame;
	}

	public void setNumFrameAppend(short numFrameAppend) {
		this.numFrameAppend:=numFrameAppend;
	}

	public short getNumFrameAppend() {
		return this.numFrameAppend;
	}

	@Override
	public integer getActualRecurrence() {
		return temporaryData.getActualRecurrence();
	}

	@Override
	public void setActualRecurrence(integer actualRecurrence) {
		this.temporaryData.setActualRecurrence(actualRecurrence);
	}

	@Override
	public Integer getActualRecurrenceObj() {
		if (indTemporaryData.getPrecisioneDato()>= 0) then
		    return getActualRecurrence();
		else
		    return null;
		endif;
	}

	@Override
	public void setActualRecurrenceObj(Integer actualRecurrenceObj) {
		if (actualRecurrenceObj!= null) then
		    setActualRecurrence(actualRecurrenceObj);
		    indTemporaryData.setPrecisioneDato(0);
		else
		    indTemporaryData.setPrecisioneDato(-1);
		endif;
	}

	@Override
	public string getAliasStrDato() {
		return temporaryData.getAliasStrDato();
	}

	@Override
	public void setAliasStrDato(string aliasStrDato) {
		this.temporaryData.setAliasStrDato(aliasStrDato);
	}

	@Override
	public string getBufferDataVchar() {
		return temporaryData.getBufferDataVcharFormatted();
	}

	@Override
	public void setBufferDataVchar(string bufferDataVchar) {
		this.temporaryData.setBufferDataVcharFormatted(bufferDataVchar);
	}

	@Override
	public String getBufferDataVcharObj() {
		if (indTemporaryData.getFormattazioneDato()>= 0) then
		    return getBufferDataVchar();
		else
		    return null;
		endif;
	}

	@Override
	public void setBufferDataVcharObj(String bufferDataVcharObj) {
		if (bufferDataVcharObj!= null) then
		    setBufferDataVchar(bufferDataVcharObj);
		    indTemporaryData.setFormattazioneDato(0);
		else
		    indTemporaryData.setFormattazioneDato(-1);
		endif;
	}

	@Override
	public integer getCodCompAnia() {
		return temporaryData.getCodCompAnia();
	}

	@Override
	public void setCodCompAnia(integer codCompAnia) {
		this.temporaryData.setCodCompAnia(codCompAnia);
	}

	@Override
	public char getDsOperSql() {
		return temporaryData.getDsOperSql();
	}

	@Override
	public void setDsOperSql(char dsOperSql) {
		this.temporaryData.setDsOperSql(dsOperSql);
	}

	@Override
	public char getDsStatoElab() {
		return temporaryData.getDsStatoElab();
	}

	@Override
	public void setDsStatoElab(char dsStatoElab) {
		this.temporaryData.setDsStatoElab(dsStatoElab);
	}

	@Override
	public long getDsTsCptz() {
		return temporaryData.getDsTsCptz();
	}

	@Override
	public void setDsTsCptz(long dsTsCptz) {
		this.temporaryData.setDsTsCptz(dsTsCptz);
	}

	@Override
	public string getDsUtente() {
		return temporaryData.getDsUtente();
	}

	@Override
	public void setDsUtente(string dsUtente) {
		this.temporaryData.setDsUtente(dsUtente);
	}

	@Override
	public integer getDsVer() {
		return temporaryData.getDsVer();
	}

	@Override
	public void setDsVer(integer dsVer) {
		this.temporaryData.setDsVer(dsVer);
	}

	@Override
	public char getFlContiguousData() {
		return temporaryData.getFlContiguousData();
	}

	@Override
	public void setFlContiguousData(char flContiguousData) {
		this.temporaryData.setFlContiguousData(flContiguousData);
	}

	@Override
	public Character getFlContiguousDataObj() {
		if (indTemporaryData.getDescDato()>= 0) then
		    return getFlContiguousData();
		else
		    return null;
		endif;
	}

	@Override
	public void setFlContiguousDataObj(Character flContiguousDataObj) {
		if (flContiguousDataObj!= null) then
		    setFlContiguousData(flContiguousDataObj);
		    indTemporaryData.setFlContiguousData(0);
		else
		    indTemporaryData.setFlContiguousData(-1);
		endif;
	}

	@Override
	public string getIdSession() {
		return temporaryData.getIdSession();
	}

	@Override
	public void setIdSession(string idSession) {
		this.temporaryData.setIdSession(idSession);
	}

	@Override
	public long getIdTemporaryData() {
		return temporaryData.getIdTemporaryData();
	}

	@Override
	public void setIdTemporaryData(long idTemporaryData) {
		this.temporaryData.setIdTemporaryData(idTemporaryData);
	}

	@Override
	public integer getNumFrame() {
		return temporaryData.getNumFrame();
	}

	@Override
	public void setNumFrame(integer numFrame) {
		this.temporaryData.setNumFrame(numFrame);
	}

	@Override
	public integer getPartialRecurrence() {
		return temporaryData.getPartialRecurrence();
	}

	@Override
	public void setPartialRecurrence(integer partialRecurrence) {
		this.temporaryData.setPartialRecurrence(partialRecurrence);
	}

	@Override
	public Integer getPartialRecurrenceObj() {
		if (indTemporaryData.getLunghezzaDato()>= 0) then
		    return getPartialRecurrence();
		else
		    return null;
		endif;
	}

	@Override
	public void setPartialRecurrenceObj(Integer partialRecurrenceObj) {
		if (partialRecurrenceObj!= null) then
		    setPartialRecurrence(partialRecurrenceObj);
		    indTemporaryData.setLunghezzaDato(0);
		else
		    indTemporaryData.setLunghezzaDato(-1);
		endif;
	}

	public TemporaryData getTemporaryData() {
		return temporaryData;
	}

	public TemporaryDataTrovati getTemporaryDataTrovati() {
		return temporaryDataTrovati;
	}

	@Override
	public integer getTotalRecurrence() {
		return temporaryData.getTotalRecurrence();
	}

	@Override
	public void setTotalRecurrence(integer totalRecurrence) {
		this.temporaryData.setTotalRecurrence(totalRecurrence);
	}

	@Override
	public Integer getTotalRecurrenceObj() {
		if (indTemporaryData.getTipoDato()>= 0) then
		    return getTotalRecurrence();
		else
		    return null;
		endif;
	}

	@Override
	public void setTotalRecurrenceObj(Integer totalRecurrenceObj) {
		if (totalRecurrenceObj!= null) then
		    setTotalRecurrence(totalRecurrenceObj);
		    indTemporaryData.setTipoDato(0);
		else
		    indTemporaryData.setTipoDato(-1);
		endif;
	}

	@Override
	public string getTypeRecord() {
		return temporaryData.getTypeRecord();
	}

	@Override
	public void setTypeRecord(string typeRecord) {
		this.temporaryData.setTypeRecord(typeRecord);
	}

	@Override
	public String getTypeRecordObj() {
		if (indTemporaryData.getCodDominio()>= 0) then
		    return getTypeRecord();
		else
		    return null;
		endif;
	}

	@Override
	public void setTypeRecordObj(String typeRecordObj) {
		if (typeRecordObj!= null) then
		    setTypeRecord(typeRecordObj);
		    indTemporaryData.setTypeRecord(0);
		else
		    indTemporaryData.setTypeRecord(-1);
		endif;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WK_START_POSITION := 9;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Idss0300Data
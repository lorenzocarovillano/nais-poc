package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.copy.Idsv0001AreaComune;
import com.myorg.myprj.copy.Idsv0001LogErrore;
import com.myorg.myprj.ws.enums.Idsv0001Esito;
import com.myorg.myprj.ws.occurs.Idsv0001EleErrori;

/**Original name: AREA-IDSV0001<br>
 * Variable: AREA-IDSV0001 from program IEAS9700<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIdsv0001 extends SerializableParameter {

	//==== PROPERTIES ====
	public final static integer ELE_ERRORI_MAXOCCURS := 10;
	//Original name: IDSV0001-AREA-COMUNE
	private Idsv0001AreaComune areaComune := new Idsv0001AreaComune();
	//Original name: IDSV0001-ESITO
	private Idsv0001Esito esito := new Idsv0001Esito();
	//Original name: IDSV0001-LOG-ERRORE
	private Idsv0001LogErrore logErrore := new Idsv0001LogErrore();
	//Original name: IDSV0001-MAX-ELE-ERRORI
	private short maxEleErrori := DefaultValues.BIN_SHORT_VAL;
	//Original name: IDSV0001-ELE-ERRORI
	private []Idsv0001EleErrori eleErrori := new [ELE_ERRORI_MAXOCCURS]Idsv0001EleErrori;

	//==== CONSTRUCTORS ====
	public AreaIdsv0001() {
		init();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.AREA_IDSV0001;
	}

	@Override
	public void deserialize([]byte buf) {
		setAreaIdsv0001Bytes(buf);
	}

	public void init() {
		for int eleErroriIdx in 1.. ELE_ERRORI_MAXOCCURS 
		do
			eleErrori[eleErroriIdx] := new Idsv0001EleErrori();
		enddo
	}

	public string getAreaIdsv0001Formatted() {
		return getAreaContestoFormatted();
	}

	public void setAreaIdsv0001Bytes([]byte buffer) {
		setAreaIdsv0001Bytes(buffer, 1);
	}

	public []byte getAreaIdsv0001Bytes() {
		[]byte buffer := new [Len.AREA_IDSV0001]byte;
		return getAreaIdsv0001Bytes(buffer, 1);
	}

	public void setAreaIdsv0001Bytes([]byte buffer, integer offset) {
		integer position := offset;
		setAreaContestoBytes(buffer, position);
	}

	public []byte getAreaIdsv0001Bytes([]byte buffer, integer offset) {
		integer position := offset;
		getAreaContestoBytes(buffer, position);
		return buffer;
	}

	public string getAreaContestoFormatted() {
		return MarshalByteExt.bufferToStr(getAreaContestoBytes());
	}

	/**Original name: IDSV0001-AREA-CONTESTO<br>*/
	public []byte getAreaContestoBytes() {
		[]byte buffer := new [Len.AREA_CONTESTO]byte;
		return getAreaContestoBytes(buffer, 1);
	}

	public void setAreaContestoBytes([]byte buffer, integer offset) {
		integer position := offset;
		areaComune.setAreaComuneBytes(buffer, position);
		position +:= Idsv0001AreaComune.Len.AREA_COMUNE;
		setAreaErroriBytes(buffer, position);
	}

	public []byte getAreaContestoBytes([]byte buffer, integer offset) {
		integer position := offset;
		areaComune.getAreaComuneBytes(buffer, position);
		position +:= Idsv0001AreaComune.Len.AREA_COMUNE;
		getAreaErroriBytes(buffer, position);
		return buffer;
	}

	public void setAreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		esito.setEsito(MarshalByte.readString(buffer, position, Idsv0001Esito.Len.ESITO));
		position +:= Idsv0001Esito.Len.ESITO;
		logErrore.setLogErroreBytes(buffer, position);
		position +:= Idsv0001LogErrore.Len.LOG_ERRORE;
		maxEleErrori := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setTabErroriFrontEndBytes(buffer, position);
	}

	public []byte getAreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, esito.getEsito(), Idsv0001Esito.Len.ESITO);
		position +:= Idsv0001Esito.Len.ESITO;
		logErrore.getLogErroreBytes(buffer, position);
		position +:= Idsv0001LogErrore.Len.LOG_ERRORE;
		MarshalByte.writeBinaryShort(buffer, position, maxEleErrori);
		position +:= Types.SHORT_SIZE;
		getTabErroriFrontEndBytes(buffer, position);
		return buffer;
	}

	public void setMaxEleErrori(short maxEleErrori) {
		this.maxEleErrori:=maxEleErrori;
	}

	public short getMaxEleErrori() {
		return this.maxEleErrori;
	}

	public void setTabErroriFrontEndBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. ELE_ERRORI_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				eleErrori[idx].setEleErroriBytes(buffer, position);
				position +:= Idsv0001EleErrori.Len.ELE_ERRORI;
			else
				eleErrori[idx].initEleErroriSpaces();
				position +:= Idsv0001EleErrori.Len.ELE_ERRORI;
			endif
		enddo
	}

	public []byte getTabErroriFrontEndBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. ELE_ERRORI_MAXOCCURS 
		do
			eleErrori[idx].getEleErroriBytes(buffer, position);
			position +:= Idsv0001EleErrori.Len.ELE_ERRORI;
		enddo
		return buffer;
	}

	public Idsv0001AreaComune getAreaComune() {
		return areaComune;
	}

	public Idsv0001EleErrori getEleErrori(integer idx) {
		return eleErrori[idx];
	}

	public Idsv0001Esito getEsito() {
		return esito;
	}

	public Idsv0001LogErrore getLogErrore() {
		return logErrore;
	}

	@Override
	public []byte serialize() {
		return getAreaIdsv0001Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer MAX_ELE_ERRORI := 2;
		public final static integer TAB_ERRORI_FRONT_END := AreaIdsv0001.ELE_ERRORI_MAXOCCURS * Idsv0001EleErrori.Len.ELE_ERRORI;
		public final static integer AREA_ERRORI := Idsv0001Esito.Len.ESITO + Idsv0001LogErrore.Len.LOG_ERRORE + MAX_ELE_ERRORI + TAB_ERRORI_FRONT_END;
		public final static integer AREA_CONTESTO := Idsv0001AreaComune.Len.AREA_COMUNE + AREA_ERRORI;
		public final static integer AREA_IDSV0001 := AREA_CONTESTO;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//AreaIdsv0001
package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.Lccv0021AreaOutput;

/**Original name: LCCV0021<br>
 * Variable: LCCV0021 from copybook LCCV0021<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccv0021 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LCCV0021-PGM
	private string pgm := DefaultValues.stringVal(Len.PGM);
	//Original name: LCCV0021-TP-MOV-PTF
	private string tpMovPtf := DefaultValues.stringVal(Len.TP_MOV_PTF);
	//Original name: LCCV0021-AREA-OUTPUT
	private Lccv0021AreaOutput areaOutput := new Lccv0021AreaOutput();


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.LCCV0021;
	}

	@Override
	public void deserialize([]byte buf) {
		setLccv0021Bytes(buf);
	}

	public string getLccv0021Formatted() {
		return MarshalByteExt.bufferToStr(getLccv0021Bytes());
	}

	public void setLccv0021Bytes([]byte buffer) {
		setLccv0021Bytes(buffer, 1);
	}

	public []byte getLccv0021Bytes() {
		[]byte buffer := new [Len.LCCV0021]byte;
		return getLccv0021Bytes(buffer, 1);
	}

	public void setLccv0021Bytes([]byte buffer, integer offset) {
		integer position := offset;
		pgm := MarshalByte.readString(buffer, position, Len.PGM);
		position +:= Len.PGM;
		setAreaInputBytes(buffer, position);
		position +:= Len.AREA_INPUT;
		areaOutput.setAreaOutputBytes(buffer, position);
	}

	public []byte getLccv0021Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, pgm, Len.PGM);
		position +:= Len.PGM;
		getAreaInputBytes(buffer, position);
		position +:= Len.AREA_INPUT;
		areaOutput.getAreaOutputBytes(buffer, position);
		return buffer;
	}

	public void setPgm(string pgm) {
		this.pgm:=Functions.subString(pgm, Len.PGM);
	}

	public string getPgm() {
		return this.pgm;
	}

	public void setAreaInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		tpMovPtf := MarshalByte.readFixedString(buffer, position, Len.TP_MOV_PTF);
	}

	public []byte getAreaInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, tpMovPtf, Len.TP_MOV_PTF);
		return buffer;
	}

	public void setTpMovPtfFormatted(string tpMovPtf) {
		this.tpMovPtf:=Trunc.toUnsignedNumeric(tpMovPtf, Len.TP_MOV_PTF);
	}

	public integer getTpMovPtf() {
		return NumericDisplay.asInt(this.tpMovPtf);
	}

	public string getTpMovPtfFormatted() {
		return this.tpMovPtf;
	}

	public Lccv0021AreaOutput getAreaOutput() {
		return areaOutput;
	}

	@Override
	public []byte serialize() {
		return getLccv0021Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer PGM := 8;
		public final static integer TP_MOV_PTF := 5;
		public final static integer AREA_INPUT := TP_MOV_PTF;
		public final static integer LCCV0021 := PGM + AREA_INPUT + Lccv0021AreaOutput.Len.AREA_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lccv0021
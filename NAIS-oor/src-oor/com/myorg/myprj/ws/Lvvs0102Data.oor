package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

import com.myorg.myprj.copy.Ivvc0218Ivvs0211;
import com.myorg.myprj.ws.occurs.S089TabLiq;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0102<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0102Data {

	//==== PROPERTIES ====
	public final static integer DLQU_TAB_LIQ_MAXOCCURS := 30;
	/**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
	private string wkPgm := "LVVS0102";
	/**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
	private decimal(11,7) wkDataOutput := DefaultValues.DEC_VAL;
	//Original name: WK-DATA-X-12
	private WkDataX12 wkDataX12 := new WkDataX12();
	//Original name: INPUT-LVVS0000
	private InputLvvs0000 inputLvvs0000 := new InputLvvs0000();
	//Original name: DLQU-ELE-LIQ-MAX
	private short dlquEleLiqMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: DLQU-TAB-LIQ
	private []S089TabLiq dlquTabLiq := new [DLQU_TAB_LIQ_MAXOCCURS]S089TabLiq;
	//Original name: IVVC0218
	private Ivvc0218Ivvs0211 ivvc0218 := new Ivvc0218Ivvs0211();
	//Original name: IX-DCLGEN
	private short ixDclgen := DefaultValues.BIN_SHORT_VAL;
	//Original name: IX-TAB-LQU
	private short ixTabLqu := DefaultValues.BIN_SHORT_VAL;

	//==== CONSTRUCTORS ====
	public Lvvs0102Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int dlquTabLiqIdx in 1.. DLQU_TAB_LIQ_MAXOCCURS 
		do
			dlquTabLiq[dlquTabLiqIdx] := new S089TabLiq();
		enddo
	}

	public string getWkPgm() {
		return this.wkPgm;
	}

	public void setWkDataOutput(decimal(11,7) wkDataOutput) {
		this.wkDataOutput:=wkDataOutput;
	}

	public decimal(11,7) getWkDataOutput() {
		return this.wkDataOutput;
	}

	public void setDlquAreaLiqFormatted(string data) {
		[]byte buffer := new [Len.DLQU_AREA_LIQ]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DLQU_AREA_LIQ);
		setDlquAreaLiqBytes(buffer, 1);
	}

	public void setDlquAreaLiqBytes([]byte buffer, integer offset) {
		integer position := offset;
		dlquEleLiqMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. DLQU_TAB_LIQ_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				dlquTabLiq[idx].setWlquTabLiqBytes(buffer, position);
				position +:= S089TabLiq.Len.WLQU_TAB_LIQ;
			else
				dlquTabLiq[idx].initWlquTabLiqSpaces();
				position +:= S089TabLiq.Len.WLQU_TAB_LIQ;
			endif
		enddo
	}

	public void setDlquEleLiqMax(short dlquEleLiqMax) {
		this.dlquEleLiqMax:=dlquEleLiqMax;
	}

	public short getDlquEleLiqMax() {
		return this.dlquEleLiqMax;
	}

	public void setIxDclgen(short ixDclgen) {
		this.ixDclgen:=ixDclgen;
	}

	public short getIxDclgen() {
		return this.ixDclgen;
	}

	public void setIxTabLqu(short ixTabLqu) {
		this.ixTabLqu:=ixTabLqu;
	}

	public short getIxTabLqu() {
		return this.ixTabLqu;
	}

	public S089TabLiq getDlquTabLiq(integer idx) {
		return dlquTabLiq[idx];
	}

	public InputLvvs0000 getInputLvvs0000() {
		return inputLvvs0000;
	}

	public Ivvc0218Ivvs0211 getIvvc0218() {
		return ivvc0218;
	}

	public WkDataX12 getWkDataX12() {
		return wkDataX12;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DLQU_ELE_LIQ_MAX := 2;
		public final static integer DLQU_AREA_LIQ := DLQU_ELE_LIQ_MAX + Lvvs0102Data.DLQU_TAB_LIQ_MAXOCCURS * S089TabLiq.Len.WLQU_TAB_LIQ;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs0102Data
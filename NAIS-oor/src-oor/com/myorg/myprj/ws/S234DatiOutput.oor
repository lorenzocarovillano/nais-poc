package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: S234-DATI-OUTPUT<br>
 * Variable: S234-DATI-OUTPUT from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class S234DatiOutput extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: S234-ID-OGG-PTF-EOC
	private integer idOggPtfEoc := DefaultValues.INT_VAL;
	//Original name: S234-IB-OGG-PTF-EOC
	private string ibOggPtfEoc := DefaultValues.stringVal(Len.IB_OGG_PTF_EOC);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.S234_DATI_OUTPUT;
	}

	@Override
	public void deserialize([]byte buf) {
		setS234DatiOutputBytes(buf);
	}

	public string getS234DatiOutputFormatted() {
		return MarshalByteExt.bufferToStr(getS234DatiOutputBytes());
	}

	public void setS234DatiOutputBytes([]byte buffer) {
		setS234DatiOutputBytes(buffer, 1);
	}

	public []byte getS234DatiOutputBytes() {
		[]byte buffer := new [Len.S234_DATI_OUTPUT]byte;
		return getS234DatiOutputBytes(buffer, 1);
	}

	public void setS234DatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		idOggPtfEoc := MarshalByte.readInt(buffer, position, Len.ID_OGG_PTF_EOC);
		position +:= Len.ID_OGG_PTF_EOC;
		ibOggPtfEoc := MarshalByte.readString(buffer, position, Len.IB_OGG_PTF_EOC);
	}

	public []byte getS234DatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeInt(buffer, position, idOggPtfEoc, Len.ID_OGG_PTF_EOC);
		position +:= Len.ID_OGG_PTF_EOC;
		MarshalByte.writeString(buffer, position, ibOggPtfEoc, Len.IB_OGG_PTF_EOC);
		return buffer;
	}

	public void setIdOggPtfEoc(integer idOggPtfEoc) {
		this.idOggPtfEoc:=idOggPtfEoc;
	}

	public integer getIdOggPtfEoc() {
		return this.idOggPtfEoc;
	}

	public void setIbOggPtfEoc(string ibOggPtfEoc) {
		this.ibOggPtfEoc:=Functions.subString(ibOggPtfEoc, Len.IB_OGG_PTF_EOC);
	}

	public string getIbOggPtfEoc() {
		return this.ibOggPtfEoc;
	}

	@Override
	public []byte serialize() {
		return getS234DatiOutputBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_OGG_PTF_EOC := 9;
		public final static integer IB_OGG_PTF_EOC := 40;
		public final static integer S234_DATI_OUTPUT := ID_OGG_PTF_EOC + IB_OGG_PTF_EOC;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//S234DatiOutput
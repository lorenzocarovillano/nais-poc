package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.enums.Lccs0025FlPresInPtf;
import com.myorg.myprj.ws.enums.Lccs0025FlTipoIb;

/**Original name: LCCC0025-AREA-COMUNICAZ<br>
 * Variable: LCCC0025-AREA-COMUNICAZ from program LCCS0025<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc0025AreaComunicaz extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LCCS0025-FL-TIPO-IB
	private Lccs0025FlTipoIb flTipoIb := new Lccs0025FlTipoIb();
	//Original name: LCCS0025-IB
	private string ib := DefaultValues.stringVal(Len.IB);
	//Original name: LCCS0025-FL-PRES-IN-PTF
	private Lccs0025FlPresInPtf flPresInPtf := new Lccs0025FlPresInPtf();
	//Original name: LCCS0025-ID-POLI
	private integer idPoli := DefaultValues.INT_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.LCCC0025_AREA_COMUNICAZ;
	}

	@Override
	public void deserialize([]byte buf) {
		setLccc0025AreaComunicazBytes(buf);
	}

	public string getLccc0025AreaComunicazFormatted() {
		return MarshalByteExt.bufferToStr(getLccc0025AreaComunicazBytes());
	}

	public void setLccc0025AreaComunicazBytes([]byte buffer) {
		setLccc0025AreaComunicazBytes(buffer, 1);
	}

	public []byte getLccc0025AreaComunicazBytes() {
		[]byte buffer := new [Len.LCCC0025_AREA_COMUNICAZ]byte;
		return getLccc0025AreaComunicazBytes(buffer, 1);
	}

	public void setLccc0025AreaComunicazBytes([]byte buffer, integer offset) {
		integer position := offset;
		setDatiInputBytes(buffer, position);
		position +:= Len.DATI_INPUT;
		setDatiOutputBytes(buffer, position);
	}

	public []byte getLccc0025AreaComunicazBytes([]byte buffer, integer offset) {
		integer position := offset;
		getDatiInputBytes(buffer, position);
		position +:= Len.DATI_INPUT;
		getDatiOutputBytes(buffer, position);
		return buffer;
	}

	public void setDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		flTipoIb.setFlTipoIb(MarshalByte.readString(buffer, position, Lccs0025FlTipoIb.Len.FL_TIPO_IB));
		position +:= Lccs0025FlTipoIb.Len.FL_TIPO_IB;
		ib := MarshalByte.readString(buffer, position, Len.IB);
	}

	public []byte getDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flTipoIb.getFlTipoIb(), Lccs0025FlTipoIb.Len.FL_TIPO_IB);
		position +:= Lccs0025FlTipoIb.Len.FL_TIPO_IB;
		MarshalByte.writeString(buffer, position, ib, Len.IB);
		return buffer;
	}

	public void setIb(string ib) {
		this.ib:=Functions.subString(ib, Len.IB);
	}

	public string getIb() {
		return this.ib;
	}

	public void setDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		flPresInPtf.setFlPresInPtf(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		idPoli := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
	}

	public []byte getDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, flPresInPtf.getFlPresInPtf());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
		return buffer;
	}

	public void setIdPoli(integer idPoli) {
		this.idPoli:=idPoli;
	}

	public integer getIdPoli() {
		return this.idPoli;
	}

	public Lccs0025FlPresInPtf getFlPresInPtf() {
		return flPresInPtf;
	}

	public Lccs0025FlTipoIb getFlTipoIb() {
		return flTipoIb;
	}

	@Override
	public []byte serialize() {
		return getLccc0025AreaComunicazBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer IB := 40;
		public final static integer DATI_INPUT := Lccs0025FlTipoIb.Len.FL_TIPO_IB + IB;
		public final static integer ID_POLI := 5;
		public final static integer DATI_OUTPUT := Lccs0025FlPresInPtf.Len.FL_PRES_IN_PTF + ID_POLI;
		public final static integer LCCC0025_AREA_COMUNICAZ := DATI_INPUT + DATI_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ID_POLI := 9;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//Lccc0025AreaComunicaz
package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.copy.Lccvb051;
import com.myorg.myprj.copy.Wb05Dati;
import com.myorg.myprj.ws.enums.WpolStatus;

/**Original name: AREA-OUT-B05<br>
 * Variable: AREA-OUT-B05 from program LLBS0230<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaOutB05 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: WB05-ELE-B05-MAX
	private short wb05EleB05Max := (short)0;
	//Original name: LCCVB051
	private Lccvb051 lccvb051 := new Lccvb051();


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.AREA_OUT_B05;
	}

	@Override
	public void deserialize([]byte buf) {
		setAreaOutB05Bytes(buf);
	}

	public string getAreaOutB05Formatted() {
		return getWb05AreaB05Formatted();
	}

	public void setAreaOutB05Bytes([]byte buffer) {
		setAreaOutB05Bytes(buffer, 1);
	}

	public []byte getAreaOutB05Bytes() {
		[]byte buffer := new [Len.AREA_OUT_B05]byte;
		return getAreaOutB05Bytes(buffer, 1);
	}

	public void setAreaOutB05Bytes([]byte buffer, integer offset) {
		integer position := offset;
		setWb05AreaB05Bytes(buffer, position);
	}

	public []byte getAreaOutB05Bytes([]byte buffer, integer offset) {
		integer position := offset;
		getWb05AreaB05Bytes(buffer, position);
		return buffer;
	}

	public string getWb05AreaB05Formatted() {
		return MarshalByteExt.bufferToStr(getWb05AreaB05Bytes());
	}

	/**Original name: WB05-AREA-B05<br>*/
	public []byte getWb05AreaB05Bytes() {
		[]byte buffer := new [Len.WB05_AREA_B05]byte;
		return getWb05AreaB05Bytes(buffer, 1);
	}

	public void setWb05AreaB05Bytes([]byte buffer, integer offset) {
		integer position := offset;
		wb05EleB05Max := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setWb05TabB05Bytes(buffer, position);
	}

	public []byte getWb05AreaB05Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, wb05EleB05Max);
		position +:= Types.SHORT_SIZE;
		getWb05TabB05Bytes(buffer, position);
		return buffer;
	}

	public void setWb05EleB05Max(short wb05EleB05Max) {
		this.wb05EleB05Max:=wb05EleB05Max;
	}

	public short getWb05EleB05Max() {
		return this.wb05EleB05Max;
	}

	public void setWb05TabB05Bytes([]byte buffer, integer offset) {
		integer position := offset;
		lccvb051.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		lccvb051.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvb051.Len.Int.ID_PTF, 0));
		position +:= Lccvb051.Len.ID_PTF;
		lccvb051.getDati().setDatiBytes(buffer, position);
	}

	public []byte getWb05TabB05Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, lccvb051.getStatus().getStatus());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, lccvb051.getIdPtf(), Lccvb051.Len.Int.ID_PTF, 0);
		position +:= Lccvb051.Len.ID_PTF;
		lccvb051.getDati().getDatiBytes(buffer, position);
		return buffer;
	}

	public Lccvb051 getLccvb051() {
		return lccvb051;
	}

	@Override
	public []byte serialize() {
		return getAreaOutB05Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WB05_ELE_B05_MAX := 2;
		public final static integer WB05_TAB_B05 := WpolStatus.Len.STATUS + Lccvb051.Len.ID_PTF + Wb05Dati.Len.DATI;
		public final static integer WB05_AREA_B05 := WB05_ELE_B05_MAX + WB05_TAB_B05;
		public final static integer AREA_OUT_B05 := WB05_AREA_B05;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//AreaOutB05
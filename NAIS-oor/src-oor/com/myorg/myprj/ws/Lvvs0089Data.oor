package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.enums.FlagCurMov;
import com.myorg.myprj.ws.enums.FlagMovTrov;
import com.myorg.myprj.ws.enums.WsMovimento;
import com.myorg.myprj.ws.enums.WsTipoMov;
import com.myorg.myprj.ws.redefines.WsTabLiquiComun;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0089<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0089Data {

	//==== PROPERTIES ====
	//Original name: WK-CALL-PGM
	private string wkCallPgm := "";
	/**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
	private decimal(11,7) wkDataOutput := DefaultValues.DEC_VAL;
	//Original name: WK-DATA-X-12
	private WkDataX12 wkDataX12 := new WkDataX12();
	//Original name: WK-DS-TS-CPTZ
	private long wkDsTsCptz := DefaultValues.LONG_VAL;
	//Original name: FLAG-CUR-MOV
	private FlagCurMov flagCurMov := new FlagCurMov();
	//Original name: FLAG-MOV-TROV
	private FlagMovTrov flagMovTrov := new FlagMovTrov();
	//Original name: WS-TIPO-MOV
	private WsTipoMov wsTipoMov := new WsTipoMov();
	//Original name: WS-TAB-MAX
	private short wsTabMax := (short)50;
	//Original name: WS-TAB-LIQUI-COMUN
	private WsTabLiquiComun wsTabLiquiComun := new WsTabLiquiComun();
	//Original name: WS-TRCH-POS
	private WsTrchPosLvvs0089 wsTrchPos := new WsTrchPosLvvs0089();
	//Original name: WS-TRCH-NEG
	private WsTrchNegLvvs0089 wsTrchNeg := new WsTrchNegLvvs0089();
	//Original name: WS-IMPB-VIS-END2000
	private decimal(15,3) wsImpbVisEnd2000 := DefaultValues.DEC_VAL;
	//Original name: WK-DATA-EFF-RIP
	private integer wkDataEffRip := DefaultValues.INT_VAL;
	//Original name: WK-DATA-CPTZ-RIP
	private long wkDataCptzRip := DefaultValues.LONG_VAL;
	//Original name: LDBVE261
	private Ldbve261 ldbve261 := new Ldbve261();
	/**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
	private WsMovimento wsMovimento := new WsMovimento();
	//Original name: MOVI
	private MoviLdbs1530 movi := new MoviLdbs1530();
	//Original name: IX-INDICI
	private IxIndiciLvvs0089 ixIndici := new IxIndiciLvvs0089();


	//==== METHODS ====
	public void setWkCallPgm(string wkCallPgm) {
		this.wkCallPgm:=Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
	}

	public string getWkCallPgm() {
		return this.wkCallPgm;
	}

	public void setWkDataOutput(decimal(11,7) wkDataOutput) {
		this.wkDataOutput:=wkDataOutput;
	}

	public decimal(11,7) getWkDataOutput() {
		return this.wkDataOutput;
	}

	public void setWkDsTsCptz(long wkDsTsCptz) {
		this.wkDsTsCptz:=wkDsTsCptz;
	}

	public long getWkDsTsCptz() {
		return this.wkDsTsCptz;
	}

	public short getWsTabMax() {
		return this.wsTabMax;
	}

	public void setWsImpbVisEnd2000(decimal(15,3) wsImpbVisEnd2000) {
		this.wsImpbVisEnd2000:=wsImpbVisEnd2000;
	}

	public decimal(15,3) getWsImpbVisEnd2000() {
		return this.wsImpbVisEnd2000;
	}

	public void setWkDataEffRip(integer wkDataEffRip) {
		this.wkDataEffRip:=wkDataEffRip;
	}

	public integer getWkDataEffRip() {
		return this.wkDataEffRip;
	}

	public void setWkDataCptzRip(long wkDataCptzRip) {
		this.wkDataCptzRip:=wkDataCptzRip;
	}

	public long getWkDataCptzRip() {
		return this.wkDataCptzRip;
	}

	public FlagCurMov getFlagCurMov() {
		return flagCurMov;
	}

	public FlagMovTrov getFlagMovTrov() {
		return flagMovTrov;
	}

	public IxIndiciLvvs0089 getIxIndici() {
		return ixIndici;
	}

	public Ldbve261 getLdbve261() {
		return ldbve261;
	}

	public MoviLdbs1530 getMovi() {
		return movi;
	}

	public WkDataX12 getWkDataX12() {
		return wkDataX12;
	}

	public WsMovimento getWsMovimento() {
		return wsMovimento;
	}

	public WsTabLiquiComun getWsTabLiquiComun() {
		return wsTabLiquiComun;
	}

	public WsTipoMov getWsTipoMov() {
		return wsTipoMov;
	}

	public WsTrchNegLvvs0089 getWsTrchNeg() {
		return wsTrchNeg;
	}

	public WsTrchPosLvvs0089 getWsTrchPos() {
		return wsTrchPos;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WK_DATA_INFERIORE := 8;
		public final static integer GG_DIFF := 5;
		public final static integer WK_CALL_PGM := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs0089Data
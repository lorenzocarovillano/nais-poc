package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.DettTitContDb;
import com.myorg.myprj.copy.Idsv0010;
import com.myorg.myprj.copy.IndParamMovi;
import com.myorg.myprj.copy.Ispv0000;
import com.myorg.myprj.copy.Loac0560;
import com.myorg.myprj.ws.enums.FlagAccessoXRangeLdbm0170;
import com.myorg.myprj.ws.redefines.WsTimestamp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBM0170<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbm0170Data {

	//==== PROPERTIES ====
	//Original name: DESCRIZ-ERR-DB2
	private string descrizErrDb2 := "";
	//Original name: WS-TIMESTAMP
	private WsTimestamp wsTimestamp := new WsTimestamp();
	//Original name: IDSV0010
	private Idsv0010 idsv0010 := new Idsv0010();
	//Original name: WS-TP-MOVI
	private integer wsTpMovi := DefaultValues.INT_VAL;
	//Original name: WS-GARANZIA
	private string wsGaranzia := DefaultValues.stringVal(Len.WS_GARANZIA);
	//Original name: WS-FORMA1
	private string wsForma1 := DefaultValues.stringVal(Len.WS_FORMA1);
	//Original name: WS-FORMA2
	private string wsForma2 := DefaultValues.stringVal(Len.WS_FORMA2);
	//Original name: WS-DATA-EFF
	private string wsDataEff := DefaultValues.stringVal(Len.WS_DATA_EFF);
	//Original name: WS-DATA-EFF-9
	private string wsDataEff9 := DefaultValues.stringVal(Len.WS_DATA_EFF9);
	//Original name: WS-DT-ELAB-DA-DB
	private string wsDtElabDaDb := DefaultValues.stringVal(Len.WS_DT_ELAB_DA_DB);
	//Original name: WS-DT-ELAB-A-DB
	private string wsDtElabADb := DefaultValues.stringVal(Len.WS_DT_ELAB_A_DB);
	//Original name: WS-COD-RAMO
	private string wsCodRamo := DefaultValues.stringVal(Len.WS_COD_RAMO);
	//Original name: WK-CC-ASSICURATIVO
	private string wkCcAssicurativo := "312";
	//Original name: WS-DATA-STRUTTURA
	private WsDataStruttura wsDataStruttura := new WsDataStruttura();
	//Original name: FLAG-ACCESSO-X-RANGE
	private FlagAccessoXRangeLdbm0170 flagAccessoXRange := new FlagAccessoXRangeLdbm0170();
	//Original name: BUFFER-WH-COD-RAMO
	private string bufferWhCodRamo := DefaultValues.stringVal(Len.BUFFER_WH_COD_RAMO);
	//Original name: LOAC0560
	private Loac0560 loac0560 := new Loac0560();
	//Original name: ISPV0000
	private Ispv0000 ispv0000 := new Ispv0000();
	//Original name: IND-PARAM-MOVI
	private IndParamMovi indParamMovi := new IndParamMovi();
	//Original name: PARAM-MOVI-DB
	private DettTitContDb paramMoviDb := new DettTitContDb();


	//==== METHODS ====
	public string getDescrizErrDb2() {
		return this.descrizErrDb2;
	}

	public void setWsTpMovi(integer wsTpMovi) {
		this.wsTpMovi:=wsTpMovi;
	}

	public integer getWsTpMovi() {
		return this.wsTpMovi;
	}

	public void setWsGaranzia(string wsGaranzia) {
		this.wsGaranzia:=Functions.subString(wsGaranzia, Len.WS_GARANZIA);
	}

	public string getWsGaranzia() {
		return this.wsGaranzia;
	}

	public void setWsForma1(string wsForma1) {
		this.wsForma1:=Functions.subString(wsForma1, Len.WS_FORMA1);
	}

	public string getWsForma1() {
		return this.wsForma1;
	}

	public void setWsForma2(string wsForma2) {
		this.wsForma2:=Functions.subString(wsForma2, Len.WS_FORMA2);
	}

	public string getWsForma2() {
		return this.wsForma2;
	}

	public void setWsDataEff(string wsDataEff) {
		this.wsDataEff:=Functions.subString(wsDataEff, Len.WS_DATA_EFF);
	}

	public string getWsDataEff() {
		return this.wsDataEff;
	}

	public void setWsDataEff9Formatted(string wsDataEff9) {
		this.wsDataEff9:=Trunc.toUnsignedNumeric(wsDataEff9, Len.WS_DATA_EFF9);
	}

	public integer getWsDataEff9() {
		return NumericDisplay.asInt(this.wsDataEff9);
	}

	public string getWsDataEff9Formatted() {
		return this.wsDataEff9;
	}

	public void setWsDtElabDaDb(string wsDtElabDaDb) {
		this.wsDtElabDaDb:=Functions.subString(wsDtElabDaDb, Len.WS_DT_ELAB_DA_DB);
	}

	public string getWsDtElabDaDb() {
		return this.wsDtElabDaDb;
	}

	public void setWsDtElabADb(string wsDtElabADb) {
		this.wsDtElabADb:=Functions.subString(wsDtElabADb, Len.WS_DT_ELAB_A_DB);
	}

	public string getWsDtElabADb() {
		return this.wsDtElabADb;
	}

	public void setWsCodRamo(string wsCodRamo) {
		this.wsCodRamo:=Functions.subString(wsCodRamo, Len.WS_COD_RAMO);
	}

	public string getWsCodRamo() {
		return this.wsCodRamo;
	}

	public string getWkCcAssicurativo() {
		return this.wkCcAssicurativo;
	}

	public void setBufferWhereConditionFormatted(string data) {
		[]byte buffer := new [Len.BUFFER_WHERE_CONDITION]byte;
		MarshalByte.writeString(buffer, 1, data, Len.BUFFER_WHERE_CONDITION);
		setBufferWhereConditionBytes(buffer, 1);
	}

	public void setBufferWhereConditionBytes([]byte buffer, integer offset) {
		integer position := offset;
		bufferWhCodRamo := MarshalByte.readString(buffer, position, Len.BUFFER_WH_COD_RAMO);
	}

	public void setBufferWhCodRamo(string bufferWhCodRamo) {
		this.bufferWhCodRamo:=Functions.subString(bufferWhCodRamo, Len.BUFFER_WH_COD_RAMO);
	}

	public string getBufferWhCodRamo() {
		return this.bufferWhCodRamo;
	}

	public FlagAccessoXRangeLdbm0170 getFlagAccessoXRange() {
		return flagAccessoXRange;
	}

	public Idsv0010 getIdsv0010() {
		return idsv0010;
	}

	public IndParamMovi getIndParamMovi() {
		return indParamMovi;
	}

	public Ispv0000 getIspv0000() {
		return ispv0000;
	}

	public Loac0560 getLoac0560() {
		return loac0560;
	}

	public DettTitContDb getParamMoviDb() {
		return paramMoviDb;
	}

	public WsDataStruttura getWsDataStruttura() {
		return wsDataStruttura;
	}

	public WsTimestamp getWsTimestamp() {
		return wsTimestamp;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WS_GARANZIA := 2;
		public final static integer WS_FORMA1 := 2;
		public final static integer WS_FORMA2 := 2;
		public final static integer WS_DATA_EFF := 10;
		public final static integer WS_DT_ELAB_DA_DB := 10;
		public final static integer WS_DT_ELAB_A_DB := 10;
		public final static integer WS_COD_RAMO := 12;
		public final static integer WS_ID_MOVI_CRZ := 9;
		public final static integer WK_ID_PARAM_MOVI := 9;
		public final static integer WS_DATA_INI := 10;
		public final static integer WS_DATA_END := 10;
		public final static integer WS_DATA_EFF9 := 8;
		public final static integer BUFFER_WH_COD_RAMO := 12;
		public final static integer BUFFER_WHERE_CONDITION := BUFFER_WH_COD_RAMO;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ldbm0170Data

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [RIS_DI_TRCH]
 * 
 */
public interface IRisDiTrch extends BaseSqlTo
{


    /**
     * Host Variable RST-ID-RIS-DI-TRCH
     * 
     */
    int getIdRisDiTrch();

    void setIdRisDiTrch(int idRisDiTrch);

    /**
     * Host Variable RST-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable RST-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for RST-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable RST-DT-INI-EFF-DB
     * 
     */
    string getDtIniEffDb();

    void setDtIniEffDb(string dtIniEffDb);

    /**
     * Host Variable RST-DT-END-EFF-DB
     * 
     */
    string getDtEndEffDb();

    void setDtEndEffDb(string dtEndEffDb);

    /**
     * Host Variable RST-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable RST-TP-CALC-RIS
     * 
     */
    string getTpCalcRis();

    void setTpCalcRis(string tpCalcRis);

    /**
     * Nullable property for RST-TP-CALC-RIS
     * 
     */
    String getTpCalcRisObj();

    void setTpCalcRisObj(String tpCalcRisObj);

    /**
     * Host Variable RST-ULT-RM
     * 
     */
    decimal(15, 3) getUltRm();

    void setUltRm(decimal(15, 3) ultRm);

    /**
     * Nullable property for RST-ULT-RM
     * 
     */
    AfDecimal getUltRmObj();

    void setUltRmObj(AfDecimal ultRmObj);

    /**
     * Host Variable RST-DT-CALC-DB
     * 
     */
    string getDtCalcDb();

    void setDtCalcDb(string dtCalcDb);

    /**
     * Nullable property for RST-DT-CALC-DB
     * 
     */
    String getDtCalcDbObj();

    void setDtCalcDbObj(String dtCalcDbObj);

    /**
     * Host Variable RST-DT-ELAB-DB
     * 
     */
    string getDtElabDb();

    void setDtElabDb(string dtElabDb);

    /**
     * Nullable property for RST-DT-ELAB-DB
     * 
     */
    String getDtElabDbObj();

    void setDtElabDbObj(String dtElabDbObj);

    /**
     * Host Variable RST-RIS-BILA
     * 
     */
    decimal(15, 3) getRisBila();

    void setRisBila(decimal(15, 3) risBila);

    /**
     * Nullable property for RST-RIS-BILA
     * 
     */
    AfDecimal getRisBilaObj();

    void setRisBilaObj(AfDecimal risBilaObj);

    /**
     * Host Variable RST-RIS-MAT
     * 
     */
    decimal(15, 3) getRisMat();

    void setRisMat(decimal(15, 3) risMat);

    /**
     * Nullable property for RST-RIS-MAT
     * 
     */
    AfDecimal getRisMatObj();

    void setRisMatObj(AfDecimal risMatObj);

    /**
     * Host Variable RST-INCR-X-RIVAL
     * 
     */
    decimal(15, 3) getIncrXRival();

    void setIncrXRival(decimal(15, 3) incrXRival);

    /**
     * Nullable property for RST-INCR-X-RIVAL
     * 
     */
    AfDecimal getIncrXRivalObj();

    void setIncrXRivalObj(AfDecimal incrXRivalObj);

    /**
     * Host Variable RST-RPTO-PRE
     * 
     */
    decimal(15, 3) getRptoPre();

    void setRptoPre(decimal(15, 3) rptoPre);

    /**
     * Nullable property for RST-RPTO-PRE
     * 
     */
    AfDecimal getRptoPreObj();

    void setRptoPreObj(AfDecimal rptoPreObj);

    /**
     * Host Variable RST-FRAZ-PRE-PP
     * 
     */
    decimal(15, 3) getFrazPrePp();

    void setFrazPrePp(decimal(15, 3) frazPrePp);

    /**
     * Nullable property for RST-FRAZ-PRE-PP
     * 
     */
    AfDecimal getFrazPrePpObj();

    void setFrazPrePpObj(AfDecimal frazPrePpObj);

    /**
     * Host Variable RST-RIS-TOT
     * 
     */
    decimal(15, 3) getRisTot();

    void setRisTot(decimal(15, 3) risTot);

    /**
     * Nullable property for RST-RIS-TOT
     * 
     */
    AfDecimal getRisTotObj();

    void setRisTotObj(AfDecimal risTotObj);

    /**
     * Host Variable RST-RIS-SPE
     * 
     */
    decimal(15, 3) getRisSpe();

    void setRisSpe(decimal(15, 3) risSpe);

    /**
     * Nullable property for RST-RIS-SPE
     * 
     */
    AfDecimal getRisSpeObj();

    void setRisSpeObj(AfDecimal risSpeObj);

    /**
     * Host Variable RST-RIS-ABB
     * 
     */
    decimal(15, 3) getRisAbb();

    void setRisAbb(decimal(15, 3) risAbb);

    /**
     * Nullable property for RST-RIS-ABB
     * 
     */
    AfDecimal getRisAbbObj();

    void setRisAbbObj(AfDecimal risAbbObj);

    /**
     * Host Variable RST-RIS-BNSFDT
     * 
     */
    decimal(15, 3) getRisBnsfdt();

    void setRisBnsfdt(decimal(15, 3) risBnsfdt);

    /**
     * Nullable property for RST-RIS-BNSFDT
     * 
     */
    AfDecimal getRisBnsfdtObj();

    void setRisBnsfdtObj(AfDecimal risBnsfdtObj);

    /**
     * Host Variable RST-RIS-SOPR
     * 
     */
    decimal(15, 3) getRisSopr();

    void setRisSopr(decimal(15, 3) risSopr);

    /**
     * Nullable property for RST-RIS-SOPR
     * 
     */
    AfDecimal getRisSoprObj();

    void setRisSoprObj(AfDecimal risSoprObj);

    /**
     * Host Variable RST-RIS-INTEG-BAS-TEC
     * 
     */
    decimal(15, 3) getRisIntegBasTec();

    void setRisIntegBasTec(decimal(15, 3) risIntegBasTec);

    /**
     * Nullable property for RST-RIS-INTEG-BAS-TEC
     * 
     */
    AfDecimal getRisIntegBasTecObj();

    void setRisIntegBasTecObj(AfDecimal risIntegBasTecObj);

    /**
     * Host Variable RST-RIS-INTEG-DECR-TS
     * 
     */
    decimal(15, 3) getRisIntegDecrTs();

    void setRisIntegDecrTs(decimal(15, 3) risIntegDecrTs);

    /**
     * Nullable property for RST-RIS-INTEG-DECR-TS
     * 
     */
    AfDecimal getRisIntegDecrTsObj();

    void setRisIntegDecrTsObj(AfDecimal risIntegDecrTsObj);

    /**
     * Host Variable RST-RIS-GAR-CASO-MOR
     * 
     */
    decimal(15, 3) getRisGarCasoMor();

    void setRisGarCasoMor(decimal(15, 3) risGarCasoMor);

    /**
     * Nullable property for RST-RIS-GAR-CASO-MOR
     * 
     */
    AfDecimal getRisGarCasoMorObj();

    void setRisGarCasoMorObj(AfDecimal risGarCasoMorObj);

    /**
     * Host Variable RST-RIS-ZIL
     * 
     */
    decimal(15, 3) getRisZil();

    void setRisZil(decimal(15, 3) risZil);

    /**
     * Nullable property for RST-RIS-ZIL
     * 
     */
    AfDecimal getRisZilObj();

    void setRisZilObj(AfDecimal risZilObj);

    /**
     * Host Variable RST-RIS-FAIVL
     * 
     */
    decimal(15, 3) getRisFaivl();

    void setRisFaivl(decimal(15, 3) risFaivl);

    /**
     * Nullable property for RST-RIS-FAIVL
     * 
     */
    AfDecimal getRisFaivlObj();

    void setRisFaivlObj(AfDecimal risFaivlObj);

    /**
     * Host Variable RST-RIS-COS-AMMTZ
     * 
     */
    decimal(15, 3) getRisCosAmmtz();

    void setRisCosAmmtz(decimal(15, 3) risCosAmmtz);

    /**
     * Nullable property for RST-RIS-COS-AMMTZ
     * 
     */
    AfDecimal getRisCosAmmtzObj();

    void setRisCosAmmtzObj(AfDecimal risCosAmmtzObj);

    /**
     * Host Variable RST-RIS-SPE-FAIVL
     * 
     */
    decimal(15, 3) getRisSpeFaivl();

    void setRisSpeFaivl(decimal(15, 3) risSpeFaivl);

    /**
     * Nullable property for RST-RIS-SPE-FAIVL
     * 
     */
    AfDecimal getRisSpeFaivlObj();

    void setRisSpeFaivlObj(AfDecimal risSpeFaivlObj);

    /**
     * Host Variable RST-RIS-PREST-FAIVL
     * 
     */
    decimal(15, 3) getRisPrestFaivl();

    void setRisPrestFaivl(decimal(15, 3) risPrestFaivl);

    /**
     * Nullable property for RST-RIS-PREST-FAIVL
     * 
     */
    AfDecimal getRisPrestFaivlObj();

    void setRisPrestFaivlObj(AfDecimal risPrestFaivlObj);

    /**
     * Host Variable RST-RIS-COMPON-ASSVA
     * 
     */
    decimal(15, 3) getRisComponAssva();

    void setRisComponAssva(decimal(15, 3) risComponAssva);

    /**
     * Nullable property for RST-RIS-COMPON-ASSVA
     * 
     */
    AfDecimal getRisComponAssvaObj();

    void setRisComponAssvaObj(AfDecimal risComponAssvaObj);

    /**
     * Host Variable RST-ULT-COEFF-RIS
     * 
     */
    decimal(14, 9) getUltCoeffRis();

    void setUltCoeffRis(decimal(14, 9) ultCoeffRis);

    /**
     * Nullable property for RST-ULT-COEFF-RIS
     * 
     */
    AfDecimal getUltCoeffRisObj();

    void setUltCoeffRisObj(AfDecimal ultCoeffRisObj);

    /**
     * Host Variable RST-ULT-COEFF-AGG-RIS
     * 
     */
    decimal(14, 9) getUltCoeffAggRis();

    void setUltCoeffAggRis(decimal(14, 9) ultCoeffAggRis);

    /**
     * Nullable property for RST-ULT-COEFF-AGG-RIS
     * 
     */
    AfDecimal getUltCoeffAggRisObj();

    void setUltCoeffAggRisObj(AfDecimal ultCoeffAggRisObj);

    /**
     * Host Variable RST-RIS-ACQ
     * 
     */
    decimal(15, 3) getRisAcq();

    void setRisAcq(decimal(15, 3) risAcq);

    /**
     * Nullable property for RST-RIS-ACQ
     * 
     */
    AfDecimal getRisAcqObj();

    void setRisAcqObj(AfDecimal risAcqObj);

    /**
     * Host Variable RST-RIS-UTI
     * 
     */
    decimal(15, 3) getRisUti();

    void setRisUti(decimal(15, 3) risUti);

    /**
     * Nullable property for RST-RIS-UTI
     * 
     */
    AfDecimal getRisUtiObj();

    void setRisUtiObj(AfDecimal risUtiObj);

    /**
     * Host Variable RST-RIS-MAT-EFF
     * 
     */
    decimal(15, 3) getRisMatEff();

    void setRisMatEff(decimal(15, 3) risMatEff);

    /**
     * Nullable property for RST-RIS-MAT-EFF
     * 
     */
    AfDecimal getRisMatEffObj();

    void setRisMatEffObj(AfDecimal risMatEffObj);

    /**
     * Host Variable RST-RIS-RISTORNI-CAP
     * 
     */
    decimal(15, 3) getRisRistorniCap();

    void setRisRistorniCap(decimal(15, 3) risRistorniCap);

    /**
     * Nullable property for RST-RIS-RISTORNI-CAP
     * 
     */
    AfDecimal getRisRistorniCapObj();

    void setRisRistorniCapObj(AfDecimal risRistorniCapObj);

    /**
     * Host Variable RST-RIS-TRM-BNS
     * 
     */
    decimal(15, 3) getRisTrmBns();

    void setRisTrmBns(decimal(15, 3) risTrmBns);

    /**
     * Nullable property for RST-RIS-TRM-BNS
     * 
     */
    AfDecimal getRisTrmBnsObj();

    void setRisTrmBnsObj(AfDecimal risTrmBnsObj);

    /**
     * Host Variable RST-RIS-BNSRIC
     * 
     */
    decimal(15, 3) getRisBnsric();

    void setRisBnsric(decimal(15, 3) risBnsric);

    /**
     * Nullable property for RST-RIS-BNSRIC
     * 
     */
    AfDecimal getRisBnsricObj();

    void setRisBnsricObj(AfDecimal risBnsricObj);

    /**
     * Host Variable RST-DS-RIGA
     * 
     */
    long getRstDsRiga();

    void setRstDsRiga(long rstDsRiga);

    /**
     * Host Variable RST-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable RST-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable RST-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable RST-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable RST-DS-UTENTE
     * 
     */
    string getDsUtente();

    void setDsUtente(string dsUtente);

    /**
     * Host Variable RST-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable RST-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable RST-COD-FND
     * 
     */
    string getCodFnd();

    void setCodFnd(string codFnd);

    /**
     * Nullable property for RST-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable RST-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable RST-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable RST-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable RST-RIS-MIN-GARTO
     * 
     */
    decimal(15, 3) getRisMinGarto();

    void setRisMinGarto(decimal(15, 3) risMinGarto);

    /**
     * Nullable property for RST-RIS-MIN-GARTO
     * 
     */
    AfDecimal getRisMinGartoObj();

    void setRisMinGartoObj(AfDecimal risMinGartoObj);

    /**
     * Host Variable RST-RIS-RSH-DFLT
     * 
     */
    decimal(15, 3) getRisRshDflt();

    void setRisRshDflt(decimal(15, 3) risRshDflt);

    /**
     * Nullable property for RST-RIS-RSH-DFLT
     * 
     */
    AfDecimal getRisRshDfltObj();

    void setRisRshDfltObj(AfDecimal risRshDfltObj);

    /**
     * Host Variable RST-RIS-MOVI-NON-INVES
     * 
     */
    decimal(15, 3) getRisMoviNonInves();

    void setRisMoviNonInves(decimal(15, 3) risMoviNonInves);

    /**
     * Nullable property for RST-RIS-MOVI-NON-INVES
     * 
     */
    AfDecimal getRisMoviNonInvesObj();

    void setRisMoviNonInvesObj(AfDecimal risMoviNonInvesObj);

}

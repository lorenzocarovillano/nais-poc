
package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [TRCH_DI_GAR]
 * 
 */
public interface ITrchDiGar extends BaseSqlTo
{


    /**
     * Host Variable TGA-ID-ADES
     * 
     */
    int getTgaIdAdes();

    void setTgaIdAdes(int tgaIdAdes);

    /**
     * Host Variable TGA-ID-POLI
     * 
     */
    int getTgaIdPoli();

    void setTgaIdPoli(int tgaIdPoli);

    /**
     * Host Variable TGA-ID-MOVI-CRZ
     * 
     */
    int getTgaIdMoviCrz();

    void setTgaIdMoviCrz(int tgaIdMoviCrz);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    string getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(string wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable TGA-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable TGA-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable TGA-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for TGA-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable TGA-DT-INI-EFF-DB
     * 
     */
    string getDtIniEffDb();

    void setDtIniEffDb(string dtIniEffDb);

    /**
     * Host Variable TGA-DT-END-EFF-DB
     * 
     */
    string getDtEndEffDb();

    void setDtEndEffDb(string dtEndEffDb);

    /**
     * Host Variable TGA-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable TGA-DT-DECOR-DB
     * 
     */
    string getDtDecorDb();

    void setDtDecorDb(string dtDecorDb);

    /**
     * Host Variable TGA-DT-SCAD-DB
     * 
     */
    string getDtScadDb();

    void setDtScadDb(string dtScadDb);

    /**
     * Nullable property for TGA-DT-SCAD-DB
     * 
     */
    String getDtScadDbObj();

    void setDtScadDbObj(String dtScadDbObj);

    /**
     * Host Variable TGA-IB-OGG
     * 
     */
    string getIbOgg();

    void setIbOgg(string ibOgg);

    /**
     * Nullable property for TGA-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable TGA-TP-RGM-FISC
     * 
     */
    string getTpRgmFisc();

    void setTpRgmFisc(string tpRgmFisc);

    /**
     * Host Variable TGA-DT-EMIS-DB
     * 
     */
    string getDtEmisDb();

    void setDtEmisDb(string dtEmisDb);

    /**
     * Nullable property for TGA-DT-EMIS-DB
     * 
     */
    String getDtEmisDbObj();

    void setDtEmisDbObj(String dtEmisDbObj);

    /**
     * Host Variable TGA-TP-TRCH
     * 
     */
    string getTpTrch();

    void setTpTrch(string tpTrch);

    /**
     * Host Variable TGA-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for TGA-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable TGA-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for TGA-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable TGA-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for TGA-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable TGA-PRE-CASO-MOR
     * 
     */
    decimal(15, 3) getPreCasoMor();

    void setPreCasoMor(decimal(15, 3) preCasoMor);

    /**
     * Nullable property for TGA-PRE-CASO-MOR
     * 
     */
    AfDecimal getPreCasoMorObj();

    void setPreCasoMorObj(AfDecimal preCasoMorObj);

    /**
     * Host Variable TGA-PC-INTR-RIAT
     * 
     */
    decimal(6, 3) getPcIntrRiat();

    void setPcIntrRiat(decimal(6, 3) pcIntrRiat);

    /**
     * Nullable property for TGA-PC-INTR-RIAT
     * 
     */
    AfDecimal getPcIntrRiatObj();

    void setPcIntrRiatObj(AfDecimal pcIntrRiatObj);

    /**
     * Host Variable TGA-IMP-BNS-ANTIC
     * 
     */
    decimal(15, 3) getImpBnsAntic();

    void setImpBnsAntic(decimal(15, 3) impBnsAntic);

    /**
     * Nullable property for TGA-IMP-BNS-ANTIC
     * 
     */
    AfDecimal getImpBnsAnticObj();

    void setImpBnsAnticObj(AfDecimal impBnsAnticObj);

    /**
     * Host Variable TGA-PRE-INI-NET
     * 
     */
    decimal(15, 3) getPreIniNet();

    void setPreIniNet(decimal(15, 3) preIniNet);

    /**
     * Nullable property for TGA-PRE-INI-NET
     * 
     */
    AfDecimal getPreIniNetObj();

    void setPreIniNetObj(AfDecimal preIniNetObj);

    /**
     * Host Variable TGA-PRE-PP-INI
     * 
     */
    decimal(15, 3) getPrePpIni();

    void setPrePpIni(decimal(15, 3) prePpIni);

    /**
     * Nullable property for TGA-PRE-PP-INI
     * 
     */
    AfDecimal getPrePpIniObj();

    void setPrePpIniObj(AfDecimal prePpIniObj);

    /**
     * Host Variable TGA-PRE-PP-ULT
     * 
     */
    decimal(15, 3) getPrePpUlt();

    void setPrePpUlt(decimal(15, 3) prePpUlt);

    /**
     * Nullable property for TGA-PRE-PP-ULT
     * 
     */
    AfDecimal getPrePpUltObj();

    void setPrePpUltObj(AfDecimal prePpUltObj);

    /**
     * Host Variable TGA-PRE-TARI-INI
     * 
     */
    decimal(15, 3) getPreTariIni();

    void setPreTariIni(decimal(15, 3) preTariIni);

    /**
     * Nullable property for TGA-PRE-TARI-INI
     * 
     */
    AfDecimal getPreTariIniObj();

    void setPreTariIniObj(AfDecimal preTariIniObj);

    /**
     * Host Variable TGA-PRE-TARI-ULT
     * 
     */
    decimal(15, 3) getPreTariUlt();

    void setPreTariUlt(decimal(15, 3) preTariUlt);

    /**
     * Nullable property for TGA-PRE-TARI-ULT
     * 
     */
    AfDecimal getPreTariUltObj();

    void setPreTariUltObj(AfDecimal preTariUltObj);

    /**
     * Host Variable TGA-PRE-INVRIO-INI
     * 
     */
    decimal(15, 3) getPreInvrioIni();

    void setPreInvrioIni(decimal(15, 3) preInvrioIni);

    /**
     * Nullable property for TGA-PRE-INVRIO-INI
     * 
     */
    AfDecimal getPreInvrioIniObj();

    void setPreInvrioIniObj(AfDecimal preInvrioIniObj);

    /**
     * Host Variable TGA-PRE-INVRIO-ULT
     * 
     */
    decimal(15, 3) getPreInvrioUlt();

    void setPreInvrioUlt(decimal(15, 3) preInvrioUlt);

    /**
     * Nullable property for TGA-PRE-INVRIO-ULT
     * 
     */
    AfDecimal getPreInvrioUltObj();

    void setPreInvrioUltObj(AfDecimal preInvrioUltObj);

    /**
     * Host Variable TGA-PRE-RIVTO
     * 
     */
    decimal(15, 3) getPreRivto();

    void setPreRivto(decimal(15, 3) preRivto);

    /**
     * Nullable property for TGA-PRE-RIVTO
     * 
     */
    AfDecimal getPreRivtoObj();

    void setPreRivtoObj(AfDecimal preRivtoObj);

    /**
     * Host Variable TGA-IMP-SOPR-PROF
     * 
     */
    decimal(15, 3) getImpSoprProf();

    void setImpSoprProf(decimal(15, 3) impSoprProf);

    /**
     * Nullable property for TGA-IMP-SOPR-PROF
     * 
     */
    AfDecimal getImpSoprProfObj();

    void setImpSoprProfObj(AfDecimal impSoprProfObj);

    /**
     * Host Variable TGA-IMP-SOPR-SAN
     * 
     */
    decimal(15, 3) getImpSoprSan();

    void setImpSoprSan(decimal(15, 3) impSoprSan);

    /**
     * Nullable property for TGA-IMP-SOPR-SAN
     * 
     */
    AfDecimal getImpSoprSanObj();

    void setImpSoprSanObj(AfDecimal impSoprSanObj);

    /**
     * Host Variable TGA-IMP-SOPR-SPO
     * 
     */
    decimal(15, 3) getImpSoprSpo();

    void setImpSoprSpo(decimal(15, 3) impSoprSpo);

    /**
     * Nullable property for TGA-IMP-SOPR-SPO
     * 
     */
    AfDecimal getImpSoprSpoObj();

    void setImpSoprSpoObj(AfDecimal impSoprSpoObj);

    /**
     * Host Variable TGA-IMP-SOPR-TEC
     * 
     */
    decimal(15, 3) getImpSoprTec();

    void setImpSoprTec(decimal(15, 3) impSoprTec);

    /**
     * Nullable property for TGA-IMP-SOPR-TEC
     * 
     */
    AfDecimal getImpSoprTecObj();

    void setImpSoprTecObj(AfDecimal impSoprTecObj);

    /**
     * Host Variable TGA-IMP-ALT-SOPR
     * 
     */
    decimal(15, 3) getImpAltSopr();

    void setImpAltSopr(decimal(15, 3) impAltSopr);

    /**
     * Nullable property for TGA-IMP-ALT-SOPR
     * 
     */
    AfDecimal getImpAltSoprObj();

    void setImpAltSoprObj(AfDecimal impAltSoprObj);

    /**
     * Host Variable TGA-PRE-STAB
     * 
     */
    decimal(15, 3) getPreStab();

    void setPreStab(decimal(15, 3) preStab);

    /**
     * Nullable property for TGA-PRE-STAB
     * 
     */
    AfDecimal getPreStabObj();

    void setPreStabObj(AfDecimal preStabObj);

    /**
     * Host Variable TGA-DT-EFF-STAB-DB
     * 
     */
    string getDtEffStabDb();

    void setDtEffStabDb(string dtEffStabDb);

    /**
     * Nullable property for TGA-DT-EFF-STAB-DB
     * 
     */
    String getDtEffStabDbObj();

    void setDtEffStabDbObj(String dtEffStabDbObj);

    /**
     * Host Variable TGA-TS-RIVAL-FIS
     * 
     */
    decimal(14, 9) getTsRivalFis();

    void setTsRivalFis(decimal(14, 9) tsRivalFis);

    /**
     * Nullable property for TGA-TS-RIVAL-FIS
     * 
     */
    AfDecimal getTsRivalFisObj();

    void setTsRivalFisObj(AfDecimal tsRivalFisObj);

    /**
     * Host Variable TGA-TS-RIVAL-INDICIZ
     * 
     */
    decimal(14, 9) getTsRivalIndiciz();

    void setTsRivalIndiciz(decimal(14, 9) tsRivalIndiciz);

    /**
     * Nullable property for TGA-TS-RIVAL-INDICIZ
     * 
     */
    AfDecimal getTsRivalIndicizObj();

    void setTsRivalIndicizObj(AfDecimal tsRivalIndicizObj);

    /**
     * Host Variable TGA-OLD-TS-TEC
     * 
     */
    decimal(14, 9) getOldTsTec();

    void setOldTsTec(decimal(14, 9) oldTsTec);

    /**
     * Nullable property for TGA-OLD-TS-TEC
     * 
     */
    AfDecimal getOldTsTecObj();

    void setOldTsTecObj(AfDecimal oldTsTecObj);

    /**
     * Host Variable TGA-RAT-LRD
     * 
     */
    decimal(15, 3) getRatLrd();

    void setRatLrd(decimal(15, 3) ratLrd);

    /**
     * Nullable property for TGA-RAT-LRD
     * 
     */
    AfDecimal getRatLrdObj();

    void setRatLrdObj(AfDecimal ratLrdObj);

    /**
     * Host Variable TGA-PRE-LRD
     * 
     */
    decimal(15, 3) getPreLrd();

    void setPreLrd(decimal(15, 3) preLrd);

    /**
     * Nullable property for TGA-PRE-LRD
     * 
     */
    AfDecimal getPreLrdObj();

    void setPreLrdObj(AfDecimal preLrdObj);

    /**
     * Host Variable TGA-PRSTZ-INI
     * 
     */
    decimal(15, 3) getPrstzIni();

    void setPrstzIni(decimal(15, 3) prstzIni);

    /**
     * Nullable property for TGA-PRSTZ-INI
     * 
     */
    AfDecimal getPrstzIniObj();

    void setPrstzIniObj(AfDecimal prstzIniObj);

    /**
     * Host Variable TGA-PRSTZ-ULT
     * 
     */
    decimal(15, 3) getPrstzUlt();

    void setPrstzUlt(decimal(15, 3) prstzUlt);

    /**
     * Nullable property for TGA-PRSTZ-ULT
     * 
     */
    AfDecimal getPrstzUltObj();

    void setPrstzUltObj(AfDecimal prstzUltObj);

    /**
     * Host Variable TGA-CPT-IN-OPZ-RIVTO
     * 
     */
    decimal(15, 3) getCptInOpzRivto();

    void setCptInOpzRivto(decimal(15, 3) cptInOpzRivto);

    /**
     * Nullable property for TGA-CPT-IN-OPZ-RIVTO
     * 
     */
    AfDecimal getCptInOpzRivtoObj();

    void setCptInOpzRivtoObj(AfDecimal cptInOpzRivtoObj);

    /**
     * Host Variable TGA-PRSTZ-INI-STAB
     * 
     */
    decimal(15, 3) getPrstzIniStab();

    void setPrstzIniStab(decimal(15, 3) prstzIniStab);

    /**
     * Nullable property for TGA-PRSTZ-INI-STAB
     * 
     */
    AfDecimal getPrstzIniStabObj();

    void setPrstzIniStabObj(AfDecimal prstzIniStabObj);

    /**
     * Host Variable TGA-CPT-RSH-MOR
     * 
     */
    decimal(15, 3) getCptRshMor();

    void setCptRshMor(decimal(15, 3) cptRshMor);

    /**
     * Nullable property for TGA-CPT-RSH-MOR
     * 
     */
    AfDecimal getCptRshMorObj();

    void setCptRshMorObj(AfDecimal cptRshMorObj);

    /**
     * Host Variable TGA-PRSTZ-RID-INI
     * 
     */
    decimal(15, 3) getPrstzRidIni();

    void setPrstzRidIni(decimal(15, 3) prstzRidIni);

    /**
     * Nullable property for TGA-PRSTZ-RID-INI
     * 
     */
    AfDecimal getPrstzRidIniObj();

    void setPrstzRidIniObj(AfDecimal prstzRidIniObj);

    /**
     * Host Variable TGA-FL-CAR-CONT
     * 
     */
    char getFlCarCont();

    void setFlCarCont(char flCarCont);

    /**
     * Nullable property for TGA-FL-CAR-CONT
     * 
     */
    Character getFlCarContObj();

    void setFlCarContObj(Character flCarContObj);

    /**
     * Host Variable TGA-BNS-GIA-LIQTO
     * 
     */
    decimal(15, 3) getBnsGiaLiqto();

    void setBnsGiaLiqto(decimal(15, 3) bnsGiaLiqto);

    /**
     * Nullable property for TGA-BNS-GIA-LIQTO
     * 
     */
    AfDecimal getBnsGiaLiqtoObj();

    void setBnsGiaLiqtoObj(AfDecimal bnsGiaLiqtoObj);

    /**
     * Host Variable TGA-IMP-BNS
     * 
     */
    decimal(15, 3) getImpBns();

    void setImpBns(decimal(15, 3) impBns);

    /**
     * Nullable property for TGA-IMP-BNS
     * 
     */
    AfDecimal getImpBnsObj();

    void setImpBnsObj(AfDecimal impBnsObj);

    /**
     * Host Variable TGA-COD-DVS
     * 
     */
    string getCodDvs();

    void setCodDvs(string codDvs);

    /**
     * Host Variable TGA-PRSTZ-INI-NEWFIS
     * 
     */
    decimal(15, 3) getPrstzIniNewfis();

    void setPrstzIniNewfis(decimal(15, 3) prstzIniNewfis);

    /**
     * Nullable property for TGA-PRSTZ-INI-NEWFIS
     * 
     */
    AfDecimal getPrstzIniNewfisObj();

    void setPrstzIniNewfisObj(AfDecimal prstzIniNewfisObj);

    /**
     * Host Variable TGA-IMP-SCON
     * 
     */
    decimal(15, 3) getImpScon();

    void setImpScon(decimal(15, 3) impScon);

    /**
     * Nullable property for TGA-IMP-SCON
     * 
     */
    AfDecimal getImpSconObj();

    void setImpSconObj(AfDecimal impSconObj);

    /**
     * Host Variable TGA-ALQ-SCON
     * 
     */
    decimal(6, 3) getAlqScon();

    void setAlqScon(decimal(6, 3) alqScon);

    /**
     * Nullable property for TGA-ALQ-SCON
     * 
     */
    AfDecimal getAlqSconObj();

    void setAlqSconObj(AfDecimal alqSconObj);

    /**
     * Host Variable TGA-IMP-CAR-ACQ
     * 
     */
    decimal(15, 3) getImpCarAcq();

    void setImpCarAcq(decimal(15, 3) impCarAcq);

    /**
     * Nullable property for TGA-IMP-CAR-ACQ
     * 
     */
    AfDecimal getImpCarAcqObj();

    void setImpCarAcqObj(AfDecimal impCarAcqObj);

    /**
     * Host Variable TGA-IMP-CAR-INC
     * 
     */
    decimal(15, 3) getImpCarInc();

    void setImpCarInc(decimal(15, 3) impCarInc);

    /**
     * Nullable property for TGA-IMP-CAR-INC
     * 
     */
    AfDecimal getImpCarIncObj();

    void setImpCarIncObj(AfDecimal impCarIncObj);

    /**
     * Host Variable TGA-IMP-CAR-GEST
     * 
     */
    decimal(15, 3) getImpCarGest();

    void setImpCarGest(decimal(15, 3) impCarGest);

    /**
     * Nullable property for TGA-IMP-CAR-GEST
     * 
     */
    AfDecimal getImpCarGestObj();

    void setImpCarGestObj(AfDecimal impCarGestObj);

    /**
     * Host Variable TGA-ETA-AA-1O-ASSTO
     * 
     */
    short getEtaAa1oAssto();

    void setEtaAa1oAssto(short etaAa1oAssto);

    /**
     * Nullable property for TGA-ETA-AA-1O-ASSTO
     * 
     */
    Short getEtaAa1oAsstoObj();

    void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-1O-ASSTO
     * 
     */
    short getEtaMm1oAssto();

    void setEtaMm1oAssto(short etaMm1oAssto);

    /**
     * Nullable property for TGA-ETA-MM-1O-ASSTO
     * 
     */
    Short getEtaMm1oAsstoObj();

    void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj);

    /**
     * Host Variable TGA-ETA-AA-2O-ASSTO
     * 
     */
    short getEtaAa2oAssto();

    void setEtaAa2oAssto(short etaAa2oAssto);

    /**
     * Nullable property for TGA-ETA-AA-2O-ASSTO
     * 
     */
    Short getEtaAa2oAsstoObj();

    void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-2O-ASSTO
     * 
     */
    short getEtaMm2oAssto();

    void setEtaMm2oAssto(short etaMm2oAssto);

    /**
     * Nullable property for TGA-ETA-MM-2O-ASSTO
     * 
     */
    Short getEtaMm2oAsstoObj();

    void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj);

    /**
     * Host Variable TGA-ETA-AA-3O-ASSTO
     * 
     */
    short getEtaAa3oAssto();

    void setEtaAa3oAssto(short etaAa3oAssto);

    /**
     * Nullable property for TGA-ETA-AA-3O-ASSTO
     * 
     */
    Short getEtaAa3oAsstoObj();

    void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-3O-ASSTO
     * 
     */
    short getEtaMm3oAssto();

    void setEtaMm3oAssto(short etaMm3oAssto);

    /**
     * Nullable property for TGA-ETA-MM-3O-ASSTO
     * 
     */
    Short getEtaMm3oAsstoObj();

    void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj);

    /**
     * Host Variable TGA-RENDTO-LRD
     * 
     */
    decimal(14, 9) getRendtoLrd();

    void setRendtoLrd(decimal(14, 9) rendtoLrd);

    /**
     * Nullable property for TGA-RENDTO-LRD
     * 
     */
    AfDecimal getRendtoLrdObj();

    void setRendtoLrdObj(AfDecimal rendtoLrdObj);

    /**
     * Host Variable TGA-PC-RETR
     * 
     */
    decimal(6, 3) getPcRetr();

    void setPcRetr(decimal(6, 3) pcRetr);

    /**
     * Nullable property for TGA-PC-RETR
     * 
     */
    AfDecimal getPcRetrObj();

    void setPcRetrObj(AfDecimal pcRetrObj);

    /**
     * Host Variable TGA-RENDTO-RETR
     * 
     */
    decimal(14, 9) getRendtoRetr();

    void setRendtoRetr(decimal(14, 9) rendtoRetr);

    /**
     * Nullable property for TGA-RENDTO-RETR
     * 
     */
    AfDecimal getRendtoRetrObj();

    void setRendtoRetrObj(AfDecimal rendtoRetrObj);

    /**
     * Host Variable TGA-MIN-GARTO
     * 
     */
    decimal(14, 9) getMinGarto();

    void setMinGarto(decimal(14, 9) minGarto);

    /**
     * Nullable property for TGA-MIN-GARTO
     * 
     */
    AfDecimal getMinGartoObj();

    void setMinGartoObj(AfDecimal minGartoObj);

    /**
     * Host Variable TGA-MIN-TRNUT
     * 
     */
    decimal(14, 9) getMinTrnut();

    void setMinTrnut(decimal(14, 9) minTrnut);

    /**
     * Nullable property for TGA-MIN-TRNUT
     * 
     */
    AfDecimal getMinTrnutObj();

    void setMinTrnutObj(AfDecimal minTrnutObj);

    /**
     * Host Variable TGA-PRE-ATT-DI-TRCH
     * 
     */
    decimal(15, 3) getPreAttDiTrch();

    void setPreAttDiTrch(decimal(15, 3) preAttDiTrch);

    /**
     * Nullable property for TGA-PRE-ATT-DI-TRCH
     * 
     */
    AfDecimal getPreAttDiTrchObj();

    void setPreAttDiTrchObj(AfDecimal preAttDiTrchObj);

    /**
     * Host Variable TGA-MATU-END2000
     * 
     */
    decimal(15, 3) getMatuEnd2000();

    void setMatuEnd2000(decimal(15, 3) matuEnd2000);

    /**
     * Nullable property for TGA-MATU-END2000
     * 
     */
    AfDecimal getMatuEnd2000Obj();

    void setMatuEnd2000Obj(AfDecimal matuEnd2000Obj);

    /**
     * Host Variable TGA-ABB-TOT-INI
     * 
     */
    decimal(15, 3) getAbbTotIni();

    void setAbbTotIni(decimal(15, 3) abbTotIni);

    /**
     * Nullable property for TGA-ABB-TOT-INI
     * 
     */
    AfDecimal getAbbTotIniObj();

    void setAbbTotIniObj(AfDecimal abbTotIniObj);

    /**
     * Host Variable TGA-ABB-TOT-ULT
     * 
     */
    decimal(15, 3) getAbbTotUlt();

    void setAbbTotUlt(decimal(15, 3) abbTotUlt);

    /**
     * Nullable property for TGA-ABB-TOT-ULT
     * 
     */
    AfDecimal getAbbTotUltObj();

    void setAbbTotUltObj(AfDecimal abbTotUltObj);

    /**
     * Host Variable TGA-ABB-ANNU-ULT
     * 
     */
    decimal(15, 3) getAbbAnnuUlt();

    void setAbbAnnuUlt(decimal(15, 3) abbAnnuUlt);

    /**
     * Nullable property for TGA-ABB-ANNU-ULT
     * 
     */
    AfDecimal getAbbAnnuUltObj();

    void setAbbAnnuUltObj(AfDecimal abbAnnuUltObj);

    /**
     * Host Variable TGA-DUR-ABB
     * 
     */
    int getDurAbb();

    void setDurAbb(int durAbb);

    /**
     * Nullable property for TGA-DUR-ABB
     * 
     */
    Integer getDurAbbObj();

    void setDurAbbObj(Integer durAbbObj);

    /**
     * Host Variable TGA-TP-ADEG-ABB
     * 
     */
    char getTpAdegAbb();

    void setTpAdegAbb(char tpAdegAbb);

    /**
     * Nullable property for TGA-TP-ADEG-ABB
     * 
     */
    Character getTpAdegAbbObj();

    void setTpAdegAbbObj(Character tpAdegAbbObj);

    /**
     * Host Variable TGA-MOD-CALC
     * 
     */
    string getModCalc();

    void setModCalc(string modCalc);

    /**
     * Nullable property for TGA-MOD-CALC
     * 
     */
    String getModCalcObj();

    void setModCalcObj(String modCalcObj);

    /**
     * Host Variable TGA-IMP-AZ
     * 
     */
    decimal(15, 3) getImpAz();

    void setImpAz(decimal(15, 3) impAz);

    /**
     * Nullable property for TGA-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable TGA-IMP-ADER
     * 
     */
    decimal(15, 3) getImpAder();

    void setImpAder(decimal(15, 3) impAder);

    /**
     * Nullable property for TGA-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable TGA-IMP-TFR
     * 
     */
    decimal(15, 3) getImpTfr();

    void setImpTfr(decimal(15, 3) impTfr);

    /**
     * Nullable property for TGA-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable TGA-IMP-VOLO
     * 
     */
    decimal(15, 3) getImpVolo();

    void setImpVolo(decimal(15, 3) impVolo);

    /**
     * Nullable property for TGA-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable TGA-VIS-END2000
     * 
     */
    decimal(15, 3) getVisEnd2000();

    void setVisEnd2000(decimal(15, 3) visEnd2000);

    /**
     * Nullable property for TGA-VIS-END2000
     * 
     */
    AfDecimal getVisEnd2000Obj();

    void setVisEnd2000Obj(AfDecimal visEnd2000Obj);

    /**
     * Host Variable TGA-DT-VLDT-PROD-DB
     * 
     */
    string getDtVldtProdDb();

    void setDtVldtProdDb(string dtVldtProdDb);

    /**
     * Nullable property for TGA-DT-VLDT-PROD-DB
     * 
     */
    String getDtVldtProdDbObj();

    void setDtVldtProdDbObj(String dtVldtProdDbObj);

    /**
     * Host Variable TGA-DT-INI-VAL-TAR-DB
     * 
     */
    string getDtIniValTarDb();

    void setDtIniValTarDb(string dtIniValTarDb);

    /**
     * Nullable property for TGA-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDbObj();

    void setDtIniValTarDbObj(String dtIniValTarDbObj);

    /**
     * Host Variable TGA-IMPB-VIS-END2000
     * 
     */
    decimal(15, 3) getImpbVisEnd2000();

    void setImpbVisEnd2000(decimal(15, 3) impbVisEnd2000);

    /**
     * Nullable property for TGA-IMPB-VIS-END2000
     * 
     */
    AfDecimal getImpbVisEnd2000Obj();

    void setImpbVisEnd2000Obj(AfDecimal impbVisEnd2000Obj);

    /**
     * Host Variable TGA-REN-INI-TS-TEC-0
     * 
     */
    decimal(15, 3) getRenIniTsTec0();

    void setRenIniTsTec0(decimal(15, 3) renIniTsTec0);

    /**
     * Nullable property for TGA-REN-INI-TS-TEC-0
     * 
     */
    AfDecimal getRenIniTsTec0Obj();

    void setRenIniTsTec0Obj(AfDecimal renIniTsTec0Obj);

    /**
     * Host Variable TGA-PC-RIP-PRE
     * 
     */
    decimal(6, 3) getPcRipPre();

    void setPcRipPre(decimal(6, 3) pcRipPre);

    /**
     * Nullable property for TGA-PC-RIP-PRE
     * 
     */
    AfDecimal getPcRipPreObj();

    void setPcRipPreObj(AfDecimal pcRipPreObj);

    /**
     * Host Variable TGA-FL-IMPORTI-FORZ
     * 
     */
    char getFlImportiForz();

    void setFlImportiForz(char flImportiForz);

    /**
     * Nullable property for TGA-FL-IMPORTI-FORZ
     * 
     */
    Character getFlImportiForzObj();

    void setFlImportiForzObj(Character flImportiForzObj);

    /**
     * Host Variable TGA-PRSTZ-INI-NFORZ
     * 
     */
    decimal(15, 3) getPrstzIniNforz();

    void setPrstzIniNforz(decimal(15, 3) prstzIniNforz);

    /**
     * Nullable property for TGA-PRSTZ-INI-NFORZ
     * 
     */
    AfDecimal getPrstzIniNforzObj();

    void setPrstzIniNforzObj(AfDecimal prstzIniNforzObj);

    /**
     * Host Variable TGA-VIS-END2000-NFORZ
     * 
     */
    decimal(15, 3) getVisEnd2000Nforz();

    void setVisEnd2000Nforz(decimal(15, 3) visEnd2000Nforz);

    /**
     * Nullable property for TGA-VIS-END2000-NFORZ
     * 
     */
    AfDecimal getVisEnd2000NforzObj();

    void setVisEnd2000NforzObj(AfDecimal visEnd2000NforzObj);

    /**
     * Host Variable TGA-INTR-MORA
     * 
     */
    decimal(15, 3) getIntrMora();

    void setIntrMora(decimal(15, 3) intrMora);

    /**
     * Nullable property for TGA-INTR-MORA
     * 
     */
    AfDecimal getIntrMoraObj();

    void setIntrMoraObj(AfDecimal intrMoraObj);

    /**
     * Host Variable TGA-MANFEE-ANTIC
     * 
     */
    decimal(15, 3) getManfeeAntic();

    void setManfeeAntic(decimal(15, 3) manfeeAntic);

    /**
     * Nullable property for TGA-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAnticObj();

    void setManfeeAnticObj(AfDecimal manfeeAnticObj);

    /**
     * Host Variable TGA-MANFEE-RICOR
     * 
     */
    decimal(15, 3) getManfeeRicor();

    void setManfeeRicor(decimal(15, 3) manfeeRicor);

    /**
     * Nullable property for TGA-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicorObj();

    void setManfeeRicorObj(AfDecimal manfeeRicorObj);

    /**
     * Host Variable TGA-PRE-UNI-RIVTO
     * 
     */
    decimal(15, 3) getPreUniRivto();

    void setPreUniRivto(decimal(15, 3) preUniRivto);

    /**
     * Nullable property for TGA-PRE-UNI-RIVTO
     * 
     */
    AfDecimal getPreUniRivtoObj();

    void setPreUniRivtoObj(AfDecimal preUniRivtoObj);

    /**
     * Host Variable TGA-PROV-1AA-ACQ
     * 
     */
    decimal(15, 3) getProv1aaAcq();

    void setProv1aaAcq(decimal(15, 3) prov1aaAcq);

    /**
     * Nullable property for TGA-PROV-1AA-ACQ
     * 
     */
    AfDecimal getProv1aaAcqObj();

    void setProv1aaAcqObj(AfDecimal prov1aaAcqObj);

    /**
     * Host Variable TGA-PROV-2AA-ACQ
     * 
     */
    decimal(15, 3) getProv2aaAcq();

    void setProv2aaAcq(decimal(15, 3) prov2aaAcq);

    /**
     * Nullable property for TGA-PROV-2AA-ACQ
     * 
     */
    AfDecimal getProv2aaAcqObj();

    void setProv2aaAcqObj(AfDecimal prov2aaAcqObj);

    /**
     * Host Variable TGA-PROV-RICOR
     * 
     */
    decimal(15, 3) getProvRicor();

    void setProvRicor(decimal(15, 3) provRicor);

    /**
     * Nullable property for TGA-PROV-RICOR
     * 
     */
    AfDecimal getProvRicorObj();

    void setProvRicorObj(AfDecimal provRicorObj);

    /**
     * Host Variable TGA-PROV-INC
     * 
     */
    decimal(15, 3) getProvInc();

    void setProvInc(decimal(15, 3) provInc);

    /**
     * Nullable property for TGA-PROV-INC
     * 
     */
    AfDecimal getProvIncObj();

    void setProvIncObj(AfDecimal provIncObj);

    /**
     * Host Variable TGA-ALQ-PROV-ACQ
     * 
     */
    decimal(6, 3) getAlqProvAcq();

    void setAlqProvAcq(decimal(6, 3) alqProvAcq);

    /**
     * Nullable property for TGA-ALQ-PROV-ACQ
     * 
     */
    AfDecimal getAlqProvAcqObj();

    void setAlqProvAcqObj(AfDecimal alqProvAcqObj);

    /**
     * Host Variable TGA-ALQ-PROV-INC
     * 
     */
    decimal(6, 3) getAlqProvInc();

    void setAlqProvInc(decimal(6, 3) alqProvInc);

    /**
     * Nullable property for TGA-ALQ-PROV-INC
     * 
     */
    AfDecimal getAlqProvIncObj();

    void setAlqProvIncObj(AfDecimal alqProvIncObj);

    /**
     * Host Variable TGA-ALQ-PROV-RICOR
     * 
     */
    decimal(6, 3) getAlqProvRicor();

    void setAlqProvRicor(decimal(6, 3) alqProvRicor);

    /**
     * Nullable property for TGA-ALQ-PROV-RICOR
     * 
     */
    AfDecimal getAlqProvRicorObj();

    void setAlqProvRicorObj(AfDecimal alqProvRicorObj);

    /**
     * Host Variable TGA-IMPB-PROV-ACQ
     * 
     */
    decimal(15, 3) getImpbProvAcq();

    void setImpbProvAcq(decimal(15, 3) impbProvAcq);

    /**
     * Nullable property for TGA-IMPB-PROV-ACQ
     * 
     */
    AfDecimal getImpbProvAcqObj();

    void setImpbProvAcqObj(AfDecimal impbProvAcqObj);

    /**
     * Host Variable TGA-IMPB-PROV-INC
     * 
     */
    decimal(15, 3) getImpbProvInc();

    void setImpbProvInc(decimal(15, 3) impbProvInc);

    /**
     * Nullable property for TGA-IMPB-PROV-INC
     * 
     */
    AfDecimal getImpbProvIncObj();

    void setImpbProvIncObj(AfDecimal impbProvIncObj);

    /**
     * Host Variable TGA-IMPB-PROV-RICOR
     * 
     */
    decimal(15, 3) getImpbProvRicor();

    void setImpbProvRicor(decimal(15, 3) impbProvRicor);

    /**
     * Nullable property for TGA-IMPB-PROV-RICOR
     * 
     */
    AfDecimal getImpbProvRicorObj();

    void setImpbProvRicorObj(AfDecimal impbProvRicorObj);

    /**
     * Host Variable TGA-FL-PROV-FORZ
     * 
     */
    char getFlProvForz();

    void setFlProvForz(char flProvForz);

    /**
     * Nullable property for TGA-FL-PROV-FORZ
     * 
     */
    Character getFlProvForzObj();

    void setFlProvForzObj(Character flProvForzObj);

    /**
     * Host Variable TGA-PRSTZ-AGG-INI
     * 
     */
    decimal(15, 3) getPrstzAggIni();

    void setPrstzAggIni(decimal(15, 3) prstzAggIni);

    /**
     * Nullable property for TGA-PRSTZ-AGG-INI
     * 
     */
    AfDecimal getPrstzAggIniObj();

    void setPrstzAggIniObj(AfDecimal prstzAggIniObj);

    /**
     * Host Variable TGA-INCR-PRE
     * 
     */
    decimal(15, 3) getIncrPre();

    void setIncrPre(decimal(15, 3) incrPre);

    /**
     * Nullable property for TGA-INCR-PRE
     * 
     */
    AfDecimal getIncrPreObj();

    void setIncrPreObj(AfDecimal incrPreObj);

    /**
     * Host Variable TGA-INCR-PRSTZ
     * 
     */
    decimal(15, 3) getIncrPrstz();

    void setIncrPrstz(decimal(15, 3) incrPrstz);

    /**
     * Nullable property for TGA-INCR-PRSTZ
     * 
     */
    AfDecimal getIncrPrstzObj();

    void setIncrPrstzObj(AfDecimal incrPrstzObj);

    /**
     * Host Variable TGA-DT-ULT-ADEG-PRE-PR-DB
     * 
     */
    string getDtUltAdegPrePrDb();

    void setDtUltAdegPrePrDb(string dtUltAdegPrePrDb);

    /**
     * Nullable property for TGA-DT-ULT-ADEG-PRE-PR-DB
     * 
     */
    String getDtUltAdegPrePrDbObj();

    void setDtUltAdegPrePrDbObj(String dtUltAdegPrePrDbObj);

    /**
     * Host Variable TGA-PRSTZ-AGG-ULT
     * 
     */
    decimal(15, 3) getPrstzAggUlt();

    void setPrstzAggUlt(decimal(15, 3) prstzAggUlt);

    /**
     * Nullable property for TGA-PRSTZ-AGG-ULT
     * 
     */
    AfDecimal getPrstzAggUltObj();

    void setPrstzAggUltObj(AfDecimal prstzAggUltObj);

    /**
     * Host Variable TGA-TS-RIVAL-NET
     * 
     */
    decimal(14, 9) getTsRivalNet();

    void setTsRivalNet(decimal(14, 9) tsRivalNet);

    /**
     * Nullable property for TGA-TS-RIVAL-NET
     * 
     */
    AfDecimal getTsRivalNetObj();

    void setTsRivalNetObj(AfDecimal tsRivalNetObj);

    /**
     * Host Variable TGA-PRE-PATTUITO
     * 
     */
    decimal(15, 3) getPrePattuito();

    void setPrePattuito(decimal(15, 3) prePattuito);

    /**
     * Nullable property for TGA-PRE-PATTUITO
     * 
     */
    AfDecimal getPrePattuitoObj();

    void setPrePattuitoObj(AfDecimal prePattuitoObj);

    /**
     * Host Variable TGA-TP-RIVAL
     * 
     */
    string getTpRival();

    void setTpRival(string tpRival);

    /**
     * Nullable property for TGA-TP-RIVAL
     * 
     */
    String getTpRivalObj();

    void setTpRivalObj(String tpRivalObj);

    /**
     * Host Variable TGA-RIS-MAT
     * 
     */
    decimal(15, 3) getRisMat();

    void setRisMat(decimal(15, 3) risMat);

    /**
     * Nullable property for TGA-RIS-MAT
     * 
     */
    AfDecimal getRisMatObj();

    void setRisMatObj(AfDecimal risMatObj);

    /**
     * Host Variable TGA-CPT-MIN-SCAD
     * 
     */
    decimal(15, 3) getCptMinScad();

    void setCptMinScad(decimal(15, 3) cptMinScad);

    /**
     * Nullable property for TGA-CPT-MIN-SCAD
     * 
     */
    AfDecimal getCptMinScadObj();

    void setCptMinScadObj(AfDecimal cptMinScadObj);

    /**
     * Host Variable TGA-COMMIS-GEST
     * 
     */
    decimal(15, 3) getCommisGest();

    void setCommisGest(decimal(15, 3) commisGest);

    /**
     * Nullable property for TGA-COMMIS-GEST
     * 
     */
    AfDecimal getCommisGestObj();

    void setCommisGestObj(AfDecimal commisGestObj);

    /**
     * Host Variable TGA-TP-MANFEE-APPL
     * 
     */
    string getTpManfeeAppl();

    void setTpManfeeAppl(string tpManfeeAppl);

    /**
     * Nullable property for TGA-TP-MANFEE-APPL
     * 
     */
    String getTpManfeeApplObj();

    void setTpManfeeApplObj(String tpManfeeApplObj);

    /**
     * Host Variable TGA-DS-RIGA
     * 
     */
    long getTgaDsRiga();

    void setTgaDsRiga(long tgaDsRiga);

    /**
     * Host Variable TGA-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable TGA-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable TGA-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable TGA-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable TGA-DS-UTENTE
     * 
     */
    string getDsUtente();

    void setDsUtente(string dsUtente);

    /**
     * Host Variable TGA-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable TGA-PC-COMMIS-GEST
     * 
     */
    decimal(6, 3) getPcCommisGest();

    void setPcCommisGest(decimal(6, 3) pcCommisGest);

    /**
     * Nullable property for TGA-PC-COMMIS-GEST
     * 
     */
    AfDecimal getPcCommisGestObj();

    void setPcCommisGestObj(AfDecimal pcCommisGestObj);

    /**
     * Host Variable TGA-NUM-GG-RIVAL
     * 
     */
    int getNumGgRival();

    void setNumGgRival(int numGgRival);

    /**
     * Nullable property for TGA-NUM-GG-RIVAL
     * 
     */
    Integer getNumGgRivalObj();

    void setNumGgRivalObj(Integer numGgRivalObj);

    /**
     * Host Variable TGA-IMP-TRASFE
     * 
     */
    decimal(15, 3) getImpTrasfe();

    void setImpTrasfe(decimal(15, 3) impTrasfe);

    /**
     * Nullable property for TGA-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfeObj();

    void setImpTrasfeObj(AfDecimal impTrasfeObj);

    /**
     * Host Variable TGA-IMP-TFR-STRC
     * 
     */
    decimal(15, 3) getImpTfrStrc();

    void setImpTfrStrc(decimal(15, 3) impTfrStrc);

    /**
     * Nullable property for TGA-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrcObj();

    void setImpTfrStrcObj(AfDecimal impTfrStrcObj);

    /**
     * Host Variable TGA-ACQ-EXP
     * 
     */
    decimal(15, 3) getAcqExp();

    void setAcqExp(decimal(15, 3) acqExp);

    /**
     * Nullable property for TGA-ACQ-EXP
     * 
     */
    AfDecimal getAcqExpObj();

    void setAcqExpObj(AfDecimal acqExpObj);

    /**
     * Host Variable TGA-REMUN-ASS
     * 
     */
    decimal(15, 3) getRemunAss();

    void setRemunAss(decimal(15, 3) remunAss);

    /**
     * Nullable property for TGA-REMUN-ASS
     * 
     */
    AfDecimal getRemunAssObj();

    void setRemunAssObj(AfDecimal remunAssObj);

    /**
     * Host Variable TGA-COMMIS-INTER
     * 
     */
    decimal(15, 3) getCommisInter();

    void setCommisInter(decimal(15, 3) commisInter);

    /**
     * Nullable property for TGA-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInterObj();

    void setCommisInterObj(AfDecimal commisInterObj);

    /**
     * Host Variable TGA-ALQ-REMUN-ASS
     * 
     */
    decimal(6, 3) getAlqRemunAss();

    void setAlqRemunAss(decimal(6, 3) alqRemunAss);

    /**
     * Nullable property for TGA-ALQ-REMUN-ASS
     * 
     */
    AfDecimal getAlqRemunAssObj();

    void setAlqRemunAssObj(AfDecimal alqRemunAssObj);

    /**
     * Host Variable TGA-ALQ-COMMIS-INTER
     * 
     */
    decimal(6, 3) getAlqCommisInter();

    void setAlqCommisInter(decimal(6, 3) alqCommisInter);

    /**
     * Nullable property for TGA-ALQ-COMMIS-INTER
     * 
     */
    AfDecimal getAlqCommisInterObj();

    void setAlqCommisInterObj(AfDecimal alqCommisInterObj);

    /**
     * Host Variable TGA-IMPB-REMUN-ASS
     * 
     */
    decimal(15, 3) getImpbRemunAss();

    void setImpbRemunAss(decimal(15, 3) impbRemunAss);

    /**
     * Nullable property for TGA-IMPB-REMUN-ASS
     * 
     */
    AfDecimal getImpbRemunAssObj();

    void setImpbRemunAssObj(AfDecimal impbRemunAssObj);

    /**
     * Host Variable TGA-IMPB-COMMIS-INTER
     * 
     */
    decimal(15, 3) getImpbCommisInter();

    void setImpbCommisInter(decimal(15, 3) impbCommisInter);

    /**
     * Nullable property for TGA-IMPB-COMMIS-INTER
     * 
     */
    AfDecimal getImpbCommisInterObj();

    void setImpbCommisInterObj(AfDecimal impbCommisInterObj);

    /**
     * Host Variable TGA-COS-RUN-ASSVA
     * 
     */
    decimal(15, 3) getCosRunAssva();

    void setCosRunAssva(decimal(15, 3) cosRunAssva);

    /**
     * Nullable property for TGA-COS-RUN-ASSVA
     * 
     */
    AfDecimal getCosRunAssvaObj();

    void setCosRunAssvaObj(AfDecimal cosRunAssvaObj);

    /**
     * Host Variable TGA-COS-RUN-ASSVA-IDC
     * 
     */
    decimal(15, 3) getCosRunAssvaIdc();

    void setCosRunAssvaIdc(decimal(15, 3) cosRunAssvaIdc);

    /**
     * Nullable property for TGA-COS-RUN-ASSVA-IDC
     * 
     */
    AfDecimal getCosRunAssvaIdcObj();

    void setCosRunAssvaIdcObj(AfDecimal cosRunAssvaIdcObj);

}

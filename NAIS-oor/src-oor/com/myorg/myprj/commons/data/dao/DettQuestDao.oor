
package com.myorg.myprj.commons.data.dao;

import java.lang.Override;
import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IDettQuest;


/**
 * Data Access Object(DAO) for table [DETT_QUEST]
 * 
 */
public class DettQuestDao
    extends <IDettQuest> BaseSqlDao
{

    private Cursor cIdUpdEffDeq;
    private Cursor cIdpEffDeq;
    private Cursor cIdpCpzDeq;
    private final <IDettQuest> IRowMapper selectByDeqDsRigaRm := buildNamedRowMapper(IDettQuest.clazz, "idDettQuest", "idQuest", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codQuestObj", "codDomObj", "codDomCollgObj", "rispNumObj", "rispFlObj", "rispTxtVcharObj", "rispTsObj", "rispImpObj", "rispPcObj", "rispDtDbObj", "rispKeyObj", "tpRispObj", "idCompQuestObj", "valRispVcharObj", "deqDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public DettQuestDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public <IDettQuest> Class getToClass() {
        return IDettQuest.clazz;
    }

    public IDettQuest selectByDeqDsRiga(long deqDsRiga, IDettQuest iDettQuest) {
        return buildQuery("selectByDeqDsRiga").bind("deqDsRiga", deqDsRiga).rowMapper(selectByDeqDsRigaRm).singleResult(iDettQuest);
    }

    public DbAccessStatus insertRec(IDettQuest iDettQuest) {
        return buildQuery("insertRec").bind(iDettQuest).executeInsert();
    }

    public DbAccessStatus updateRec(IDettQuest iDettQuest) {
        return buildQuery("updateRec").bind(iDettQuest).executeUpdate();
    }

    public DbAccessStatus deleteByDeqDsRiga(long deqDsRiga) {
        return buildQuery("deleteByDeqDsRiga").bind("deqDsRiga", deqDsRiga).executeDelete();
    }

    public IDettQuest selectRec(int deqIdDettQuest, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, IDettQuest iDettQuest) {
        return buildQuery("selectRec").bind("deqIdDettQuest", deqIdDettQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDeqDsRigaRm).singleResult(iDettQuest);
    }

    public DbAccessStatus updateRec1(IDettQuest iDettQuest) {
        return buildQuery("updateRec1").bind(iDettQuest).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffDeq(int deqIdDettQuest, long wsTsInfinito, string wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffDeq := buildQuery("openCIdUpdEffDeq").bind("deqIdDettQuest", deqIdDettQuest).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffDeq() {
        return closeCursor(cIdUpdEffDeq);
    }

    public IDettQuest fetchCIdUpdEffDeq(IDettQuest iDettQuest) {
        return fetch(cIdUpdEffDeq, iDettQuest, selectByDeqDsRigaRm);
    }

    public IDettQuest selectRec1(int deqIdQuest, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, IDettQuest iDettQuest) {
        return buildQuery("selectRec1").bind("deqIdQuest", deqIdQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDeqDsRigaRm).singleResult(iDettQuest);
    }

    public DbAccessStatus openCIdpEffDeq(int deqIdQuest, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb) {
        cIdpEffDeq := buildQuery("openCIdpEffDeq").bind("deqIdQuest", deqIdQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffDeq() {
        return closeCursor(cIdpEffDeq);
    }

    public IDettQuest fetchCIdpEffDeq(IDettQuest iDettQuest) {
        return fetch(cIdpEffDeq, iDettQuest, selectByDeqDsRigaRm);
    }

    public IDettQuest selectRec2(int deqIdDettQuest, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, long wsTsCompetenza, IDettQuest iDettQuest) {
        return buildQuery("selectRec2").bind("deqIdDettQuest", deqIdDettQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDeqDsRigaRm).singleResult(iDettQuest);
    }

    public IDettQuest selectRec3(int deqIdQuest, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, long wsTsCompetenza, IDettQuest iDettQuest) {
        return buildQuery("selectRec3").bind("deqIdQuest", deqIdQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDeqDsRigaRm).singleResult(iDettQuest);
    }

    public DbAccessStatus openCIdpCpzDeq(int deqIdQuest, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzDeq := buildQuery("openCIdpCpzDeq").bind("deqIdQuest", deqIdQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzDeq() {
        return closeCursor(cIdpCpzDeq);
    }

    public IDettQuest fetchCIdpCpzDeq(IDettQuest iDettQuest) {
        return fetch(cIdpCpzDeq, iDettQuest, selectByDeqDsRigaRm);
    }

}

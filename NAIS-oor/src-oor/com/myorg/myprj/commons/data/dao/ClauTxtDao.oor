
package com.myorg.myprj.commons.data.dao;

import java.lang.Override;
import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IClauTxt;


/**
 * Data Access Object(DAO) for table [CLAU_TXT]
 * 
 */
public class ClauTxtDao
    extends <IClauTxt> BaseSqlDao
{

    private Cursor cIdUpdEffClt;
    private Cursor cIdoEffClt;
    private Cursor cIdoCpzClt;
    private final <IClauTxt> IRowMapper selectByCltDsRigaRm := buildNamedRowMapper(IClauTxt.clazz, "idClauTxt", "cltIdOgg", "cltTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpClauObj", "codClauObj", "descBreveObj", "descLngVcharObj", "cltDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public ClauTxtDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public <IClauTxt> Class getToClass() {
        return IClauTxt.clazz;
    }

    public IClauTxt selectByCltDsRiga(long cltDsRiga, IClauTxt iClauTxt) {
        return buildQuery("selectByCltDsRiga").bind("cltDsRiga", cltDsRiga).rowMapper(selectByCltDsRigaRm).singleResult(iClauTxt);
    }

    public DbAccessStatus insertRec(IClauTxt iClauTxt) {
        return buildQuery("insertRec").bind(iClauTxt).executeInsert();
    }

    public DbAccessStatus updateRec(IClauTxt iClauTxt) {
        return buildQuery("updateRec").bind(iClauTxt).executeUpdate();
    }

    public DbAccessStatus deleteByCltDsRiga(long cltDsRiga) {
        return buildQuery("deleteByCltDsRiga").bind("cltDsRiga", cltDsRiga).executeDelete();
    }

    public IClauTxt selectRec(int cltIdClauTxt, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, IClauTxt iClauTxt) {
        return buildQuery("selectRec").bind("cltIdClauTxt", cltIdClauTxt).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByCltDsRigaRm).singleResult(iClauTxt);
    }

    public DbAccessStatus updateRec1(IClauTxt iClauTxt) {
        return buildQuery("updateRec1").bind(iClauTxt).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffClt(int cltIdClauTxt, long wsTsInfinito, string wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffClt := buildQuery("openCIdUpdEffClt").bind("cltIdClauTxt", cltIdClauTxt).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffClt() {
        return closeCursor(cIdUpdEffClt);
    }

    public IClauTxt fetchCIdUpdEffClt(IClauTxt iClauTxt) {
        return fetch(cIdUpdEffClt, iClauTxt, selectByCltDsRigaRm);
    }

    public IClauTxt selectRec1(int cltIdOgg, string cltTpOgg, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, IClauTxt iClauTxt) {
        return buildQuery("selectRec1").bind("cltIdOgg", cltIdOgg).bind("cltTpOgg", cltTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByCltDsRigaRm).singleResult(iClauTxt);
    }

    public DbAccessStatus openCIdoEffClt(int cltIdOgg, string cltTpOgg, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb) {
        cIdoEffClt := buildQuery("openCIdoEffClt").bind("cltIdOgg", cltIdOgg).bind("cltTpOgg", cltTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffClt() {
        return closeCursor(cIdoEffClt);
    }

    public IClauTxt fetchCIdoEffClt(IClauTxt iClauTxt) {
        return fetch(cIdoEffClt, iClauTxt, selectByCltDsRigaRm);
    }

    public IClauTxt selectRec2(int cltIdClauTxt, int idsv0003CodiceCompagniaAnia, string wsDataInizioEffettoDb, long wsTsCompetenza, IClauTxt iClauTxt) {
        return buildQuery("selectRec2").bind("cltIdClauTxt", cltIdClauTxt).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByCltDsRigaRm).singleResult(iClauTxt);
    }

    public IClauTxt selectRec3(IClauTxt iClauTxt) {
        return buildQuery("selectRec3").bind(iClauTxt).rowMapper(selectByCltDsRigaRm).singleResult(iClauTxt);
    }

    public DbAccessStatus openCIdoCpzClt(IClauTxt iClauTxt) {
        cIdoCpzClt := buildQuery("openCIdoCpzClt").bind(iClauTxt).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzClt() {
        return closeCursor(cIdoCpzClt);
    }

    public IClauTxt fetchCIdoCpzClt(IClauTxt iClauTxt) {
        return fetch(cIdoCpzClt, iClauTxt, selectByCltDsRigaRm);
    }

}

package com.myorg.myprj;

import java.lang.Override;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;

import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.commons.data.dao.MatrValVarDao;
import com.myorg.myprj.commons.data.to.IMatrValVar;
import com.myorg.myprj.copy.Sqlca;
import com.myorg.myprj.ws.Idsv0003;
import com.myorg.myprj.ws.Ldbs1400Data;
import com.myorg.myprj.ws.MatrValVarLdbs1400;
import com.myorg.myprj.ws.redefines.MvvIdpMatrValVar;
import com.myorg.myprj.ws.redefines.MvvTipoMovimento;

/**Original name: LDBS1400<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  NOVEMBRE 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
class Ldbs1400 extends Program implements IMatrValVar {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca := new Sqlca();
	private DbAccessStatus dbAccessStatus := new DbAccessStatus(sqlca);
	private MatrValVarDao matrValVarDao := new MatrValVarDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Ldbs1400Data ws := new Ldbs1400Data();
	//Original name: IDSV0003
	private Idsv0003 idsv0003;
	//Original name: MATR-VAL-VAR
	private MatrValVarLdbs1400 matrValVar;


	//==== METHODS ====
	/**Original name: PROGRAM_LDBS1400_FIRST_SENTENCES<br>*/
	public long execute(Idsv0003 idsv0003, MatrValVarLdbs1400 matrValVar) {
		this.idsv0003 := idsv0003;
		this.matrValVar := matrValVar;
		// COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
		a000Inizio();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              PERFORM A200-ELABORA                THRU A200-EX
		//           END-IF.
		if (this.idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: PERFORM A200-ELABORA                THRU A200-EX
			a200Elabora();
		endif
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Ldbs1400 getInstance() {
		return (Ldbs1400)Programs.getInstance(Ldbs1400.clazz);
	}

	/**Original name: A000-INIZIO<br>*/
	private void a000Inizio() {
		// COB_CODE: MOVE 'LDBS1400'   TO IDSV0003-COD-SERVIZIO-BE.
		idsv0003.getCampiEsito().setCodServizioBe("LDBS1400");
		// COB_CODE: MOVE 'MATR_VAL_VAR' TO IDSV0003-NOME-TABELLA.
		idsv0003.getCampiEsito().setNomeTabella("MATR_VAL_VAR");
		// COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
		idsv0003.getReturnCode().setReturnCode("00");
		// COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
		//                                              IDSV0003-NUM-RIGHE-LETTE.
		idsv0003.getSqlcode().setSqlcode(0);
		idsv0003.getCampiEsito().setNumRigheLette(0);
		// COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
		//                                              IDSV0003-KEY-TABELLA.
		idsv0003.getCampiEsito().setDescrizErrDb2("");
		idsv0003.getCampiEsito().setKeyTabella("");
		// COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
		a001TrattaDateTimestamp();
	}

	/**Original name: A100-CHECK-RETURN-CODE<br>*/
	private void a100CheckReturnCode() {
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-EVALUATE
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
			idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
			// COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
			// COB_CODE: EVALUATE IDSV0003-SQLCODE
			//               WHEN ZERO
			//                             CONTINUE
			//               WHEN +100
			//                  END-IF
			//               WHEN -811
			//                             CONTINUE
			//               WHEN OTHER
			//                             SET IDSV0003-SQL-ERROR TO TRUE
			//           END-EVALUATE
			if (idsv0003.getSqlcode().getSqlcode() = 0) then
				// COB_CODE: CONTINUE
				//continue
			elseif (idsv0003.getSqlcode().getSqlcode() = 100) then
				// COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
				//              IDSV0003-DELETE-LOGICA         OR
				//              IDSV0003-SELECT                OR
				//              IDSV0003-FETCH-FIRST           OR
				//              IDSV0003-FETCH-NEXT            OR
				//              IDSV0003-FETCH-FIRST-MULTIPLE  OR
				//              IDSV0003-FETCH-NEXT-MULTIPLE
				//                      CONTINUE
				//           ELSE
				//                      SET IDSV0003-SQL-ERROR TO TRUE
				//           END-IF
				if (idsv0003.getOperazione().isAggiornamentoStorico() | idsv0003.getOperazione().isDeleteLogica() | idsv0003.getOperazione().isSelect() | idsv0003.getOperazione().isFetchFirst() | idsv0003.getOperazione().isFetchNext() | idsv0003.getOperazione().isFetchFirstMultiple() | idsv0003.getOperazione().isFetchNextMultiple()) then
					// COB_CODE: CONTINUE
					//continue
				else
					// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
					idsv0003.getReturnCode().setSqlError();
				endif
			elseif (idsv0003.getSqlcode().getSqlcode() = -811) then
				// COB_CODE: CONTINUE
				//continue
			else
				// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
				idsv0003.getReturnCode().setSqlError();
			endif
		endif
	}

	/**Original name: A200-ELABORA<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
	private void a200Elabora() {
		// COB_CODE: PERFORM A210-SELECT             THRU A210-EX.
		a210Select();
	}

	/**Original name: A210-SELECT<br>
	 * <pre>----
	 * ----  gestione SELECT
	 * ----</pre>*/
	private void a210Select() {
		// COB_CODE: EXEC SQL
		//             SELECT
		//                ID_MATR_VAL_VAR
		//                ,IDP_MATR_VAL_VAR
		//                ,COD_COMPAGNIA_ANIA
		//                ,TIPO_MOVIMENTO
		//                ,COD_DATO_EXT
		//                ,OBBLIGATORIETA
		//                ,VALORE_DEFAULT
		//                ,COD_STR_DATO_PTF
		//                ,COD_DATO_PTF
		//                ,COD_PARAMETRO
		//                ,OPERAZIONE
		//                ,LIVELLO_OPERAZIONE
		//                ,TIPO_OGGETTO
		//                ,WHERE_CONDITION
		//                ,SERVIZIO_LETTURA
		//                ,MODULO_CALCOLO
		//                ,STEP_VALORIZZATORE
		//                ,STEP_CONVERSAZIONE
		//                ,OPER_LOG_STATO_BUS
		//                ,ARRAY_STATO_BUS
		//                ,OPER_LOG_CAUSALE
		//                ,ARRAY_CAUSALE
		//             INTO
		//                :MVV-ID-MATR-VAL-VAR
		//               ,:MVV-IDP-MATR-VAL-VAR
		//                :IND-MVV-IDP-MATR-VAL-VAR
		//               ,:MVV-COD-COMPAGNIA-ANIA
		//               ,:MVV-TIPO-MOVIMENTO
		//                :IND-MVV-TIPO-MOVIMENTO
		//               ,:MVV-COD-DATO-EXT
		//                :IND-MVV-COD-DATO-EXT
		//               ,:MVV-OBBLIGATORIETA
		//                :IND-MVV-OBBLIGATORIETA
		//               ,:MVV-VALORE-DEFAULT
		//                :IND-MVV-VALORE-DEFAULT
		//               ,:MVV-COD-STR-DATO-PTF
		//                :IND-MVV-COD-STR-DATO-PTF
		//               ,:MVV-COD-DATO-PTF
		//                :IND-MVV-COD-DATO-PTF
		//               ,:MVV-COD-PARAMETRO
		//                :IND-MVV-COD-PARAMETRO
		//               ,:MVV-OPERAZIONE
		//                :IND-MVV-OPERAZIONE
		//               ,:MVV-LIVELLO-OPERAZIONE
		//                :IND-MVV-LIVELLO-OPERAZIONE
		//               ,:MVV-TIPO-OGGETTO
		//                :IND-MVV-TIPO-OGGETTO
		//               ,:MVV-WHERE-CONDITION-VCHAR
		//                :IND-MVV-WHERE-CONDITION
		//               ,:MVV-SERVIZIO-LETTURA
		//                :IND-MVV-SERVIZIO-LETTURA
		//               ,:MVV-MODULO-CALCOLO
		//                :IND-MVV-MODULO-CALCOLO
		//               ,:MVV-STEP-VALORIZZATORE
		//                :IND-MVV-STEP-VALORIZZATORE
		//               ,:MVV-STEP-CONVERSAZIONE
		//                :IND-MVV-STEP-CONVERSAZIONE
		//               ,:MVV-OPER-LOG-STATO-BUS
		//                :IND-MVV-OPER-LOG-STATO-BUS
		//               ,:MVV-ARRAY-STATO-BUS
		//                :IND-MVV-ARRAY-STATO-BUS
		//               ,:MVV-OPER-LOG-CAUSALE
		//                :IND-MVV-OPER-LOG-CAUSALE
		//               ,:MVV-ARRAY-CAUSALE
		//                :IND-MVV-ARRAY-CAUSALE
		//             FROM  MATR_VAL_VAR
		//             WHERE COD_COMPAGNIA_ANIA = :MVV-COD-COMPAGNIA-ANIA
		//             AND   TIPO_MOVIMENTO     = :MVV-TIPO-MOVIMENTO
		//             AND   STEP_VALORIZZATORE = :MVV-STEP-VALORIZZATORE
		//             AND   STEP_CONVERSAZIONE = :MVV-STEP-CONVERSAZIONE
		//           END-EXEC.
		matrValVarDao.selectRec(matrValVar.getMvvCodCompagniaAnia(), matrValVar.getMvvTipoMovimento().getMvvTipoMovimento(), matrValVar.getMvvStepValorizzatore(), matrValVar.getMvvStepConversazione(), this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		endif
	}

	/**Original name: Z100-SET-COLONNE-NULL<br>*/
	private void z100SetColonneNull() {
		// COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
		idsv0003.getCampiEsito().setNumRigheLette((short)1);
		// COB_CODE: IF IND-MVV-IDP-MATR-VAL-VAR = -1
		//              MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getIdpMatrValVar() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
			matrValVar.getMvvIdpMatrValVar().setMvvIdpMatrValVarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MvvIdpMatrValVar.Len.MVV_IDP_MATR_VAL_VAR_NULL));
		endif
		// COB_CODE: IF IND-MVV-TIPO-MOVIMENTO = -1
		//              MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getTipoMovimento() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
			matrValVar.getMvvTipoMovimento().setMvvTipoMovimentoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MvvTipoMovimento.Len.MVV_TIPO_MOVIMENTO_NULL));
		endif
		// COB_CODE: IF IND-MVV-COD-DATO-EXT = -1
		//              MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
		//           END-IF
		if (ws.getIndMatrValVar().getCodDatoExt() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
			matrValVar.setMvvCodDatoExt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_DATO_EXT));
		endif
		// COB_CODE: IF IND-MVV-OBBLIGATORIETA = -1
		//              MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getObbligatorieta() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
			matrValVar.setMvvObbligatorieta(Types.HIGH_CHAR_VAL);
		endif
		// COB_CODE: IF IND-MVV-VALORE-DEFAULT = -1
		//              MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
		//           END-IF
		if (ws.getIndMatrValVar().getValoreDefault() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
			matrValVar.setMvvValoreDefault(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_VALORE_DEFAULT));
		endif
		// COB_CODE: IF IND-MVV-COD-STR-DATO-PTF = -1
		//              MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
		//           END-IF
		if (ws.getIndMatrValVar().getCodStrDatoPtf() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
			matrValVar.setMvvCodStrDatoPtf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_STR_DATO_PTF));
		endif
		// COB_CODE: IF IND-MVV-COD-DATO-PTF = -1
		//              MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
		//           END-IF
		if (ws.getIndMatrValVar().getCodDatoPtf() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
			matrValVar.setMvvCodDatoPtf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_DATO_PTF));
		endif
		// COB_CODE: IF IND-MVV-COD-PARAMETRO = -1
		//              MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
		//           END-IF
		if (ws.getIndMatrValVar().getCodParametro() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
			matrValVar.setMvvCodParametro(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_PARAMETRO));
		endif
		// COB_CODE: IF IND-MVV-OPERAZIONE = -1
		//              MOVE HIGH-VALUES TO MVV-OPERAZIONE
		//           END-IF
		if (ws.getIndMatrValVar().getOperazione() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-OPERAZIONE
			matrValVar.setMvvOperazione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_OPERAZIONE));
		endif
		// COB_CODE: IF IND-MVV-LIVELLO-OPERAZIONE = -1
		//              MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getLivelloOperazione() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
			matrValVar.setMvvLivelloOperazione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_LIVELLO_OPERAZIONE));
		endif
		// COB_CODE: IF IND-MVV-TIPO-OGGETTO = -1
		//              MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getTipoOggetto() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
			matrValVar.setMvvTipoOggetto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_TIPO_OGGETTO));
		endif
		// COB_CODE: IF IND-MVV-WHERE-CONDITION = -1
		//              MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
		//           END-IF
		if (ws.getIndMatrValVar().getWhereCondition() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
			matrValVar.setMvvWhereCondition(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_WHERE_CONDITION));
		endif
		// COB_CODE: IF IND-MVV-SERVIZIO-LETTURA = -1
		//              MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getServizioLettura() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
			matrValVar.setMvvServizioLettura(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_SERVIZIO_LETTURA));
		endif
		// COB_CODE: IF IND-MVV-MODULO-CALCOLO = -1
		//              MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getModuloCalcolo() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
			matrValVar.setMvvModuloCalcolo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_MODULO_CALCOLO));
		endif
		// COB_CODE: IF IND-MVV-STEP-VALORIZZATORE = -1
		//              MOVE HIGH-VALUES TO MVV-STEP-VALORIZZATORE-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getStepValorizzatore() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-STEP-VALORIZZATORE-NULL
			matrValVar.setMvvStepValorizzatore(Types.HIGH_CHAR_VAL);
		endif
		// COB_CODE: IF IND-MVV-STEP-CONVERSAZIONE = -1
		//              MOVE HIGH-VALUES TO MVV-STEP-CONVERSAZIONE-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getStepConversazione() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-STEP-CONVERSAZIONE-NULL
			matrValVar.setMvvStepConversazione(Types.HIGH_CHAR_VAL);
		endif
		// COB_CODE: IF IND-MVV-OPER-LOG-STATO-BUS = -1
		//              MOVE HIGH-VALUES TO MVV-OPER-LOG-STATO-BUS-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getOperLogStatoBus() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-OPER-LOG-STATO-BUS-NULL
			matrValVar.setMvvOperLogStatoBus(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_OPER_LOG_STATO_BUS));
		endif
		// COB_CODE: IF IND-MVV-ARRAY-STATO-BUS = -1
		//              MOVE HIGH-VALUES TO MVV-ARRAY-STATO-BUS
		//           END-IF
		if (ws.getIndMatrValVar().getArrayStatoBus() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-ARRAY-STATO-BUS
			matrValVar.setMvvArrayStatoBus(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_ARRAY_STATO_BUS));
		endif
		// COB_CODE: IF IND-MVV-OPER-LOG-CAUSALE = -1
		//              MOVE HIGH-VALUES TO MVV-OPER-LOG-CAUSALE-NULL
		//           END-IF
		if (ws.getIndMatrValVar().getOperLogCausale() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-OPER-LOG-CAUSALE-NULL
			matrValVar.setMvvOperLogCausale(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_OPER_LOG_CAUSALE));
		endif
		// COB_CODE: IF IND-MVV-ARRAY-CAUSALE = -1
		//              MOVE HIGH-VALUES TO MVV-ARRAY-CAUSALE
		//           END-IF.
		if (ws.getIndMatrValVar().getArrayCausale() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO MVV-ARRAY-CAUSALE
			matrValVar.setMvvArrayCausale(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_ARRAY_CAUSALE));
		endif
	}

	/**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
	private void z950ConvertiXToN() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
	private void a001TrattaDateTimestamp() {
		// COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
		a020ConvertiDtEffetto();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
			a050ValorizzaCptz();
		endif
	}

	/**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
	private void a020ConvertiDtEffetto() {
		// COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
		//                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
		//                END-IF
		if (! Functions.isNumber(idsv0003.getDataInizioEffetto()) | idsv0003.getDataInizioEffetto() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
			ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
			// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
			
			// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
			ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
		endif
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-IF
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
			//              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
			//              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
			//           END-IF
			if (Functions.isNumber(idsv0003.getDataFineEffetto()) & idsv0003.getDataFineEffetto() != 0) then
				// COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
				ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
				// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
				//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
				
				// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
				ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
			endif
		endif
	}

	/**Original name: A050-VALORIZZA-CPTZ<br>*/
	private void a050ValorizzaCptz() {
		// COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMPETENZA  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
		//                END-IF.
		if (! Functions.isNumber(idsv0003.getDataCompetenza()) | idsv0003.getDataCompetenza() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
			ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
		endif
		// COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMP-AGG-STOR  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                                       TO WS-TS-COMPETENZA-AGG-STOR
		//                END-IF.
		if (! Functions.isNumber(idsv0003.getDataCompAggStor()) | idsv0003.getDataCompAggStor() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
			//                               TO WS-TS-COMPETENZA-AGG-STOR
			ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
		endif
	}

	@Override
	public string getArrayCausale() {
		return matrValVar.getMvvArrayCausale();
	}

	@Override
	public void setArrayCausale(string arrayCausale) {
		this.matrValVar.setMvvArrayCausale(arrayCausale);
	}

	@Override
	public String getArrayCausaleObj() {
		if (ws.getIndMatrValVar().getArrayCausale()>= 0) then
		    return getArrayCausale();
		else
		    return null;
		endif;
	}

	@Override
	public void setArrayCausaleObj(String arrayCausaleObj) {
		if (arrayCausaleObj!= null) then
		    setArrayCausale(arrayCausaleObj);
		    ws.getIndMatrValVar().setArrayCausale(0);
		else
		    ws.getIndMatrValVar().setArrayCausale(-1);
		endif;
	}

	@Override
	public string getArrayStatoBus() {
		return matrValVar.getMvvArrayStatoBus();
	}

	@Override
	public void setArrayStatoBus(string arrayStatoBus) {
		this.matrValVar.setMvvArrayStatoBus(arrayStatoBus);
	}

	@Override
	public String getArrayStatoBusObj() {
		if (ws.getIndMatrValVar().getArrayStatoBus()>= 0) then
		    return getArrayStatoBus();
		else
		    return null;
		endif;
	}

	@Override
	public void setArrayStatoBusObj(String arrayStatoBusObj) {
		if (arrayStatoBusObj!= null) then
		    setArrayStatoBus(arrayStatoBusObj);
		    ws.getIndMatrValVar().setArrayStatoBus(0);
		else
		    ws.getIndMatrValVar().setArrayStatoBus(-1);
		endif;
	}

	@Override
	public integer getCodCompagniaAnia() {
		return matrValVar.getMvvCodCompagniaAnia();
	}

	@Override
	public void setCodCompagniaAnia(integer codCompagniaAnia) {
		this.matrValVar.setMvvCodCompagniaAnia(codCompagniaAnia);
	}

	@Override
	public string getCodDatoExt() {
		return matrValVar.getMvvCodDatoExt();
	}

	@Override
	public void setCodDatoExt(string codDatoExt) {
		this.matrValVar.setMvvCodDatoExt(codDatoExt);
	}

	@Override
	public String getCodDatoExtObj() {
		if (ws.getIndMatrValVar().getCodDatoExt()>= 0) then
		    return getCodDatoExt();
		else
		    return null;
		endif;
	}

	@Override
	public void setCodDatoExtObj(String codDatoExtObj) {
		if (codDatoExtObj!= null) then
		    setCodDatoExt(codDatoExtObj);
		    ws.getIndMatrValVar().setCodDatoExt(0);
		else
		    ws.getIndMatrValVar().setCodDatoExt(-1);
		endif;
	}

	@Override
	public string getCodDatoPtf() {
		return matrValVar.getMvvCodDatoPtf();
	}

	@Override
	public void setCodDatoPtf(string codDatoPtf) {
		this.matrValVar.setMvvCodDatoPtf(codDatoPtf);
	}

	@Override
	public String getCodDatoPtfObj() {
		if (ws.getIndMatrValVar().getCodDatoPtf()>= 0) then
		    return getCodDatoPtf();
		else
		    return null;
		endif;
	}

	@Override
	public void setCodDatoPtfObj(String codDatoPtfObj) {
		if (codDatoPtfObj!= null) then
		    setCodDatoPtf(codDatoPtfObj);
		    ws.getIndMatrValVar().setCodDatoPtf(0);
		else
		    ws.getIndMatrValVar().setCodDatoPtf(-1);
		endif;
	}

	@Override
	public string getCodParametro() {
		return matrValVar.getMvvCodParametro();
	}

	@Override
	public void setCodParametro(string codParametro) {
		this.matrValVar.setMvvCodParametro(codParametro);
	}

	@Override
	public String getCodParametroObj() {
		if (ws.getIndMatrValVar().getCodParametro()>= 0) then
		    return getCodParametro();
		else
		    return null;
		endif;
	}

	@Override
	public void setCodParametroObj(String codParametroObj) {
		if (codParametroObj!= null) then
		    setCodParametro(codParametroObj);
		    ws.getIndMatrValVar().setCodParametro(0);
		else
		    ws.getIndMatrValVar().setCodParametro(-1);
		endif;
	}

	@Override
	public string getCodStrDatoPtf() {
		return matrValVar.getMvvCodStrDatoPtf();
	}

	@Override
	public void setCodStrDatoPtf(string codStrDatoPtf) {
		this.matrValVar.setMvvCodStrDatoPtf(codStrDatoPtf);
	}

	@Override
	public String getCodStrDatoPtfObj() {
		if (ws.getIndMatrValVar().getCodStrDatoPtf()>= 0) then
		    return getCodStrDatoPtf();
		else
		    return null;
		endif;
	}

	@Override
	public void setCodStrDatoPtfObj(String codStrDatoPtfObj) {
		if (codStrDatoPtfObj!= null) then
		    setCodStrDatoPtf(codStrDatoPtfObj);
		    ws.getIndMatrValVar().setCodStrDatoPtf(0);
		else
		    ws.getIndMatrValVar().setCodStrDatoPtf(-1);
		endif;
	}

	@Override
	public integer getIdMatrValVar() {
		return matrValVar.getMvvIdMatrValVar();
	}

	@Override
	public void setIdMatrValVar(integer idMatrValVar) {
		this.matrValVar.setMvvIdMatrValVar(idMatrValVar);
	}

	@Override
	public integer getIdpMatrValVar() {
		return matrValVar.getMvvIdpMatrValVar().getMvvIdpMatrValVar();
	}

	@Override
	public void setIdpMatrValVar(integer idpMatrValVar) {
		this.matrValVar.getMvvIdpMatrValVar().setMvvIdpMatrValVar(idpMatrValVar);
	}

	@Override
	public Integer getIdpMatrValVarObj() {
		if (ws.getIndMatrValVar().getIdpMatrValVar()>= 0) then
		    return getIdpMatrValVar();
		else
		    return null;
		endif;
	}

	@Override
	public void setIdpMatrValVarObj(Integer idpMatrValVarObj) {
		if (idpMatrValVarObj!= null) then
		    setIdpMatrValVar(idpMatrValVarObj);
		    ws.getIndMatrValVar().setIdpMatrValVar(0);
		else
		    ws.getIndMatrValVar().setIdpMatrValVar(-1);
		endif;
	}

	@Override
	public string getLivelloOperazione() {
		return matrValVar.getMvvLivelloOperazione();
	}

	@Override
	public void setLivelloOperazione(string livelloOperazione) {
		this.matrValVar.setMvvLivelloOperazione(livelloOperazione);
	}

	@Override
	public String getLivelloOperazioneObj() {
		if (ws.getIndMatrValVar().getLivelloOperazione()>= 0) then
		    return getLivelloOperazione();
		else
		    return null;
		endif;
	}

	@Override
	public void setLivelloOperazioneObj(String livelloOperazioneObj) {
		if (livelloOperazioneObj!= null) then
		    setLivelloOperazione(livelloOperazioneObj);
		    ws.getIndMatrValVar().setLivelloOperazione(0);
		else
		    ws.getIndMatrValVar().setLivelloOperazione(-1);
		endif;
	}

	@Override
	public string getModuloCalcolo() {
		return matrValVar.getMvvModuloCalcolo();
	}

	@Override
	public void setModuloCalcolo(string moduloCalcolo) {
		this.matrValVar.setMvvModuloCalcolo(moduloCalcolo);
	}

	@Override
	public String getModuloCalcoloObj() {
		if (ws.getIndMatrValVar().getModuloCalcolo()>= 0) then
		    return getModuloCalcolo();
		else
		    return null;
		endif;
	}

	@Override
	public void setModuloCalcoloObj(String moduloCalcoloObj) {
		if (moduloCalcoloObj!= null) then
		    setModuloCalcolo(moduloCalcoloObj);
		    ws.getIndMatrValVar().setModuloCalcolo(0);
		else
		    ws.getIndMatrValVar().setModuloCalcolo(-1);
		endif;
	}

	@Override
	public char getObbligatorieta() {
		return matrValVar.getMvvObbligatorieta();
	}

	@Override
	public void setObbligatorieta(char obbligatorieta) {
		this.matrValVar.setMvvObbligatorieta(obbligatorieta);
	}

	@Override
	public Character getObbligatorietaObj() {
		if (ws.getIndMatrValVar().getObbligatorieta()>= 0) then
		    return getObbligatorieta();
		else
		    return null;
		endif;
	}

	@Override
	public void setObbligatorietaObj(Character obbligatorietaObj) {
		if (obbligatorietaObj!= null) then
		    setObbligatorieta(obbligatorietaObj);
		    ws.getIndMatrValVar().setObbligatorieta(0);
		else
		    ws.getIndMatrValVar().setObbligatorieta(-1);
		endif;
	}

	@Override
	public string getOperLogCausale() {
		return matrValVar.getMvvOperLogCausale();
	}

	@Override
	public void setOperLogCausale(string operLogCausale) {
		this.matrValVar.setMvvOperLogCausale(operLogCausale);
	}

	@Override
	public String getOperLogCausaleObj() {
		if (ws.getIndMatrValVar().getOperLogCausale()>= 0) then
		    return getOperLogCausale();
		else
		    return null;
		endif;
	}

	@Override
	public void setOperLogCausaleObj(String operLogCausaleObj) {
		if (operLogCausaleObj!= null) then
		    setOperLogCausale(operLogCausaleObj);
		    ws.getIndMatrValVar().setOperLogCausale(0);
		else
		    ws.getIndMatrValVar().setOperLogCausale(-1);
		endif;
	}

	@Override
	public string getOperLogStatoBus() {
		return matrValVar.getMvvOperLogStatoBus();
	}

	@Override
	public void setOperLogStatoBus(string operLogStatoBus) {
		this.matrValVar.setMvvOperLogStatoBus(operLogStatoBus);
	}

	@Override
	public String getOperLogStatoBusObj() {
		if (ws.getIndMatrValVar().getOperLogStatoBus()>= 0) then
		    return getOperLogStatoBus();
		else
		    return null;
		endif;
	}

	@Override
	public void setOperLogStatoBusObj(String operLogStatoBusObj) {
		if (operLogStatoBusObj!= null) then
		    setOperLogStatoBus(operLogStatoBusObj);
		    ws.getIndMatrValVar().setOperLogStatoBus(0);
		else
		    ws.getIndMatrValVar().setOperLogStatoBus(-1);
		endif;
	}

	@Override
	public string getOperazione() {
		return matrValVar.getMvvOperazione();
	}

	@Override
	public void setOperazione(string operazione) {
		this.matrValVar.setMvvOperazione(operazione);
	}

	@Override
	public String getOperazioneObj() {
		if (ws.getIndMatrValVar().getOperazione()>= 0) then
		    return getOperazione();
		else
		    return null;
		endif;
	}

	@Override
	public void setOperazioneObj(String operazioneObj) {
		if (operazioneObj!= null) then
		    setOperazione(operazioneObj);
		    ws.getIndMatrValVar().setOperazione(0);
		else
		    ws.getIndMatrValVar().setOperazione(-1);
		endif;
	}

	@Override
	public string getServizioLettura() {
		return matrValVar.getMvvServizioLettura();
	}

	@Override
	public void setServizioLettura(string servizioLettura) {
		this.matrValVar.setMvvServizioLettura(servizioLettura);
	}

	@Override
	public String getServizioLetturaObj() {
		if (ws.getIndMatrValVar().getServizioLettura()>= 0) then
		    return getServizioLettura();
		else
		    return null;
		endif;
	}

	@Override
	public void setServizioLetturaObj(String servizioLetturaObj) {
		if (servizioLetturaObj!= null) then
		    setServizioLettura(servizioLetturaObj);
		    ws.getIndMatrValVar().setServizioLettura(0);
		else
		    ws.getIndMatrValVar().setServizioLettura(-1);
		endif;
	}

	@Override
	public char getStepConversazione() {
		return matrValVar.getMvvStepConversazione();
	}

	@Override
	public void setStepConversazione(char stepConversazione) {
		this.matrValVar.setMvvStepConversazione(stepConversazione);
	}

	@Override
	public Character getStepConversazioneObj() {
		if (ws.getIndMatrValVar().getStepConversazione()>= 0) then
		    return getStepConversazione();
		else
		    return null;
		endif;
	}

	@Override
	public void setStepConversazioneObj(Character stepConversazioneObj) {
		if (stepConversazioneObj!= null) then
		    setStepConversazione(stepConversazioneObj);
		    ws.getIndMatrValVar().setStepConversazione(0);
		else
		    ws.getIndMatrValVar().setStepConversazione(-1);
		endif;
	}

	@Override
	public char getStepValorizzatore() {
		return matrValVar.getMvvStepValorizzatore();
	}

	@Override
	public void setStepValorizzatore(char stepValorizzatore) {
		this.matrValVar.setMvvStepValorizzatore(stepValorizzatore);
	}

	@Override
	public Character getStepValorizzatoreObj() {
		if (ws.getIndMatrValVar().getStepValorizzatore()>= 0) then
		    return getStepValorizzatore();
		else
		    return null;
		endif;
	}

	@Override
	public void setStepValorizzatoreObj(Character stepValorizzatoreObj) {
		if (stepValorizzatoreObj!= null) then
		    setStepValorizzatore(stepValorizzatoreObj);
		    ws.getIndMatrValVar().setStepValorizzatore(0);
		else
		    ws.getIndMatrValVar().setStepValorizzatore(-1);
		endif;
	}

	@Override
	public integer getTipoMovimento() {
		return matrValVar.getMvvTipoMovimento().getMvvTipoMovimento();
	}

	@Override
	public void setTipoMovimento(integer tipoMovimento) {
		this.matrValVar.getMvvTipoMovimento().setMvvTipoMovimento(tipoMovimento);
	}

	@Override
	public Integer getTipoMovimentoObj() {
		if (ws.getIndMatrValVar().getTipoMovimento()>= 0) then
		    return getTipoMovimento();
		else
		    return null;
		endif;
	}

	@Override
	public void setTipoMovimentoObj(Integer tipoMovimentoObj) {
		if (tipoMovimentoObj!= null) then
		    setTipoMovimento(tipoMovimentoObj);
		    ws.getIndMatrValVar().setTipoMovimento(0);
		else
		    ws.getIndMatrValVar().setTipoMovimento(-1);
		endif;
	}

	@Override
	public string getTipoOggetto() {
		return matrValVar.getMvvTipoOggetto();
	}

	@Override
	public void setTipoOggetto(string tipoOggetto) {
		this.matrValVar.setMvvTipoOggetto(tipoOggetto);
	}

	@Override
	public String getTipoOggettoObj() {
		if (ws.getIndMatrValVar().getTipoOggetto()>= 0) then
		    return getTipoOggetto();
		else
		    return null;
		endif;
	}

	@Override
	public void setTipoOggettoObj(String tipoOggettoObj) {
		if (tipoOggettoObj!= null) then
		    setTipoOggetto(tipoOggettoObj);
		    ws.getIndMatrValVar().setTipoOggetto(0);
		else
		    ws.getIndMatrValVar().setTipoOggetto(-1);
		endif;
	}

	@Override
	public string getValoreDefault() {
		return matrValVar.getMvvValoreDefault();
	}

	@Override
	public void setValoreDefault(string valoreDefault) {
		this.matrValVar.setMvvValoreDefault(valoreDefault);
	}

	@Override
	public String getValoreDefaultObj() {
		if (ws.getIndMatrValVar().getValoreDefault()>= 0) then
		    return getValoreDefault();
		else
		    return null;
		endif;
	}

	@Override
	public void setValoreDefaultObj(String valoreDefaultObj) {
		if (valoreDefaultObj!= null) then
		    setValoreDefault(valoreDefaultObj);
		    ws.getIndMatrValVar().setValoreDefault(0);
		else
		    ws.getIndMatrValVar().setValoreDefault(-1);
		endif;
	}

	@Override
	public string getWhereConditionVchar() {
		return matrValVar.getMvvWhereConditionVcharFormatted();
	}

	@Override
	public void setWhereConditionVchar(string whereConditionVchar) {
		this.matrValVar.setMvvWhereConditionVcharFormatted(whereConditionVchar);
	}

	@Override
	public String getWhereConditionVcharObj() {
		if (ws.getIndMatrValVar().getWhereCondition()>= 0) then
		    return getWhereConditionVchar();
		else
		    return null;
		endif;
	}

	@Override
	public void setWhereConditionVcharObj(String whereConditionVcharObj) {
		if (whereConditionVcharObj!= null) then
		    setWhereConditionVchar(whereConditionVcharObj);
		    ws.getIndMatrValVar().setWhereCondition(0);
		else
		    ws.getIndMatrValVar().setWhereCondition(-1);
		endif;
	}

}//Ldbs1400